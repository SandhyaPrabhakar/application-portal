use Master
go

if not exists (select * from master.dbo.syslogins where [Name] = 'CaravanQLDLogin')
  exec sp_addlogin 
      @loginame = 'CaravanQLDLogin' 
    , @passwd = 'Admin@CaravanQLD' 
    , @defdb = 'CaravanQLDPortal' 
go

--*********************--

use CaravanQLDPortal
go

exec sp_changedbowner 'CaravanQLDLogin'
go


