declare @ShowIds table(ID int identity, ShowId varchar(max))
create table #ExhibitorIds (ID int identity, ExhibitorId uniqueidentifier)

declare @ShowIdTotalCount int,
@ShowIdCount int = 1,
@ShowId varchar(max),
@ExhibitorIdTotalCount int,
@ExhibitorIdCount int,
@ExhibitorId uniqueidentifier

insert into @ShowIds (ShowId) 
select distinct ShowId from ExhibitorApplication 
where LinkedShowId = '678A19BE-A5F9-4B83-84B1-6ABCA50F08F2'

select @ShowIdTotalCount = count(*) from @ShowIds

while @ShowIdCount <= @ShowIdTotalCount
begin
	select @ShowId = ShowId from @ShowIds where ID = @ShowIdCount

	insert into #ExhibitorIds (ExhibitorId) 
	select Id from ExhibitorApplication 
	where ShowId = @ShowId and LinkedShowId = '678A19BE-A5F9-4B83-84B1-6ABCA50F08F2'

	set @ExhibitorIdCount = 1
	select @ExhibitorIdTotalCount = count(*) from #ExhibitorIds

	while @ExhibitorIdCount < @ExhibitorIdTotalCount
	begin
		select @ExhibitorId = ExhibitorId from #ExhibitorIds where ID = @ExhibitorIdCount
		
		--delete FormFieldValues
		delete FormFieldValues where ExhibitorApplicationId = @ExhibitorId
		--delete FormExhibitors
		delete from FormExhibitors where ExhibitorApplicationId = @ExhibitorId
		--delete EmailLog
		delete from EmailLog where ExhibitorApplicationId = @ExhibitorId
		--delete ExhibitorApplicationEmail
		delete from ExhibitorApplicationEmail where ExhibitorApplicationId = @ExhibitorId
		--delete SiteInfo
		delete from SiteInfo where ExhibitorApplicationId = @ExhibitorId
		--delete ProductInfo
		delete from ProductInfo where ExhibitorApplicationId = @ExhibitorId
		--delete ExhibitorApplication
		delete from ExhibitorApplication where Id = @ExhibitorId
				
		set @ExhibitorIdCount = @ExhibitorIdCount + 1
	end
	
	truncate table #ExhibitorIds
	set @ShowIdCount = @ShowIdCount + 1
end

drop table #ExhibitorIds