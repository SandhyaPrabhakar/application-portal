
---------------------------------------------------------------------------------------
--This script is to move from a static application form to a dynamic application form--
---------------------------------------------------------------------------------------

declare @ExhibitorId uniqueidentifier,
@TradingName nvarchar(max),
@ShowListingName nvarchar(max),
@AddressLine1 nvarchar(max),
@AddressLine2 nvarchar(max),
@SuburbTown nvarchar(max),
@State nvarchar(max),
@Country nvarchar(max),
@Postcode nvarchar(max),
@ExhibitorPhone nvarchar(max),
@ExhibitorFax nvarchar(max),
@ExhibitorEmail nvarchar(max),
@ConfirmExhibitorEmail nvarchar(max),
@Website nvarchar(max),

@TitlePosition nvarchar(max),
@FirstName nvarchar(max),
@Surname nvarchar(max),
@Mobile nvarchar(max),
@Phone nvarchar(max),
@Email nvarchar(max),
@ConfirmEmail nvarchar(max),

@IsShowContact nvarchar(max),
@ShowContactTitlePosition nvarchar(max),
@ShowContactFirstName nvarchar(max),
@ShowContactSurname nvarchar(max),
@ShowContactMobile nvarchar(max),

@IsCQMember nvarchar(max),
@CaravanQLDMember nvarchar(max),
@CaravanQLDAssociate nvarchar(max),
@CaravanQLDMembershipID nvarchar(max),
@IsInterstateMember nvarchar(max),
@InterstateMember nvarchar(max),
@InterstateAssociate nvarchar(max),
@InterstateAssociation nvarchar(max),
@CIAAContributed nvarchar(max),

@MarqueeRequired nvarchar(max),
@Frontage nvarchar(max),
@Depth nvarchar(max),
@LocationRequested nvarchar(max),
@ShellScheme nvarchar(max),
@AmplificationRequired nvarchar(max),

@Products nvarchar(max),
@CaravanBrand1 nvarchar(max),
@CaravanBrand2 nvarchar(max),
@CaravanBrand3 nvarchar(max),
@CaravanBrand4 nvarchar(max),
@CaravanBrand5 nvarchar(max),
@CaravanBrand6 nvarchar(max),
@MotorhomeBrand1 nvarchar(max),
@MotorhomeBrand2 nvarchar(max),
@MotorhomeBrand3 nvarchar(max),
@MotorhomeBrand4 nvarchar(max),
@CamperTrailerBrand1 nvarchar(max),
@CamperTrailerBrand2 nvarchar(max),
@CamperTrailerBrand3 nvarchar(max),
@CamperTrailerBrand4 nvarchar(max),
@CamperTrailerBrand5 nvarchar(max),
@CamperTrailerBrand6 nvarchar(max),
@FifthwheelerBrand1 nvarchar(max),
@FifthwheelerBrand2 nvarchar(max),
@FifthwheelerBrand3 nvarchar(max),
@FifthwheelerBrand4 nvarchar(max),
@AccessoriesParts nvarchar(max),
@AdvisoryServices nvarchar(max),
@AnnexesAwnings nvarchar(max),
@ArtUnionsFundraising nvarchar(max),
@CabinsHomes nvarchar(max),
@CamperTrailers nvarchar(max),
@CampingEquipmentTents nvarchar(max),
@CaravansPoptops nvarchar(max),
@CarportsCaravanCovers nvarchar(max),
@ClothingHats nvarchar(max),
@Clubs nvarchar(max),
@Communication nvarchar(max),
@Fifthwheelers nvarchar(max),
@Hire nvarchar(max),
@Insurance nvarchar(max),
@MarineFishingBoatTrailers nvarchar(max),
@MotorhomesCampervansConversions nvarchar(max),
@Publications nvarchar(max),
@RoofTopCampers nvarchar(max),
@SlideOnTraytopCampers nvarchar(max),
@TouringTourismCaravanParks nvarchar(max),
@TowingEquipment nvarchar(max),
@VehiclesVehicleAcc4WDAcc nvarchar(max),
@OtherProductsServices nvarchar(max),

@AccessHeight nvarchar(max),
@BSB nvarchar(max),
@AccountNo nvarchar(max),
@AccountName nvarchar(max),
@BankName nvarchar(max),
@BranchName nvarchar(max),
@AdditionalNotesRequests nvarchar(max),
@AcceptedTermsAndConditions nvarchar(max)

--Gold Coast Show ID - 446204DB-D1FA-4A9D-89DA-75A2125C6CB9
--Get the list of exhibitors
declare @ExhibitorIdList table(ID int identity, ExhibitorID uniqueidentifier)

insert into @ExhibitorIdList (ExhibitorID) select Id from ExhibitorApplication ea where ea.LinkedShowId = '446204DB-D1FA-4A9D-89DA-75A2125C6CB9'

select * from @ExhibitorIdList

--Loop through every exhibitor
declare @RowCount int = 1
declare @ExhibitorIdCount int
set @ExhibitorIdCount = (select count(*) from @ExhibitorIdList)

while @RowCount <= @ExhibitorIdCount
	begin
		set @ExhibitorId = (select ExhibitorID from @ExhibitorIdList where ID = @RowCount)

		select 
		@TradingName = e.TradingName, 
		@ShowListingName = e.ShowListingName, 
		@AddressLine1 = e.AddressLine1,
		@AddressLine2 = e.AddressLine2,
		@SuburbTown = e.Suburb,
		@State = e.[State],
		@Country = e.Country,
		@Postcode = e.Postcode,
		@ExhibitorPhone = e.ExhibitorPhone,
		@ExhibitorFax = e.ExhibitorFax,
		@ExhibitorEmail = e.ExhibitorEmail,
		@ConfirmExhibitorEmail = e.ExhibitorEmail,
		@Website = e.Website,
		@TitlePosition = e.Title,
		@FirstName = e.FirstName,
		@Surname = e.Surname,
		@Mobile = e.Mobile,
		@Phone = e.Phone,
		@Email = e.Email,
		@ConfirmEmail = e.Email,
		@IsShowContact = (CASE e.IsShowContact WHEN 1 THEN 'Yes' ELSE 'No' END),
		@ShowContactTitlePosition = e.ShowContactTitle,
		@ShowContactFirstName = e.ShowContactFirstName,
		@ShowContactSurname = e.ShowContactSurname,
		@ShowContactMobile = e.ShowContactMobile,
		@IsCQMember = (CASE e.CQMember WHEN 1 THEN 'Yes' ELSE 'No' END),
		@CaravanQLDMember = (CASE e.CQMemberType WHEN 'Full Member' THEN 'Caravan QLD Member' ELSE '' END),
		@CaravanQLDAssociate = (CASE e.CQMemberType WHEN 'Associate Member' THEN 'Caravan QLD Associate' ELSE '' END),
		@CaravanQLDMembershipID = e.CQMembershipID,
		@IsInterstateMember = (CASE e.InterstateMember WHEN 1 THEN 'Yes' ELSE 'No' END),
		@InterstateMember = (CASE e.InterstateMemberType WHEN 'Full Member' THEN 'Interstate Member' ELSE '' END),
		@InterstateAssociate = (CASE e.InterstateMemberType WHEN 'Associate Member' THEN 'Interstate Associate' ELSE '' END),
		@InterstateAssociation = e.InterstateAssociation,
		@CIAAContributed = (CASE e.Contributed WHEN 1 THEN 'Yes' ELSE 'No' END),
		@MarqueeRequired = (CASE s.MarqueeRequired WHEN 1 THEN 'Yes' ELSE 'No' END),
		@Frontage = s.FrontageRequested,
		@Depth = s.DepthRequested,
		@LocationRequested = (case s.Indoors when 1 then (case s.Outdoors when 1 then '''Indoors''|''Outdoors''' else '''Indoors''' end) else '''Outdoors''' end),
		@ShellScheme = (CASE s.ShellScheme WHEN 1 THEN 'Yes' ELSE 'No' END),
		@AmplificationRequired = (CASE s.Amplification WHEN 1 THEN 'Yes' ELSE 'No' END),
		@Products = p.Products,
		@CaravanBrand1 = p.CaravanBrand1,
		@CaravanBrand2 = p.CaravanBrand2,
		@CaravanBrand3 = p.CaravanBrand3,
		@CaravanBrand4 = p.CaravanBrand4,
		@CaravanBrand5 = p.CaravanBrand5,
		@CaravanBrand6 = p.CaravanBrand6,
		@MotorhomeBrand1 = p.MotorhomeBrand1,
		@MotorhomeBrand2 = p.MotorhomeBrand2,
		@MotorhomeBrand3 = p.MotorhomeBrand3,
		@MotorhomeBrand4 = p.MotorhomeBrand4,
		@CamperTrailerBrand1 = p.CamperTrailerBrand1,
		@CamperTrailerBrand2 = p.CamperTrailerBrand2,
		@CamperTrailerBrand3 = p.CamperTrailerBrand3,
		@CamperTrailerBrand4 = p.CamperTrailerBrand4,
		@CamperTrailerBrand5 = p.CamperTrailerBrand5,
		@CamperTrailerBrand6 = p.CamperTrailerBrand6,
		@FifthwheelerBrand1 = p.FifthwheelerBrand1,
		@FifthwheelerBrand2 = p.FifthwheelerBrand2,
		@FifthwheelerBrand3 = p.FifthwheelerBrand3,
		@FifthwheelerBrand4 = p.FifthwheelerBrand4,
		@AccessoriesParts = (CASE p.AccessoriesParts WHEN 1 THEN 'Accessories / Parts' ELSE '' END),
		@AdvisoryServices = (CASE p.AdvisoryServices WHEN 1 THEN 'Advisory Services' ELSE '' END),
		@AnnexesAwnings = (CASE p.AnnexesAwnings WHEN 1 THEN 'Annexes / Awnings' ELSE '' END),
		@ArtUnionsFundraising = (CASE p.ArtUnionsFundraising WHEN 1 THEN 'Art Unions / Fundraising' ELSE '' END),
		@CabinsHomes = (CASE p.CabinsHomes WHEN 1 THEN 'Cabins / Homes' ELSE '' END),
		@CamperTrailers = (CASE p.CamperTrailers WHEN 1 THEN 'Camper Trailers' ELSE '' END),
		@CampingEquipmentTents = (CASE p.CampingEquipmentTents WHEN 1 THEN 'Camping Equipment / Tents' ELSE '' END),
		@CaravansPoptops = (CASE p.CaravansPoptops WHEN 1 THEN 'Caravans / Poptops' ELSE '' END),
		@CarportsCaravanCovers = (CASE p.CarportsCaravanCovers WHEN 1 THEN 'Carports / Caravan Covers' ELSE '' END),
		@ClothingHats = (CASE p.ClothingHats WHEN 1 THEN 'Clothing / Hats' ELSE '' END),
		@Clubs = (CASE p.Clubs WHEN 1 THEN 'Clubs' ELSE '' END),
		@Communication = (CASE p.Communication WHEN 1 THEN 'Communication' ELSE '' END),
		@Fifthwheelers = (CASE p.Fifthwheelers WHEN 1 THEN 'Fifthwheelers' ELSE '' END),
		@Hire = (CASE p.Hire WHEN 1 THEN 'Hire' ELSE '' END),
		@Insurance = (CASE p.Insurance WHEN 1 THEN 'Insurance' ELSE '' END),
		@MarineFishingBoatTrailers = (CASE p.MarineFishingBoatTrailers WHEN 1 THEN 'Marine / Fishing / Boat Trailers' ELSE '' END),
		@MotorhomesCampervansConversions = (CASE p.MotorhomesCampervansConversions WHEN 1 THEN 'Motorhomes / Campervans / Conversions' ELSE '' END),
		@Publications = (CASE p.Publications WHEN 1 THEN 'Publications' ELSE '' END),
		@RoofTopCampers = (CASE p.AccessoriesParts WHEN 1 THEN 'Roof Top Campers' ELSE '' END),
		@SlideOnTraytopCampers = (CASE p.AccessoriesParts WHEN 1 THEN 'Slide On / Traytop Campers' ELSE '' END),
		@TouringTourismCaravanParks = (CASE p.AccessoriesParts WHEN 1 THEN 'Touring / Tourism / Caravan Parks' ELSE '' END),
		@TowingEquipment = (CASE p.TowingEquipment WHEN 1 THEN 'Towing Equipment' ELSE '' END),
		@VehiclesVehicleAcc4WDAcc = (CASE p.VehiclesVehicleAccessories4WDAccessories WHEN 1 THEN 'Vehicles / Vehicle Acc. / 4WD Acc.' ELSE '' END),
		@OtherProductsServices = (CASE p.OtherProductsServices WHEN 1 THEN 'Other Products / Services' ELSE '' END),
		@AccessHeight = p.AccessHeight,
		@BSB = s.BSB,
		@AccountNo = s.AccountNo,
		@AccountName = s.AccountName,
		@BankName = s.BankName,
		@BranchName = s.BranchName,
		@AdditionalNotesRequests = e.NotesAndRequests,
		@AcceptedTermsAndConditions = (CASE e.AcceptedTermsAndConditions WHEN 1 THEN 'Yes' ELSE 'No' END)
		from ExhibitorApplication e
		inner join SiteInfo s on s.ExhibitorApplicationId = e.Id
		inner join ProductInfo p on p.ExhibitorApplicationId = e.Id
		where e.Id = @ExhibitorId

		update FormFieldValues set Value = @TradingName where FormFieldId = 'E9C692D0-92AD-4F7A-BF8B-C3A338CBCEB2' and ExhibitorApplicationId = @ExhibitorId
		print 'hello'
		update FormFieldValues set Value = @ShowListingName where FormFieldId = 'EF679AFF-1009-4D92-B94D-30B03B0E31BA' and ExhibitorApplicationId = @ExhibitorId
		print 'hello'
		update FormFieldValues set Value = @AddressLine1 where FormFieldId = '6A753E15-6BDA-48FD-9AD7-BF3B65DEE9AC' and ExhibitorApplicationId = @ExhibitorId
		print 'hello'
		update FormFieldValues set Value = @AddressLine2 where FormFieldId = '6B80897C-C232-4C88-8664-3E1A1B4BE893' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @SuburbTown where FormFieldId = '0E492EB9-2531-43FB-8085-9C81F5DD9C0C' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @State where FormFieldId = '19201591-F8B9-4EB1-91F2-2CF9C5D4B7CF' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Country where FormFieldId = 'C690EC79-32E4-44CB-8175-9CBA6FB869FC' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Postcode where FormFieldId = 'C3D0E443-ABE6-44F2-88A4-CC500BA2DBBD' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ExhibitorPhone where FormFieldId = '1CAACB3A-8A4C-4D13-81B8-24015B436964' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ExhibitorFax where FormFieldId = '427DD9DE-345E-4620-AF59-EB24F9D84383' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ExhibitorEmail where FormFieldId = '39974F65-ACB8-4097-A9BE-CF4DABC3E22D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ConfirmExhibitorEmail where FormFieldId = '299FBF30-36D1-4596-8BC3-9F39672DD065' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Website where FormFieldId = 'A17CDB10-9894-4705-A026-A0562DCFE22C' and ExhibitorApplicationId = @ExhibitorId

		update FormFieldValues set Value = @TitlePosition where FormFieldId = '02DA53F6-7920-49DC-B297-4613BE6F5844' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @FirstName where FormFieldId = 'C7C5F538-1C7A-457D-BF74-ADAAFB3911B7' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Surname where FormFieldId = '36285BE3-11D8-4719-BE69-915832FDBF95' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Mobile where FormFieldId = 'AF8E6C29-5302-4365-A2F4-FFBDABF5731D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Phone where FormFieldId = '3A4E668D-CB7D-4548-BE80-CC693D454743' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Email where FormFieldId = 'C3B3080C-7B04-4F9F-AE3E-F41CD41D6BCF' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ConfirmEmail where FormFieldId = 'E4BC524D-2623-4ECD-B2FE-AABBE74AB700' and ExhibitorApplicationId = @ExhibitorId

		update FormFieldValues set Value = @IsShowContact where FormFieldId = '227BCEA3-5E47-4FEE-9703-85AAFBC7D711' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ShowContactTitlePosition where FormFieldId = '0C186DC6-6742-4880-AAA6-0A1258A8ADB9' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ShowContactFirstName where FormFieldId = 'A509314C-5E3D-4421-B84A-7543497486D7' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ShowContactSurname where FormFieldId = 'A115D532-D35A-44B8-B02F-988B0A3116C2' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ShowContactMobile where FormFieldId = '931A908A-4926-46EC-A7D4-CED252857AD5' and ExhibitorApplicationId = @ExhibitorId
		print '789'
		update FormFieldValues set Value = @IsCQMember where FormFieldId = 'E4464DA3-E596-4D42-9AC3-2F2B351494B5' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanQLDMember where FormFieldId = '328DCD33-7E0E-4C7A-8176-DC26B1CDA620' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanQLDAssociate where FormFieldId = '8A833F9A-C6F1-4311-B314-34DBB724CF8B' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanQLDMembershipID where FormFieldId = '1629A655-DC14-48EA-BF89-C593CAC30D42' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @IsInterstateMember where FormFieldId = 'C046C3D7-AC3A-4D71-88EB-F9E9DB742949' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @InterstateMember where FormFieldId = '65623160-93BB-4D97-9484-5B466356BE1D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @InterstateAssociate where FormFieldId = 'FA0F53DD-E59C-43CF-8494-B5328219E8EB' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @InterstateAssociation where FormFieldId = '5C3740A8-A80F-486E-B0D1-C1DFDB541A0D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CIAAContributed where FormFieldId = 'EE5F6BD4-C4A7-413B-BAC5-445603C0140D' and ExhibitorApplicationId = @ExhibitorId
		print '456'
		update FormFieldValues set Value = @MarqueeRequired where FormFieldId = 'BCAAA9C7-7E90-40DC-ABA3-B891E687B23A' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Frontage where FormFieldId = 'C5F495D8-6DD0-43F2-A010-56125C80FB49' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Depth where FormFieldId = '816864E9-F5AB-44B1-9623-B04D4BCB7835' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @LocationRequested where FormFieldId = '95519E59-F162-4AF2-B7D2-4D1F96660E2D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ShellScheme where FormFieldId = '8FAE87DB-AF3B-4707-8210-C105D7E88C2D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AmplificationRequired where FormFieldId = 'CEF30F64-4CA6-4B3D-898C-6D521514A695' and ExhibitorApplicationId = @ExhibitorId
		print '234'

		update FormFieldValues set Value = @Products where FormFieldId = 'A5F0C267-E69E-4196-93E3-2CE3F20CF530' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanBrand1 where FormFieldId = 'C41FB2BE-5AD4-4051-AFE4-B4F2BC61E21C' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanBrand2 where FormFieldId = '109A5AA4-5CDB-47A9-9F04-286D133F6843' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanBrand3 where FormFieldId = '63F48E0B-36FE-4201-B555-8EA13E41AC51' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanBrand4 where FormFieldId = '53AA70BC-57CE-4A71-805D-BEBBF7D1F040' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanBrand5 where FormFieldId = 'B62A3E1D-0FB6-423C-89D5-DE6CCFD7D698' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravanBrand6 where FormFieldId = 'AD0470BD-CA6B-4395-8BF0-58608F7103FB' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @MotorhomeBrand1 where FormFieldId = '0982BCB8-75EA-458F-9BEF-A53F082AD6BA' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @MotorhomeBrand2 where FormFieldId = '98B97F0C-BF38-467B-9B0B-134916F0F4D8' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @MotorhomeBrand3 where FormFieldId = '11644BFA-D129-4192-990E-7877FF6C4547' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @MotorhomeBrand4 where FormFieldId = '0DBBE236-BF83-440A-8FC8-358C724E2FC8' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailerBrand1 where FormFieldId = 'DF1D7133-1637-482B-8F11-4859455E4CB3' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailerBrand2 where FormFieldId = '344BB13E-D94E-4492-9928-7BBDDF5610A1' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailerBrand3 where FormFieldId = '258F99D9-A159-4E30-A3CF-28F50F6069A6' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailerBrand4 where FormFieldId = '0AA3B2F1-067E-4561-9567-5230E25FD076' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailerBrand5 where FormFieldId = '89E99719-E8EE-4003-AFB4-8556CBB2390B' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailerBrand6 where FormFieldId = '7DFF259F-5B01-425D-9CE1-75EB4A47EEAF' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @FifthwheelerBrand1 where FormFieldId = '965AB712-B175-4449-9351-40F070A3A87F' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @FifthwheelerBrand2 where FormFieldId = 'AD4B2E9F-19DD-40EF-B63E-19DF11D6EAFC' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @FifthwheelerBrand3 where FormFieldId = 'D23A7757-009E-40ED-9A17-E545E9E60A2C' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @FifthwheelerBrand4 where FormFieldId = 'E3D8D367-6DFF-40CE-B3DC-692C51C81A5B' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AccessoriesParts where FormFieldId = 'BB6049D1-F1A0-44AB-B214-BCF8CBA0D49C' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AdvisoryServices where FormFieldId = 'CA365510-7607-4A22-9F70-46793F4CF108' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AnnexesAwnings where FormFieldId = '67158942-ADE5-4D8D-99A4-47AB50D4164E' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ArtUnionsFundraising where FormFieldId = '9EAA3E02-7655-492F-A045-F521FD5A0C4F' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CabinsHomes where FormFieldId = '6BE8886F-30DC-4B6E-B49B-9E3FFF920454' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CamperTrailers where FormFieldId = 'C7E638F5-8F42-4A96-ABAA-0A49FEACCCD3' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CampingEquipmentTents where FormFieldId = 'BCAD618E-61C4-416B-9A2F-C77B439358A9' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CaravansPoptops where FormFieldId = 'C6C07957-3928-4F33-8EC4-0C49DBBD8E27' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @CarportsCaravanCovers where FormFieldId = '893E2BEA-867C-4FBD-90F2-0B7C7C26F87C' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @ClothingHats where FormFieldId = '1CB2CAF6-D005-4F07-A197-DFD25F89755E' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Clubs where FormFieldId = '6E697BAF-A866-4DD1-8357-3F009F316103' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Communication where FormFieldId = 'F33708BC-22AF-4234-87DA-0FF73E363AD0' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Fifthwheelers where FormFieldId = '62F23B87-6770-49CA-8DDF-281F2F7B8F78' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Hire where FormFieldId = '75420296-15DE-4C09-9F26-D7ECB07DA31E' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Insurance where FormFieldId = '1F0D7C73-9132-437D-9E15-10F48AC45741' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @MarineFishingBoatTrailers where FormFieldId = 'FE4A8F69-4C71-45F6-AA6D-ADE105BFF901' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @MotorhomesCampervansConversions where FormFieldId = 'E02DD6A5-4569-4016-9CF2-3CE3541A257E' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @Publications where FormFieldId = '72BD96E3-04A2-43DE-B6D5-B3784D03F871' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @RoofTopCampers where FormFieldId = '9ED1FE5B-1619-45FE-AD23-F7E885E14976' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @SlideOnTraytopCampers where FormFieldId = '043B8E55-5C4D-4002-AD31-E42D3C86088B' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @TouringTourismCaravanParks where FormFieldId = 'C0CF305A-C3DD-4DE7-B750-38FDA5F34A91' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @TowingEquipment where FormFieldId = '190EC385-D194-4F0F-A1ED-96F789A9469B' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @VehiclesVehicleAcc4WDAcc where FormFieldId = 'FDEC3BB0-E3E7-41E4-9AB1-9E80F75C8E1F' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @OtherProductsServices where FormFieldId = '77AD70A7-559F-42AF-9BD5-2432B5302DCE' and ExhibitorApplicationId = @ExhibitorId

		update FormFieldValues set Value = @AccessHeight where FormFieldId = '229ADF3F-3914-4901-8987-C2A84484FF33' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @BSB where FormFieldId = 'FAAF21BE-F69A-40DB-882B-9ABCD3312EA0' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AccountNo where FormFieldId = 'EE6A833F-0C38-4D81-A6D0-375E1AC4D234' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AccountName where FormFieldId = '78D14846-E5B9-4AD9-8708-83A4E8BFE9A7' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @BankName where FormFieldId = '2D0DD043-870C-45C6-AD9A-446B3A180F6D' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @BranchName where FormFieldId = '1E6ED4A4-8FD1-4804-890A-00E79AC8EECE' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AdditionalNotesRequests where FormFieldId = 'EC2C9739-BAEF-4A25-B77C-3D6B5188FCDB' and ExhibitorApplicationId = @ExhibitorId
		update FormFieldValues set Value = @AcceptedTermsAndConditions where FormFieldId = '2E398271-B4D5-433C-8A9C-895232935A2E' and ExhibitorApplicationId = @ExhibitorId

		set @RowCount = @RowCount + 1;
	end


