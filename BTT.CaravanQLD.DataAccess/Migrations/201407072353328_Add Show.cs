namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShow : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Shows",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ShowType = c.Int(nullable: false),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ExhibitorApplication", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.EmailLog", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.Email", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.EmailSetting", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.PortalSetting", "LinkedShowId", c => c.Guid());
            AddColumn("dbo.ContentPages", "LinkedShowId", c => c.Guid());
            AddForeignKey("dbo.ExhibitorApplication", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.ContentPages", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.Email", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.EmailLog", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.EmailSetting", "LinkedShowId", "dbo.Shows", "Id");
            AddForeignKey("dbo.PortalSetting", "LinkedShowId", "dbo.Shows", "Id");
            CreateIndex("dbo.ExhibitorApplication", "LinkedShowId");
            CreateIndex("dbo.ContentPages", "LinkedShowId");
            CreateIndex("dbo.Email", "LinkedShowId");
            CreateIndex("dbo.EmailLog", "LinkedShowId");
            CreateIndex("dbo.EmailSetting", "LinkedShowId");
            CreateIndex("dbo.PortalSetting", "LinkedShowId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PortalSetting", new[] { "LinkedShowId" });
            DropIndex("dbo.EmailSetting", new[] { "LinkedShowId" });
            DropIndex("dbo.EmailLog", new[] { "LinkedShowId" });
            DropIndex("dbo.Email", new[] { "LinkedShowId" });
            DropIndex("dbo.ContentPages", new[] { "LinkedShowId" });
            DropIndex("dbo.ExhibitorApplication", new[] { "LinkedShowId" });
            DropForeignKey("dbo.PortalSetting", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.EmailSetting", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.EmailLog", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.Email", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.ContentPages", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.ExhibitorApplication", "LinkedShowId", "dbo.Shows");
            DropColumn("dbo.ContentPages", "LinkedShowId");
            DropColumn("dbo.PortalSetting", "LinkedShowId");
            DropColumn("dbo.EmailSetting", "LinkedShowId");
            DropColumn("dbo.Email", "LinkedShowId");
            DropColumn("dbo.EmailLog", "LinkedShowId");
            DropColumn("dbo.ExhibitorApplication", "LinkedShowId");
            DropTable("dbo.Shows");
        }
    }
}
