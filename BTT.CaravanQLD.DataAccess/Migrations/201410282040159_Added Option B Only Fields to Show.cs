namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOptionBOnlyFieldstoShow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shows", "OnlyOptionB", c => c.Boolean(nullable: false));
            AddColumn("dbo.Shows", "OptionBTitle", c => c.String());
            AddColumn("dbo.Shows", "OptionBText", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shows", "OptionBText");
            DropColumn("dbo.Shows", "OptionBTitle");
            DropColumn("dbo.Shows", "OnlyOptionB");
        }
    }
}
