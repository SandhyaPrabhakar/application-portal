namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOutdoorsWithMarqueetoSiteInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteInfo", "OutdoorsWithMarquee", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteInfo", "OutdoorsWithMarquee");
        }
    }
}
