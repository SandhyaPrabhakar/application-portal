namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveShowTitlefromEmailSetting : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EmailSetting", "ShowTitle");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EmailSetting", "ShowTitle", c => c.String());
        }
    }
}
