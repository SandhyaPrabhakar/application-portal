namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMandatoryFormFieldandMandatoryFormFieldValuetoFormField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormFields", "MandatoryFormFieldId", c => c.Guid());
            AddColumn("dbo.FormFields", "MandatoryFormFieldValue", c => c.String());
            AddForeignKey("dbo.FormFields", "MandatoryFormFieldId", "dbo.FormFields", "Id");
            CreateIndex("dbo.FormFields", "MandatoryFormFieldId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormFields", new[] { "MandatoryFormFieldId" });
            DropForeignKey("dbo.FormFields", "MandatoryFormFieldId", "dbo.FormFields");
            DropColumn("dbo.FormFields", "MandatoryFormFieldValue");
            DropColumn("dbo.FormFields", "MandatoryFormFieldId");
        }
    }
}
