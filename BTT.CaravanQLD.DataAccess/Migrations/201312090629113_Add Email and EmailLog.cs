namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailandEmailLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailLog",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateSent = c.DateTime(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                        EmailId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExhibitorApplication", t => t.ExhibitorApplicationId, cascadeDelete: true)
                .ForeignKey("dbo.Email", t => t.EmailId, cascadeDelete: true)
                .Index(t => t.ExhibitorApplicationId)
                .Index(t => t.EmailId);
            
            CreateTable(
                "dbo.Email",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Subject = c.String(),
                        Body = c.String(unicode: false),
                        IsInitialEmail = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.EmailLog", new[] { "EmailId" });
            DropIndex("dbo.EmailLog", new[] { "ExhibitorApplicationId" });
            DropForeignKey("dbo.EmailLog", "EmailId", "dbo.Email");
            DropForeignKey("dbo.EmailLog", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropTable("dbo.Email");
            DropTable("dbo.EmailLog");
        }
    }
}
