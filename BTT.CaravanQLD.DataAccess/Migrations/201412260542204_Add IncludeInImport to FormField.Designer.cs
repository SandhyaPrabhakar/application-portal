// <auto-generated />
namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddIncludeInImporttoFormField : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddIncludeInImporttoFormField));
        
        string IMigrationMetadata.Id
        {
            get { return "201412260542204_Add IncludeInImport to FormField"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
