namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortByDateCreatedandDateCreated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Email", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.EmailSetting", "SortByDateCreated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSetting", "SortByDateCreated");
            DropColumn("dbo.Email", "DateCreated");
        }
    }
}
