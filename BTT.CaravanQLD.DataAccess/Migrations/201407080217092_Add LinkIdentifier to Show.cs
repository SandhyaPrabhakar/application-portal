namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLinkIdentifiertoShow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shows", "LinkIdentifier", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shows", "LinkIdentifier");
        }
    }
}
