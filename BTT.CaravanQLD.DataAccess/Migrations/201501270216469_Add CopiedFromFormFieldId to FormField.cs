namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCopiedFromFormFieldIdtoFormField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormFields", "CopiedFromFormFieldId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormFields", "CopiedFromFormFieldId");
        }
    }
}
