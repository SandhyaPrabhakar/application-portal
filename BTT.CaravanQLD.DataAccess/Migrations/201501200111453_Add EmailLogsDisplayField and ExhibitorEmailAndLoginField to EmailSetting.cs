namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailLogsDisplayFieldandExhibitorEmailAndLoginFieldtoEmailSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSetting", "EmailLogsDisplayField", c => c.String());
            AddColumn("dbo.EmailSetting", "ExhibitorEmailAndLoginField", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSetting", "ExhibitorEmailAndLoginField");
            DropColumn("dbo.EmailSetting", "EmailLogsDisplayField");
        }
    }
}
