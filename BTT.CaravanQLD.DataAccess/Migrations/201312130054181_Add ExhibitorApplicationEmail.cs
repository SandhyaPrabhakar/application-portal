namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExhibitorApplicationEmail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExhibitorApplicationEmail",
                c => new
                    {
                        ExhibitorApplicationId = c.Guid(nullable: false),
                        EmailId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ExhibitorApplicationId, t.EmailId })
                .ForeignKey("dbo.ExhibitorApplication", t => t.ExhibitorApplicationId, cascadeDelete: true)
                .ForeignKey("dbo.Email", t => t.EmailId, cascadeDelete: true)
                .Index(t => t.ExhibitorApplicationId)
                .Index(t => t.EmailId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ExhibitorApplicationEmail", new[] { "EmailId" });
            DropIndex("dbo.ExhibitorApplicationEmail", new[] { "ExhibitorApplicationId" });
            DropForeignKey("dbo.ExhibitorApplicationEmail", "EmailId", "dbo.Email");
            DropForeignKey("dbo.ExhibitorApplicationEmail", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropTable("dbo.ExhibitorApplicationEmail");
        }
    }
}
