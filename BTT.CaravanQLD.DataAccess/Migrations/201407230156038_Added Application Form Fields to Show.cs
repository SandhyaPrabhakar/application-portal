namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedApplicationFormFieldstoShow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shows", "Contributor", c => c.String());
            AddColumn("dbo.Shows", "ShellScheme", c => c.String());
            AddColumn("dbo.Shows", "AdditionalNotesAndRequests", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shows", "AdditionalNotesAndRequests");
            DropColumn("dbo.Shows", "ShellScheme");
            DropColumn("dbo.Shows", "Contributor");
        }
    }
}
