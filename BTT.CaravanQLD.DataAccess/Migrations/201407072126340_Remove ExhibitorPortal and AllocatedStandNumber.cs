namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveExhibitorPortalandAllocatedStandNumber : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExhibitorApplication", "ExhibitorPortal");
            DropColumn("dbo.SiteInfo", "AllocatedStandNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SiteInfo", "AllocatedStandNumber", c => c.String());
            AddColumn("dbo.ExhibitorApplication", "ExhibitorPortal", c => c.Boolean(nullable: false));
        }
    }
}
