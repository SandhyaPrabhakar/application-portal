namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTextFieldtoFormField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormFields", "TextField", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormFields", "TextField");
        }
    }
}
