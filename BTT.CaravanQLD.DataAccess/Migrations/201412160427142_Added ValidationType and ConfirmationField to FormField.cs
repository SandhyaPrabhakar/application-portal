namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedValidationTypeandConfirmationFieldtoFormField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormFields", "ValidationType", c => c.Int(nullable: false));
            AddColumn("dbo.FormFields", "ConfirmationField", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormFields", "ConfirmationField");
            DropColumn("dbo.FormFields", "ValidationType");
        }
    }
}
