namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailSetting",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CompanyName = c.String(),
                        ContactEmail = c.String(),
                        ContactNumber = c.String(),
                        ContactPostalAddress = c.String(),
                        ShowTitle = c.String(),
                        TestEmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailSetting");
        }
    }
}
