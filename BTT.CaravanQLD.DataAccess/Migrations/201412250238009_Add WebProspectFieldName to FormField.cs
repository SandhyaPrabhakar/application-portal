namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWebProspectFieldNametoFormField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormFields", "WebProspectFieldName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormFields", "WebProspectFieldName");
        }
    }
}
