namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added3WebProspectFieldstoShow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shows", "State", c => c.String());
            AddColumn("dbo.Shows", "CaravanQLDMembershipID", c => c.String());
            AddColumn("dbo.Shows", "InterstateAssociate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shows", "InterstateAssociate");
            DropColumn("dbo.Shows", "CaravanQLDMembershipID");
            DropColumn("dbo.Shows", "State");
        }
    }
}
