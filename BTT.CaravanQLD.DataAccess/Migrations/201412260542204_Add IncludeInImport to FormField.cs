namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIncludeInImporttoFormField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormFields", "IncludeInImport", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormFields", "IncludeInImport");
        }
    }
}
