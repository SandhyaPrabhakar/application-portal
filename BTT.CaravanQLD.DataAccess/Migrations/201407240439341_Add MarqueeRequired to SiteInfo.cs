namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMarqueeRequiredtoSiteInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteInfo", "MarqueeRequired", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteInfo", "MarqueeRequired");
        }
    }
}
