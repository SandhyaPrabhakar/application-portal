namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedFormsandallrelatedtables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FormFieldValues", "FormFieldId", "dbo.FormFields");
            DropForeignKey("dbo.FormFieldValues", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.FormFields", "FormId", "dbo.Forms");
            DropForeignKey("dbo.Forms", "FormGroupId", "dbo.FormGroups");
            DropForeignKey("dbo.Forms", "EmailId", "dbo.Email");
            DropForeignKey("dbo.FormExhibitors", "FormId", "dbo.Forms");
            DropForeignKey("dbo.FormExhibitors", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.FormFieldItems", "FormFieldId", "dbo.FormFields");
            DropIndex("dbo.FormFieldValues", new[] { "FormFieldId" });
            DropIndex("dbo.FormFieldValues", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.FormFields", new[] { "FormId" });
            DropIndex("dbo.Forms", new[] { "FormGroupId" });
            DropIndex("dbo.Forms", new[] { "EmailId" });
            DropIndex("dbo.FormExhibitors", new[] { "FormId" });
            DropIndex("dbo.FormExhibitors", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.FormFieldItems", new[] { "FormFieldId" });
            DropTable("dbo.FormFieldValues");
            DropTable("dbo.FormFields");
            DropTable("dbo.Forms");
            DropTable("dbo.FormGroups");
            DropTable("dbo.FormExhibitors");
            DropTable("dbo.FormFieldItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FormFieldItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Text = c.String(),
                        Value = c.String(),
                        Order = c.Int(nullable: false),
                        FormFieldId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormExhibitors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateCompleted = c.DateTime(),
                        FormId = c.Guid(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Forms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        DueDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        IsCurrent = c.Boolean(nullable: false),
                        Mandatory = c.Boolean(nullable: false),
                        TextContent = c.String(unicode: false),
                        FormGroupId = c.Guid(nullable: false),
                        EmailId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormFields",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DataType = c.Int(nullable: false),
                        Label = c.String(),
                        Name = c.String(),
                        TextContent = c.String(unicode: false),
                        Order = c.Int(nullable: false),
                        Mandatory = c.Boolean(nullable: false),
                        SpanTwoColumns = c.Boolean(nullable: false),
                        FormId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormFieldValues",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Value = c.String(),
                        FormFieldId = c.Guid(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.FormFieldItems", "FormFieldId");
            CreateIndex("dbo.FormExhibitors", "ExhibitorApplicationId");
            CreateIndex("dbo.FormExhibitors", "FormId");
            CreateIndex("dbo.Forms", "EmailId");
            CreateIndex("dbo.Forms", "FormGroupId");
            CreateIndex("dbo.FormFields", "FormId");
            CreateIndex("dbo.FormFieldValues", "ExhibitorApplicationId");
            CreateIndex("dbo.FormFieldValues", "FormFieldId");
            AddForeignKey("dbo.FormFieldItems", "FormFieldId", "dbo.FormFields", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FormExhibitors", "ExhibitorApplicationId", "dbo.ExhibitorApplication", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FormExhibitors", "FormId", "dbo.Forms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Forms", "EmailId", "dbo.Email", "Id");
            AddForeignKey("dbo.Forms", "FormGroupId", "dbo.FormGroups", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FormFields", "FormId", "dbo.Forms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FormFieldValues", "ExhibitorApplicationId", "dbo.ExhibitorApplication", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FormFieldValues", "FormFieldId", "dbo.FormFields", "Id", cascadeDelete: true);
        }
    }
}
