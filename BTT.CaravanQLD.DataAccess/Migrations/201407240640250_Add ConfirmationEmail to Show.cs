namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConfirmationEmailtoShow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shows", "ConfirmationEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shows", "ConfirmationEmail");
        }
    }
}
