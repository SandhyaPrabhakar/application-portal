namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortByFormFieldtoPortalSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PortalSetting", "FormFieldId", c => c.Guid());
            AddForeignKey("dbo.PortalSetting", "FormFieldId", "dbo.FormFields", "Id");
            CreateIndex("dbo.PortalSetting", "FormFieldId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PortalSetting", new[] { "FormFieldId" });
            DropForeignKey("dbo.PortalSetting", "FormFieldId", "dbo.FormFields");
            DropColumn("dbo.PortalSetting", "FormFieldId");
        }
    }
}
