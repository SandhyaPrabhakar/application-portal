namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFormClassesforDynamicForm : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Forms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        DueDate = c.DateTime(nullable: false),
                        Order = c.Int(nullable: false),
                        IsCurrent = c.Boolean(nullable: false),
                        Mandatory = c.Boolean(nullable: false),
                        TextContent = c.String(unicode: false),
                        EmailId = c.Guid(),
                        LinkedShowId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Email", t => t.EmailId)
                .ForeignKey("dbo.Shows", t => t.LinkedShowId)
                .Index(t => t.EmailId)
                .Index(t => t.LinkedShowId);
            
            CreateTable(
                "dbo.FormExhibitors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateCompleted = c.DateTime(),
                        FormId = c.Guid(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .ForeignKey("dbo.ExhibitorApplication", t => t.ExhibitorApplicationId, cascadeDelete: true)
                .Index(t => t.FormId)
                .Index(t => t.ExhibitorApplicationId);
            
            CreateTable(
                "dbo.FormFields",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DataType = c.Int(nullable: false),
                        Label = c.String(),
                        Name = c.String(),
                        TextContent = c.String(unicode: false),
                        Order = c.Int(nullable: false),
                        Mandatory = c.Boolean(nullable: false),
                        SpanTwoColumns = c.Boolean(nullable: false),
                        FormId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .Index(t => t.FormId);
            
            CreateTable(
                "dbo.FormFieldValues",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Value = c.String(),
                        FormFieldId = c.Guid(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormFields", t => t.FormFieldId, cascadeDelete: true)
                .ForeignKey("dbo.ExhibitorApplication", t => t.ExhibitorApplicationId, cascadeDelete: true)
                .Index(t => t.FormFieldId)
                .Index(t => t.ExhibitorApplicationId);
            
            CreateTable(
                "dbo.FormFieldItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Text = c.String(),
                        Value = c.String(),
                        Order = c.Int(nullable: false),
                        FormFieldId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormFields", t => t.FormFieldId, cascadeDelete: true)
                .Index(t => t.FormFieldId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormFieldItems", new[] { "FormFieldId" });
            DropIndex("dbo.FormFieldValues", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.FormFieldValues", new[] { "FormFieldId" });
            DropIndex("dbo.FormFields", new[] { "FormId" });
            DropIndex("dbo.FormExhibitors", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.FormExhibitors", new[] { "FormId" });
            DropIndex("dbo.Forms", new[] { "LinkedShowId" });
            DropIndex("dbo.Forms", new[] { "EmailId" });
            DropForeignKey("dbo.FormFieldItems", "FormFieldId", "dbo.FormFields");
            DropForeignKey("dbo.FormFieldValues", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.FormFieldValues", "FormFieldId", "dbo.FormFields");
            DropForeignKey("dbo.FormFields", "FormId", "dbo.Forms");
            DropForeignKey("dbo.FormExhibitors", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.FormExhibitors", "FormId", "dbo.Forms");
            DropForeignKey("dbo.Forms", "LinkedShowId", "dbo.Shows");
            DropForeignKey("dbo.Forms", "EmailId", "dbo.Email");
            DropTable("dbo.FormFieldItems");
            DropTable("dbo.FormFieldValues");
            DropTable("dbo.FormFields");
            DropTable("dbo.FormExhibitors");
            DropTable("dbo.Forms");
        }
    }
}
