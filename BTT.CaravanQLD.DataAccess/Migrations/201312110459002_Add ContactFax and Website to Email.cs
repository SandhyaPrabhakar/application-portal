namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContactFaxandWebsitetoEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmailSetting", "ContactFax", c => c.String());
            AddColumn("dbo.EmailSetting", "Website", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmailSetting", "Website");
            DropColumn("dbo.EmailSetting", "ContactFax");
        }
    }
}
