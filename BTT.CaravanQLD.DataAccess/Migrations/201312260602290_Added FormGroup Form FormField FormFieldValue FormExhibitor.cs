namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFormGroupFormFormFieldFormFieldValueFormExhibitor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FormFieldValues",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Value = c.String(),
                        FormFieldId = c.Guid(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormFields", t => t.FormFieldId, cascadeDelete: true)
                .ForeignKey("dbo.ExhibitorApplication", t => t.ExhibitorApplicationId, cascadeDelete: true)
                .Index(t => t.FormFieldId)
                .Index(t => t.ExhibitorApplicationId);
            
            CreateTable(
                "dbo.FormFields",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DataType = c.Int(nullable: false),
                        Label = c.String(),
                        Name = c.String(),
                        TextContent = c.String(unicode: false),
                        Order = c.Int(nullable: false),
                        Mandatory = c.Boolean(nullable: false),
                        SpanTwoColumns = c.Boolean(nullable: false),
                        FormId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .Index(t => t.FormId);
            
            CreateTable(
                "dbo.Forms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        DueDate = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        IsCurrent = c.Boolean(nullable: false),
                        Mandatory = c.Boolean(nullable: false),
                        FormGroupId = c.Guid(nullable: false),
                        EmailId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormGroups", t => t.FormGroupId, cascadeDelete: true)
                .ForeignKey("dbo.Email", t => t.EmailId)
                .Index(t => t.FormGroupId)
                .Index(t => t.EmailId);
            
            CreateTable(
                "dbo.FormGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FormExhibitors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateCompleted = c.DateTime(),
                        FormId = c.Guid(nullable: false),
                        ExhibitorApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Forms", t => t.FormId, cascadeDelete: true)
                .ForeignKey("dbo.ExhibitorApplication", t => t.ExhibitorApplicationId, cascadeDelete: true)
                .Index(t => t.FormId)
                .Index(t => t.ExhibitorApplicationId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.FormExhibitors", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.FormExhibitors", new[] { "FormId" });
            DropIndex("dbo.Forms", new[] { "EmailId" });
            DropIndex("dbo.Forms", new[] { "FormGroupId" });
            DropIndex("dbo.FormFields", new[] { "FormId" });
            DropIndex("dbo.FormFieldValues", new[] { "ExhibitorApplicationId" });
            DropIndex("dbo.FormFieldValues", new[] { "FormFieldId" });
            DropForeignKey("dbo.FormExhibitors", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.FormExhibitors", "FormId", "dbo.Forms");
            DropForeignKey("dbo.Forms", "EmailId", "dbo.Email");
            DropForeignKey("dbo.Forms", "FormGroupId", "dbo.FormGroups");
            DropForeignKey("dbo.FormFields", "FormId", "dbo.Forms");
            DropForeignKey("dbo.FormFieldValues", "ExhibitorApplicationId", "dbo.ExhibitorApplication");
            DropForeignKey("dbo.FormFieldValues", "FormFieldId", "dbo.FormFields");
            DropTable("dbo.FormExhibitors");
            DropTable("dbo.FormGroups");
            DropTable("dbo.Forms");
            DropTable("dbo.FormFields");
            DropTable("dbo.FormFieldValues");
        }
    }
}
