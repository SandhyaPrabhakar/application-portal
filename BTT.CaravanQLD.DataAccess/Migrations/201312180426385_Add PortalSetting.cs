namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPortalSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PortalSetting",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ExhibitorType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PortalSetting");
        }
    }
}
