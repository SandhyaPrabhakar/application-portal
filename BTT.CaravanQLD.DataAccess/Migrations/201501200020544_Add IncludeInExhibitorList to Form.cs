namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIncludeInExhibitorListtoForm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Forms", "IncludeInExhibitorList", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Forms", "IncludeInExhibitorList");
        }
    }
}
