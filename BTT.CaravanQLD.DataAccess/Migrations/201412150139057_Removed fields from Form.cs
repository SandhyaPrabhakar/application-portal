namespace BTT.CaravanQLD.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedfieldsfromForm : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Forms", "EmailId", "dbo.Email");
            DropIndex("dbo.Forms", new[] { "EmailId" });
            DropColumn("dbo.Forms", "DueDate");
            DropColumn("dbo.Forms", "Mandatory");
            DropColumn("dbo.Forms", "TextContent");
            DropColumn("dbo.Forms", "EmailId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Forms", "EmailId", c => c.Guid());
            AddColumn("dbo.Forms", "TextContent", c => c.String(unicode: false));
            AddColumn("dbo.Forms", "Mandatory", c => c.Boolean(nullable: false));
            AddColumn("dbo.Forms", "DueDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Forms", "EmailId");
            AddForeignKey("dbo.Forms", "EmailId", "dbo.Email", "Id");
        }
    }
}
