﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.DataAccess
{
    public class CaravanQLDContext : DbContext, IDisposable
    {
        static CaravanQLDContext()
        {
            Database.SetInitializer<CaravanQLDContext>(new MigrateDatabaseToLatestVersion<CaravanQLDContext, BTT.CaravanQLD.DataAccess.Migrations.Configuration>());
            //Database.SetInitializer<CaravanQLDContext>(new DropCreateDatabaseIfModelChanges<CaravanQLDContext>());
        }

        public DbSet<ExhibitorApplication> ExhibitorApplications { get; set; }
        public DbSet<SiteInfo> SiteInfos { get; set; }
        public DbSet<ProductInfo> ProductInfos { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<EmailLog> EmailLogs { get; set; }
        public DbSet<EmailSetting> EmailSettings { get; set; }
        public DbSet<ExhibitorApplicationEmail> ExhibitorApplicationEmails { get; set; }
        public DbSet<PortalSetting> PortalSettings { get; set; }
        public DbSet<Form> Forms { get; set; }
        public DbSet<FormField> FormFields { get; set; }
        public DbSet<FormFieldItem> FormFieldItems { get; set; }
        public DbSet<FormFieldValue> FormFieldValues { get; set; }
        public DbSet<FormExhibitor> FormExhibitors { get; set; }
        public DbSet<ContentPage> ContentPages { get; set; }
        public DbSet<Show> Shows { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
