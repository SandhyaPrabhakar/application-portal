﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("PortalSetting")]
    public class PortalSetting
    {
        public PortalSetting()
        {
            Id = Guid.NewGuid();
            ExhibitorType = ExhibitorType.NewExhibitors;
        }

        public Guid Id { get; set; }
        public ExhibitorType ExhibitorType { get; set; }

        public Guid? FormFieldId { get; set; }
        [Column("FormFieldId")]
        public virtual FormField SortByFormField { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }
    }
}
