﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("ExhibitorApplication")]
    public class ExhibitorApplication
    {
        public ExhibitorApplication()
        {
            Id = Guid.NewGuid();
            TradingName = "";
            ShowListingName = "";
            AddressLine1 = "";
            AddressLine2 = "";
            Suburb = "";
            State = "";
            Country = "";
            Postcode = "";
            ExhibitorPhone = "";
            ExhibitorFax = "";
            ExhibitorEmail = "";
            Website = "";
            Title = "";
            FirstName = "";
            Surname = "";
            Mobile = "";
            Phone = "";
            Email = "";
            ShowContactTitle = "";
            ShowContactFirstName = "";
            ShowContactSurname = "";
            ShowContactMobile = "";
            CQMemberType = "";
            CQMembershipID = "";
            InterstateMemberType = "";
            InterstateAssociation = "";
            NotesAndRequests = "";

            EmailLogs = new List<EmailLog>();
            ExhibitorApplicationEmails = new List<ExhibitorApplicationEmail>();
        }

        //Exhibitor Details
        [Key]
        public Guid Id { get; set; }
        [Display(Name = "Show ID")]
        public string ShowId { get; set; }
        [MaxLength(150)]
        public string TradingName { get; set; }
        [MaxLength(150)]
        public string ShowListingName { get; set; }
        [MaxLength(150)]
        public string AddressLine1 { get; set; }
        [MaxLength(150)]
        public string AddressLine2 { get; set; }
        [MaxLength(50), Display(Name = "Suburb / Town")]
        public string Suburb { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        [MaxLength(10)]
        public string Postcode { get; set; }
        [MaxLength(20)]
        public string ExhibitorPhone { get; set; }
        [MaxLength(20)]
        public string ExhibitorFax { get; set; }
        [MaxLength(100)]
        public string ExhibitorEmail { get; set; }
        [MaxLength(100)]
        public string Website { get; set; }

        //Contact Person Details
        [MaxLength(50), Display(Name = "Title / Position")]
        public string Title { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string Surname { get; set; }
        [MaxLength(20)]
        public string Mobile { get; set; }
        [MaxLength(20)]
        public string Phone { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        public bool IsShowContact { get; set; }
        [MaxLength(50), Display(Name = "Title / Position")]
        public string ShowContactTitle { get; set; }
        [MaxLength(50), Display(Name = "First Name")]
        public string ShowContactFirstName { get; set; }
        [MaxLength(50), Display(Name = "Surname")]
        public string ShowContactSurname { get; set; }
        [MaxLength(20), Display(Name = "Mobile")]
        public string ShowContactMobile { get; set; }

        //Member/CRVA Contributor
        public bool CQMember { get; set; }
        [Display(Name = "CQ Membership Type")]
        public string CQMemberType { get; set; }
        public string CQMembershipID { get; set; }
        [Display(Name = "Reciprocal Member")]
        public bool InterstateMember { get; set; }
        [Display(Name = "Reciprocal Membership Type")]
        public string InterstateMemberType { get; set; }
        [Display(Name = "Reciprocal Association")]
        public string InterstateAssociation { get; set; }
        [Display(Name = "CIAA Contribution")]
        public bool Contributed { get; set; }

        //Additional Notes and Requests
        [MaxLength(2055)]
        public string NotesAndRequests { get; set; }

        //Misc
        public DateTime? DateAdded { get; set; }
        public bool AcceptedTermsAndConditions { get; set; }
        public DateTime? DateSubmitted { get; set; }

        public SiteInfo SiteInfo { get; set; }

        public ProductInfo ProductInfo { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public virtual ICollection<EmailLog> EmailLogs { get; set; }
        public virtual ICollection<ExhibitorApplicationEmail> ExhibitorApplicationEmails { get; set; }
        public virtual ICollection<FormFieldValue> FormFieldValues { get; set; }
    }
}
