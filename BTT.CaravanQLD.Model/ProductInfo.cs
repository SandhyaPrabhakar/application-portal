﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("ProductInfo")]
    public class ProductInfo
    {
        public ProductInfo()
        {
            Products = "";
            CaravanBrand1 = "";
            CaravanBrand2 = "";
            CaravanBrand3 = "";
            CaravanBrand4 = "";
            CaravanBrand5 = "";
            CaravanBrand6 = "";
            CamperTrailerBrand1 = "";
            CamperTrailerBrand2 = "";
            CamperTrailerBrand3 = "";
            CamperTrailerBrand4 = "";
            CamperTrailerBrand5 = "";
            CamperTrailerBrand6 = "";
            MotorhomeBrand1 = "";
            MotorhomeBrand2 = "";
            MotorhomeBrand3 = "";
            MotorhomeBrand4 = "";
            FifthwheelerBrand1 = "";
            FifthwheelerBrand2 = "";
            FifthwheelerBrand3 = "";
            FifthwheelerBrand4 = "";
            AccessHeight = "";
        }

        //Products and Services
        [Key]
        [ForeignKey("ExhibitorApplication")]
        public Guid ExhibitorApplicationId { get; set; }
        public ExhibitorApplication ExhibitorApplication { get; set; }

        [MaxLength(2055)]
        public string Products { get; set; }
        [MaxLength(50)]
        public string CaravanBrand1 { get; set; }
        [MaxLength(50)]
        public string CaravanBrand2 { get; set; }
        [MaxLength(50)]
        public string CaravanBrand3 { get; set; }
        [MaxLength(50)]
        public string CaravanBrand4 { get; set; }
        [MaxLength(50)]
        public string CaravanBrand5 { get; set; }
        [MaxLength(50)]
        public string CaravanBrand6 { get; set; }
        [MaxLength(50)]
        public string CamperTrailerBrand1 { get; set; }
        [MaxLength(50)]
        public string CamperTrailerBrand2 { get; set; }
        [MaxLength(50)]
        public string CamperTrailerBrand3 { get; set; }
        [MaxLength(50)]
        public string CamperTrailerBrand4 { get; set; }
        [MaxLength(50)]
        public string CamperTrailerBrand5 { get; set; }
        [MaxLength(50)]
        public string CamperTrailerBrand6 { get; set; }
        [MaxLength(50)]
        public string MotorhomeBrand1 { get; set; }
        [MaxLength(50)]
        public string MotorhomeBrand2 { get; set; }
        [MaxLength(50)]
        public string MotorhomeBrand3 { get; set; }
        [MaxLength(50)]
        public string MotorhomeBrand4 { get; set; }
        [MaxLength(50)]
        public string FifthwheelerBrand1 { get; set; }
        [MaxLength(50)]
        public string FifthwheelerBrand2 { get; set; }
        [MaxLength(50)]
        public string FifthwheelerBrand3 { get; set; }
        [MaxLength(50)]
        public string FifthwheelerBrand4 { get; set; }
        [Display(Name = "Accessories / Parts")]
        public bool AccessoriesParts { get; set; }
        public bool AdvisoryServices { get; set; }
        [Display(Name = "Annexes / Awnings")]
        public bool AnnexesAwnings { get; set; }
        [Display(Name = "Art Unions / Fundraising")]
        public bool ArtUnionsFundraising { get; set; }
        [Display(Name = "Cabins / Homes")]
        public bool CabinsHomes { get; set; }
        public bool CamperTrailers { get; set; }
        [Display(Name = "Camping Equipment / Tents")]
        public bool CampingEquipmentTents { get; set; }
        [Display(Name = "Caravans / Pop Tops")]
        public bool CaravansPopTops { get; set; }
        [Display(Name = "Carports / Caravan Covers")]
        public bool CarportsCaravanCovers { get; set; }
        [Display(Name = "Clothing / Hats")]
        public bool ClothingHats { get; set; }
        public bool Clubs { get; set; }
        public bool Communication { get; set; }
        public bool FifthWheelers { get; set; }
        public bool Hire { get; set; }
        public bool Insurance { get; set; }
        [Display(Name = "Marine / Fishing / Boat Trailers")]
        public bool MarineFishingBoatTrailers { get; set; }
        [Display(Name = "Motorhomes / Campervans & Conversions")]
        public bool MotorhomesCampervansConversions { get; set; }
        public bool Publications { get; set; }
        public bool RoofTopCampers { get; set; }
        [Display(Name = "Slide On / Traytop Campers")]
        public bool SlideOnTraytopCampers { get; set; }
        [Display(Name = "Touring / Tourism / Caravan Parks")]
        public bool TouringTourismCaravanParks { get; set; }
        public bool TowingEquipment { get; set; }
        [Display(Name = "Vehicles / Vehicle Acc. / 4WD Acc.")]
        public bool VehiclesVehicleAccessories4WDAccessories { get; set; }
        [Display(Name = "Other Products / Services")]
        public bool OtherProductsServices { get; set; }
        public string AccessHeight { get; set; }
    }
}
