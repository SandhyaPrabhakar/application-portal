﻿using System.ComponentModel.DataAnnotations;
public enum PageType
{
    [Display(Name = "Custom Content")]
    Custom = 0,
    [Display(Name = "Application Content")]
    Application = 1,
    [Display(Name = "Confirmation Content")]
    Confirmation = 2,
    [Display(Name = "Login Content")]
    Login = 3
}