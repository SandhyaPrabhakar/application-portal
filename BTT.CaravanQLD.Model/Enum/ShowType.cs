﻿using System.ComponentModel.DataAnnotations;
public enum ShowType 
{
    [Display(Name="Main")]
    Main = 0,
    [Display(Name = "Sales")]
    Sales = 1
}