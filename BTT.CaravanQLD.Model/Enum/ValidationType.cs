﻿using System.ComponentModel.DataAnnotations;
public enum ValidationType
{
    [Display(Name = "None")]
    None = 0, 
    //This validation confirms that the text is a valid email address
    [Display(Name = "Email")]
    Email = 1,
    //This validation confirms that the text is a valid AU or NZ phone number
    [Display(Name = "Phone")]
    Phone = 2, 
    //This validation confirms that it's value is the same as another field in the same section
    [Display(Name = "Confirm Value With Field")]
    ConfirmSameAsField = 3, 
    //This validation resets all text to lower case
    [Display(Name = "All Lower Case")]
    AllLowerCase = 4,
    //This validation resets all text to upper case
    [Display(Name = "All Upper Case")]
    AllUpperCase = 5,
    //This validation resets all test to title case
    [Display(Name = "All Title Case")]
    AllTitleCase = 6
}