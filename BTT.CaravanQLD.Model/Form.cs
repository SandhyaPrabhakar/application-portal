﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    //This class is now being used as a Form Section
    public class Form
    {
        public Form()
        {
            Id = Guid.NewGuid();
            Name = "New Section";
            Order = 0;
            IsCurrent = true;
            IncludeInExhibitorList = true;
            FormFields = new List<FormField>();
        }

        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        [Display(Name = "Is Active")]
        public bool IsCurrent { get; set; }
        public bool IncludeInExhibitorList { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }

        public virtual ICollection<FormExhibitor> FormExhibitors { get; set; }
        public virtual ICollection<FormField> FormFields { get; set; }
    }
}
