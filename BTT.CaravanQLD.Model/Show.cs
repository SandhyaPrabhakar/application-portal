﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    public class Show
    {
        public Show()
        {
            Id = Guid.NewGuid();
            ShowType = ShowType.Main;
            Name = "New Show";
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddMonths(6);
            LinkIdentifier = "Show" + new Random().Next(1000, 9999);
            Contributor = "";
            ShellScheme = "";
            AdditionalNotesAndRequests = "";
            ConfirmationEmail = "";

            //Web Prospect Fields
            //Exhibitor Details
            ShowId = "";
            TradingName = "";
            ShowListingName = "";
            AddressLine1 = "";
            AddressLine2 = "";
            Suburb = "";
            Postcode = "";
            ExhibitorPhone = "";
            ExhibitorFax = "";
            ExhibitorEmail = "";
            Website = "";
            //Contact Details
            Title = "";
            FirstName = "";
            Surname = "";
            Mobile = "";
            Phone = "";
            Email = "";
            //Show Contact Person Details
            IsShowContact = "";
            ShowContactTitle = "";
            ShowContactFirstName = "";
            ShowContactSurname = "";
            ShowContactMobile = "";
            //Member / CIAA Contributor
            IsCQMember = "";
            CaravanQLDMember = "";
            CaravanQLDAssociate = "";
            IsInterstateMember = "";
            InterstateMember = "";
            InterstateMemberStatus = "";
            CRVAContributor = "";
            //Site Preference
            MarqueeRequired = "";
            StandChoice1 = "";
            StandChoice2 = "";
            StandChoice3 = "";
            FrontageRequired = "";
            DepthRequired = "";
            FrontageRequested = "";
            DepthRequested = "";
            LocationRequested = "";
            ShellSchemeWPF = "";
            AmplificationRequired = "";
            //Products / Services
            Products = "";
            CaravanBrand1 = "";
            CaravanBrand2 = "";
            CaravanBrand3 = "";
            CaravanBrand4 = "";
            CaravanBrand5 = "";
            CaravanBrand6 = "";
            CamperTrailerBrand1 = "";
            CamperTrailerBrand2 = "";
            CamperTrailerBrand3 = "";
            CamperTrailerBrand4 = "";
            CamperTrailerBrand5 = "";
            CamperTrailerBrand6 = "";
            MotorhomeBrand1 = "";
            MotorhomeBrand2 = "";
            MotorhomeBrand3 = "";
            MotorhomeBrand4 = "";
            FifthwheelerBrand1 = "";
            FifthwheelerBrand2 = "";
            FifthwheelerBrand3 = "";
            FifthwheelerBrand4 = "";
            AccessoriesParts = "";
            AdvisoryServices = "";
            AnnexesAwnings = "";
            ArtUnionsFundraising = "";
            CabinsHomes = "";
            CamperTrailers = "";
            CampingEquipmentTents = "";
            CaravansPopTops = "";
            CarportsCaravanCovers = "";
            ClothingHats = "";
            Clubs = "";
            Communication = "";
            FifthWheelers = "";
            Hire = "";
            Insurance = "";
            MarineFishingBoatTrailers = "";
            MotorhomesCampervansConversions = "";
            Publications = "";
            RoofTopCampers = "";
            SlideOnTrayTopCampers = "";
            TouringTourismCaravanParks = "";
            TowingEquipment = "";
            VehiclesVehicleAccessories4WD = "";
            OtherProductsServices = "";
            AccessHeightRequired = "";
            //Security Deposit Refund
            BankName = "";
            BranchName = "";
            BSB = "";
            AccountNo = "";
            AccountName = "";
            //Additional Notes / Requests
            NotesAndRequests = "";
            DateSubmitted = "";
        }

        [Key]
        public Guid Id { get; set; }
        public ShowType ShowType { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LinkIdentifier { get; set; }
        //These three fields are the text that appears on the application and confirmation forms
        public string Contributor { get; set; }
        public string ShellScheme { get; set; }
        public string AdditionalNotesAndRequests { get; set; }
        public string ConfirmationEmail { get; set; }

        //Web Prospect Fields
        //Exhibitor Details
        [Display(Name=("Show ID"))]
        public string ShowId { get; set; }
        public string TradingName { get; set; }
        public string ShowListingName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string ExhibitorPhone { get; set; }
        public string ExhibitorFax { get; set; }
        public string ExhibitorEmail { get; set; }
        public string Website { get; set; }
        
        //Contact Details
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        //Show Contact Person Details
        public string IsShowContact { get; set; }
        public string ShowContactTitle { get; set; }
        public string ShowContactFirstName { get; set; }
        public string ShowContactSurname { get; set; }
        public string ShowContactMobile { get; set; }

        //Member / CIAA Contributor
        public string IsCQMember { get; set; }
        public string CaravanQLDMember { get; set; }
        public string CaravanQLDAssociate { get; set; }
        public string CaravanQLDMembershipID { get; set; }
        public string IsInterstateMember { get; set; }
        public string InterstateMember { get; set; }
        public string InterstateAssociate { get; set; }
        public string InterstateMemberStatus { get; set; }
        public string CRVAContributor { get; set; }

        //Site Preference
        public string MarqueeRequired { get; set; }
        public string StandChoice1 { get; set; }
        public string StandChoice2 { get; set; }
        public string StandChoice3 { get; set; }
        public string FrontageRequired { get; set; }
        public string DepthRequired { get; set; }
        public string FrontageRequested { get; set; }
        public string DepthRequested { get; set; }
        public string LocationRequested { get; set; }
        [Display(Name = "Shell Scheme")]
        public string ShellSchemeWPF { get; set; }
        public string AmplificationRequired { get; set; }

        //Products / Services
        public string Products { get; set; }
        public string CaravanBrand1 { get; set; }
        public string CaravanBrand2 { get; set; }
        public string CaravanBrand3 { get; set; }
        public string CaravanBrand4 { get; set; }
        public string CaravanBrand5 { get; set; }
        public string CaravanBrand6 { get; set; }
        public string CamperTrailerBrand1 { get; set; }
        public string CamperTrailerBrand2 { get; set; }
        public string CamperTrailerBrand3 { get; set; }
        public string CamperTrailerBrand4 { get; set; }
        public string CamperTrailerBrand5 { get; set; }
        public string CamperTrailerBrand6 { get; set; }
        public string MotorhomeBrand1 { get; set; }
        public string MotorhomeBrand2 { get; set; }
        public string MotorhomeBrand3 { get; set; }
        public string MotorhomeBrand4 { get; set; }
        public string FifthwheelerBrand1 { get; set; }
        public string FifthwheelerBrand2 { get; set; }
        public string FifthwheelerBrand3 { get; set; }
        public string FifthwheelerBrand4 { get; set; }
        [Display(Name = "Accessories / Parts")]
        public string AccessoriesParts { get; set; }
        public string AdvisoryServices { get; set; }
        [Display(Name = "Annexes / Awnings")]
        public string AnnexesAwnings { get; set; }
        [Display(Name = "Art Unions / Fundraising")]
        public string ArtUnionsFundraising { get; set; }
        [Display(Name = "Cabins / Homes")]
        public string CabinsHomes { get; set; }
        public string CamperTrailers { get; set; }
        [Display(Name = "Camping Equipment / Tents")]
        public string CampingEquipmentTents { get; set; }
        [Display(Name = "Caravans / Pop Tops")]
        public string CaravansPopTops { get; set; }
        [Display(Name = "Carports / Caravan Covers")]
        public string CarportsCaravanCovers { get; set; }
        [Display(Name = "Clothing / Hats")]
        public string ClothingHats { get; set; }
        public string Clubs { get; set; }
        public string Communication { get; set; }
        public string FifthWheelers { get; set; }
        public string Hire { get; set; }
        public string Insurance { get; set; }
        [Display(Name = "Marine / Fishing / Boat Trailers")]
        public string MarineFishingBoatTrailers { get; set; }
        [Display(Name = "Motorhomes / Campervans & Conversions")]
        public string MotorhomesCampervansConversions { get; set; }
        public string Publications { get; set; }
        [Display(Name = "Roof-top Campers")]
        public string RoofTopCampers { get; set; }
        [Display(Name = "Slide-on / Traytop Campers")]
        public string SlideOnTrayTopCampers { get; set; }
        [Display(Name = "Touring / Tourism / Caravan Parks")]
        public string TouringTourismCaravanParks { get; set; }
        public string TowingEquipment { get; set; }
        [Display(Name = "Vehicles / Vehicle Accessories / 4WD")]
        public string VehiclesVehicleAccessories4WD { get; set; }
        [Display(Name = "Other Products / Services")]
        public string OtherProductsServices { get; set; }
        public string AccessHeightRequired { get; set; }

        //Security Deposit Refund
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BSB { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }

        //Additional Notes / Requests
        public string NotesAndRequests { get; set; }
        public string DateSubmitted { get; set; }

        //Option B Only
        public bool OnlyOptionB { get; set; }
        public string OptionBTitle { get; set; }
        public string OptionBText { get; set; }

        public virtual ICollection<ContentPage> ContentPages { get; set; }
        public virtual ICollection<Email> Emails { get; set; }
        public virtual ICollection<EmailLog> EmailLogs { get; set; }
        public virtual ICollection<EmailSetting> EmailSettings { get; set; }
        public virtual ICollection<ExhibitorApplication> ExhibitorApplications { get; set; }
        public virtual ICollection<PortalSetting> PortalSettings { get; set; }
    }
}
