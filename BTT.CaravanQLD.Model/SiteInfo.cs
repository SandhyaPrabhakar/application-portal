﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("SiteInfo")]
    public class SiteInfo
    {
        public SiteInfo()
        {
            StandChoice1 = "";
            StandChoice2 = "";
            StandChoice3 = "";
            Frontage = "";
            Depth = "";
            FrontageRequested = "";
            DepthRequested = "";

            SecurityDepositAmount = "";
            SecurityDepositCardType = "";
            SecurityDepositCardName = "";
            SecurityDepositCardNumber = "";
            SecurityDepositCardExpiry = "";
            SecurityDepositCCV = "";
            BSB = "";
            AccountNo = "";
            AccountName = "";
            BankName = "";
            BranchName = "";
        }

        //Site Preference
        [Key]
        [ForeignKey("ExhibitorApplication")]
        public Guid ExhibitorApplicationId { get; set; }
        public ExhibitorApplication ExhibitorApplication { get; set; }

        public bool MarqueeRequired { get; set; }
        [MaxLength(50)]
        public string StandChoice1 { get; set; }
        [MaxLength(50)]
        public string StandChoice2 { get; set; }
        [MaxLength(50)]
        public string StandChoice3 { get; set; }
        [Display(Name = "Frontage")]
        public string Frontage { get; set; }
        [Display(Name = "Depth")]
        public string Depth { get; set; }
        [Display(Name = "Frontage")]
        public string FrontageRequested { get; set; }
        [Display(Name = "Depth")]
        public string DepthRequested { get; set; }
        public bool Indoors { get; set; }
        public bool Outdoors { get; set; }
        public bool OutdoorsWithMarquee { get; set; }

        //Shell Scheme & Amplification
        public bool ShellScheme { get; set; }
        public bool Amplification { get; set; }

        //Security Deposit Payable
        public bool PayByCreditCard { get; set; }
        public bool PayByCheque { get; set; }
        [Display(Name = "Deposit Amount")]
        public string SecurityDepositAmount { get; set; }
        [Display(Name = "Card Type")]
        public string SecurityDepositCardType { get; set; }
        [MaxLength(100)]
        [Display(Name = "Card Name")]
        public string SecurityDepositCardName { get; set; }
        [MaxLength(19)]
        [Display(Name = "Card Number")]
        public string SecurityDepositCardNumber { get; set; }
        [MaxLength(10)]
        [Display(Name = "Expiry Date")]
        public string SecurityDepositCardExpiry { get; set; }
        [MaxLength(10)]
        [Display(Name = "CCV")]
        public string SecurityDepositCCV { get; set; }

        //Security Deposit Refund
        [MaxLength(10)]
        public string BSB { get; set; }
        [MaxLength(20)]
        public string AccountNo { get; set; }
        [MaxLength(50)]
        public string AccountName { get; set; }
        [MaxLength(50)]
        public string BankName { get; set; }
        [MaxLength(50)]
        public string BranchName { get; set; }
    }
}
