﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTT.CaravanQLD.Model
{
    [Table("EmailLog")]
    public class EmailLog
    {
        public EmailLog()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }
        public DateTime DateSent { get; set; }
        
        [Required]
        public Guid ExhibitorApplicationId { get; set; }
        [Column("ExhibitorApplicationId")]
        public virtual ExhibitorApplication ExhibitorApplication { get; set; }
        
        [Required]
        public Guid EmailId { get; set; }
        [Column("EmailId")]
        public virtual Email Email { get; set; }

        public Guid? LinkedShowId { get; set; }
        [Column("LinkedShowId")]
        public virtual Show LinkedShow { get; set; }
    }
}
