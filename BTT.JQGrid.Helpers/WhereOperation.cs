﻿// from http://www.codeproject.com/Articles/58357/Using-jqGrid-s-search-toolbar-with-multiple-filter

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.JQGrid.Helpers
{
    /// <summary>
    /// The supported operations in where-extension
    /// </summary>
    /// 

    // jqGrid search options
    // ['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc'] 
    // ['equal','not equal', 'less', 'less or equal','greater','greater or equal', 'begins with','does not begin with','is in','is not in','ends with','does not end with','contains','does not contain'] 
    public enum WhereOperation
    {
        [StringValue("eq")]
        Equal,
        [StringValue("ne")]
        NotEqual,
        [StringValue("lt")]
        LessThan,
        [StringValue("le")]
        LessThanOrEqual,
        [StringValue("gt")]
        GreaterThan,
        [StringValue("ge")]
        GreaterThanOrEqual,
        [StringValue("bw")] // not implemented
        BeginsWith,
        [StringValue("bw")] // not implemented
        DoesNotBeginWith,
        [StringValue("in")] // not implemented
        IsIn,
        [StringValue("ni")] // not implemented
        IsNotIn,
        [StringValue("ew")] // not implemented
        EndsWith,
        [StringValue("en")] // not implemented
        DoesNotEndWith,
        [StringValue("cn")]
        Contains,
        [StringValue("nc")] // not implemented
        DoesNotContain       
    }
}