﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class AddNewExhibitorModel
    {
        public ExhibitorApplication Exhibitor { get; set; }
        public List<Form> FormSections { get; set; }
        public List<FormFieldItem> FormFieldItems { get; set; }
    }
}