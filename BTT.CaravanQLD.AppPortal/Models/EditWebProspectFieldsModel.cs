﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EditWebProspectFieldsModel
    {
        public Show Show { get; set; }
        public List<Form> FormSections { get; set; }
    }
}