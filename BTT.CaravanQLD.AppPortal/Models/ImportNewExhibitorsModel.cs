﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ImportNewExhibitorsModel
    {
        public List<Form> FormSections { get; set; }
        public List<FormField> FormFieldsForImport { get; set; }
    }
}