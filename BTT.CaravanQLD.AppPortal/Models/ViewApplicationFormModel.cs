﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ViewApplicationFormModel
    {
        public List<Form> FormSections { get; set; }
        public List<FormFieldItem> FormFieldItems { get; set; }
        public string PageContent { get; set; }
    }
}