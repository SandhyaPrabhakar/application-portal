﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EmailSettingsModel
    {
        public EmailSetting EmailSetting { get; set; }
        public List<SelectListItem> EmailLogsDisplayFieldList { get; set; }
        public List<SelectListItem> ExhibitorEmailAndLoginFieldList { get; set; }
    }
}