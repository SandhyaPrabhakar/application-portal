﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ExhibitorListModel
    {
        public PortalSetting PortalSetting { get; set; }
        public List<Form> FormSections { get; set; }
        public List<ExhibitorApplication> Exhibitors { get; set; }
        public List<FormFieldValue> FormFieldValues { get; set; }
        public bool IsSearching { get; set; }
        public int GridWidth { get; set; }
    }
}