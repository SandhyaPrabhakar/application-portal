﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EditPageModel
    {
        public ContentPage ContentPage { get; set; }
    }
}