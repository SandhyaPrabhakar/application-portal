﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class EditFormFieldModel
    {
        public FormField FormField { get; set; }
        public List<SelectListItem> ConfirmFieldList { get; set; }
        public List<SelectListItem> TextFieldList { get; set; }
        public List<SelectListItem> MandatoryFormFieldList { get; set; }
    }
}