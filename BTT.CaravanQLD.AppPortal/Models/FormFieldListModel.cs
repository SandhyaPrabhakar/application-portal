﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class FormFieldListModel
    {
        public Form Form { get; set; }
        public List<FormField> FormFields { get; set; }
    }
}