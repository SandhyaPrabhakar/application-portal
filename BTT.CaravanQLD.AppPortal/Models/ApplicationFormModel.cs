﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ApplicationFormModel
    {
        public bool ApplicationAlreadySubmitted { get; set; }
        public ExhibitorApplication Exhibitor { get; set; }
        public List<Form> FormSections { get; set; }
        public List<FormFieldItem> FormFieldItems { get; set; }
        public List<FormFieldValue> FormFieldValues { get; set; }
        public bool ClearModelState { get; set; }
        public bool ValidateAppForm { get; set; }
    }
}