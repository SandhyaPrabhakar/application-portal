﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ChooseShowModel
    {
        public List<Show> Shows { get; set; }
        public string Show { get; set; }
    }
}