﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class FormFieldItemListModel
    {
        public List<FormFieldItem> FormFieldItems { get; set; }
        public FormField FormField { get; set; }
    }
}