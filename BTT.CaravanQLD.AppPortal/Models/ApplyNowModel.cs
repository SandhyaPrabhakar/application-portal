﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ApplyNowModel
    {
        public bool IsPortalAdmin { get; set; }
        public ContentPage Page { get; set; }
    }
}