﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class ExhibitorsModel
    {
        public bool ForwardToExhibitor { get; set; }
        public Guid ExhibitorId { get; set; }
    }
}