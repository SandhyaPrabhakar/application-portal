﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTT.CaravanQLD.AppPortal.Models
{
    public class AddNewShowModel
    {
        public List<Show> Shows { get; set; }
        public string Show { get; set; }
    }
}