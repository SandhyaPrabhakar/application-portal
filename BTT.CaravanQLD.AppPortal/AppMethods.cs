﻿using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal
{
    public class AppMethods
    {
        public static List<SelectListItem> CreateListOfStates()
        {
            var stateList = new List<SelectListItem>();
            stateList.Add(new SelectListItem() { Text = "", Value = "" });
            stateList.Add(new SelectListItem() { Text = "ACT - Australian Capital Territory", Value = "ACT" });
            stateList.Add(new SelectListItem() { Text = "NSW - New South Wales", Value = "NSW" });
            stateList.Add(new SelectListItem() { Text = "NT - Northern Territory", Value = "NT" });
            stateList.Add(new SelectListItem() { Text = "NZ - New Zealand", Value = "NZ" });
            stateList.Add(new SelectListItem() { Text = "QLD - Queensland", Value = "QLD" });
            stateList.Add(new SelectListItem() { Text = "SA - South Australia", Value = "SA" });
            stateList.Add(new SelectListItem() { Text = "TAS - Tasmania", Value = "TAS" });
            stateList.Add(new SelectListItem() { Text = "VIC - Victoria", Value = "VIC" });
            stateList.Add(new SelectListItem() { Text = "WA - Western Australia", Value = "WA" });
            return stateList;
        }

        public static List<SelectListItem> CreateListOfAssociations()
        {
            var assoList = new List<SelectListItem>();
            assoList.Add(new SelectListItem() { Text = "", Value = "" });
            assoList.Add(new SelectListItem() { Text = "Caravan Parks Qld", Value = "Caravan Parks Qld" });
            assoList.Add(new SelectListItem() { Text = "CCIA of NSW", Value = "CCIA of NSW" });
            assoList.Add(new SelectListItem() { Text = "CIA Victoria", Value = "CIA Victoria" });
            assoList.Add(new SelectListItem() { Text = "Caravan Industry WA", Value = "Caravan Industry WA" });
            assoList.Add(new SelectListItem() { Text = "CCIA of SA", Value = "CCIA of SA" });
            assoList.Add(new SelectListItem() { Text = "Vic Parks", Value = "Vic Parks" });
            assoList.Add(new SelectListItem() { Text = "Caravan Parks of SA", Value = "Caravan Parks of SA" });
            assoList.Add(new SelectListItem() { Text = "Caravan Industry Tas", Value = "Caravan Industry Tas" });
            assoList.Add(new SelectListItem() { Text = "CPA NT", Value = "CPA NT" });
            assoList.Add(new SelectListItem() { Text = "CIAA - RVMAP", Value = "RCIAA - RVMAP" });
            return assoList;
        }

        public static string ReplaceSmartFields(Show currentShow, ExhibitorApplication exApp, List<Form> formSections, List<FormFieldValue> formValues, string text)
        {
            if (!String.IsNullOrEmpty(text))
            {
                text = text.Replace("%Current Show Title%", currentShow.Name);
                text = text.Replace("%Show ID%", exApp.ShowId);

                foreach (var formSection in formSections)
                {
                    foreach (var formField in formSection.FormFields.Where(x => x.DataType != FormDataType.ContentSection && x.DataType != FormDataType.TextField))
                    {
                        var formValue = formValues.FirstOrDefault(x => x.FormFieldId == formField.Id);
                        text = text.Replace("%" + formField.Name + "%", formValue.Value);
                    }
                }

                return text;
            }
            else
            {
                return "";
            }
        }

        public static string ConvertBoolToYesNo(bool value)
        {
            if (value)
                return "Yes";
            else
                return "No";
        }

        public static bool IsTextValid(string value, int maxLength)
        {
            if (!String.IsNullOrEmpty(value) && (maxLength == 0 || value.Length <= maxLength))
                return true;
            else
                return false;
        }

        public static bool IsLessThanMax(string value, int maxLength)
        {
            if (String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && (maxLength == 0 || value.Length <= maxLength)))
                return true;
            else
                return false;
        }

        public static bool IsRequiredLengthOnly(string value, int minLength, int maxLength)
        {
            if (String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && value.Length >= minLength && (maxLength == 0 || value.Length <= maxLength)))
                return true;
            else
                return false;
        }

        public static bool IsPhoneNumber(string value)
        {
            if (!String.IsNullOrEmpty(value) && (Regex.IsMatch(value, AppConstants.AustralianPhoneRegex) || Regex.IsMatch(value, AppConstants.NZPhoneRegex)))
                return true;
            else
                return false;
        }

        public static bool IsEmailAddress(string value)
        {
            if (!String.IsNullOrEmpty(value) && Regex.IsMatch(value, AppConstants.EmailRegex))
                return true;
            else
                return false;
        }

        public static bool IsUrl(string value)
        {
            if (!String.IsNullOrEmpty(value) && Regex.IsMatch(value, AppConstants.UrlRegex))
                return true;
            else
                return false;
        }

        public static bool IsSameEmail(string value, string valueToCompare)
        {
            if (!String.IsNullOrEmpty(value) && !String.IsNullOrEmpty(valueToCompare) && value == valueToCompare)
                return true;
            else
                return false;
        }

        public static bool IsBrandValid(string value)
        {
            if (String.IsNullOrEmpty(value) || (!String.IsNullOrEmpty(value) && value.Length <= 50))
                return true;
            else
                return false;
        }
    }
}