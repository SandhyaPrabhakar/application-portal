﻿function confirmDialog(dialogText, okFunc, cancelFunc, dialogTitle) {
    $('<div style="padding: 10px; max-width: 500px; word-wrap: break-word;">' + dialogText + '</div>').dialog({
        draggable: false,
        modal: true,
        resizable: false,
        width: 'auto',
        title: dialogTitle || 'Confirm',
        minHeight: 75,
        buttons: {
            OK: function () {
                if (typeof (okFunc) == 'function') { setTimeout(okFunc, 50); }
                $(this).dialog('destroy');
            },
            Cancel: function () {
                if (typeof (cancelFunc) == 'function') { setTimeout(cancelFunc, 50); }
                $(this).dialog('destroy');
            }
        }
    });
}

function infoDialog(dialogText, dialogTitle) {
    $('<div style="padding: 10px; max-width: 500px; word-wrap: break-word;">' + dialogText + '</div>').dialog({
        draggable: false,
        modal: true,
        resizable: false,
        width: 'auto',
        title: dialogTitle || 'Info',
        minHeight: 75,
        buttons: {
            OK: function () {
                $(this).dialog('destroy');
            }
        },
        closeText: ''
    });
}

function showNotification(message, type, timeout, layout) {
    // see http://needim.github.com/noty/

    type = (type === undefined) ? "information" : type;
    timeout = (timeout === undefined) ? 2000 : timeout;
    layout = (layout === undefined) ? "topCenter" : layout;

    //Close any open notys before creating another.
    $.noty.closeAll();

    var n = noty({
        layout: layout,
        theme: 'defaultTheme',
        text: message,
        type: type,
        timeout: timeout,
        closeWith: ['hover'],
        callback: {
            onShow: function () {
                window.setTimeout(function () { $.noty.closeAll(); }, timeout)
            }
        }
    });
};
