﻿$(document).ready(function () {
    $.ajaxSetup({
        cache: false
    });

    setupImportFileInput();
    clearSearchBoxes();

    //This function is to counter the ForwardToEmail from email logs
    checkIfEditEmailAndCreateEditor();
});

function setupImportFileInput() {
    $("#importFileInput").change(function () {
        if ($(this).val() != null && $(this).val() != '' && $(this).val().split('.').pop() != 'csv') {
            $("span[data-valmsg-for='importFileInput']").html("<span for='importFileInput'>You must pick a .csv file.</span>");
            $("span[data-valmsg-for='importFileInput']").removeClass('field-validation-valid field-validation-error').addClass('field-validation-error');
        }
        else {
            $("span[data-valmsg-for='importFileInput']").html(' ');
            $("span[data-valmsg-for='importFileInput']").removeClass('field-validation-valid field-validation-error').addClass('field-validation-valid');
        }
    });
}

function createCKEditor() {
    var editor = CKEDITOR.replace('ckeditor-page-content');
    CKFinder.setupCKEditor(editor, '../../Scripts/ckfinder/');
}

function createCKEditorForPages() {
    var editor = CKEDITOR.replace('ContentPage_TextContent', {
        width: 755,
        height: 150,
        extraPlugins: 'autogrow',
        autoGrow_minHeight: 150,
        autoGrow_onStartup: true,
        removePlugins: 'resize',
        filebrowserBrowseUrl: '../../Scripts/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserFlashBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserUploadUrl: '../../Scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Flash'
    });

    //CKFinder.setupCKEditor(editor, '../../Scripts/ckfinder/');
}

function getNewPageContentSubmitForm() {
    var editor = CKEDITOR.instances["ckeditor-page-content"];
    //Just a little trick to encode a HTML string
    var htmlString = $('<div/>').text(editor.getData()).html();
    $("#PageContent").val(htmlString);
    $("#ckEditorForm").submit();
}

function openCKFinder() {
    CKFinder.popup('../../Scripts/ckfinder/', null, null, null);
}

function createCKEditorForEmails() {
    var editor = CKEDITOR.replace('Email_Body', {
        contentsCss: " ",
        width: 900,
        height: 750,
        extraPlugins: 'smartfieldlist',
        filebrowserBrowseUrl: '../../Scripts/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserFlashBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserUploadUrl: '../../Scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Flash'
    });

    CKFinder.setupCKEditor(editor, '../../Scripts/ckfinder/');
}

function checkIfEditEmailAndCreateEditor() {
    var emailBody = $("#Email_Body");
    if (emailBody[0] != undefined) {
        createCKEditorForEmails();
    }
}

function updateLastRemindedDate(exhibitorId) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    //if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    today = dd + '/' + mm + '/' + yyyy;
    $("#reminder-" + exhibitorId).html(today);
}

function clearSearchBoxes() {
    $(".search-form input[type='text']").val("");
    createDatePickerForSearch();
    $(".search-form input[type='text']").keydown(function (e) {
        if (e.which == 13) {
            $(".search-form input[type='submit']").click();
        }
    })
}

function createDatePickerForSearch() {
    $(".date-input").datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText, inst) {
            $(".search-form").submit();
        }
    });
}

function createDatePickerForShows() {
    $(".date-input").datepicker({
        dateFormat: 'dd/mm/yy'
    });
}

function updateDateLastSent(id) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    //if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    today = dd + '/' + mm + '/' + yyyy;
    $('span#' + id).html(today);
}

function createCKEditorsForShow() {
    //var showContributor = $("#Show_Contributor")[0];

    //if (showContributor != undefined) {
    //    //Create the editor for the contributor section
    //    var firstEditor = CKEDITOR.replace('Show_Contributor', {
    //        width: 755,
    //        height: 150,
    //        extraPlugins: 'autogrow',
    //        autoGrow_minHeight: 150,
    //        autoGrow_onStartup: true,
    //        removePlugins: 'resize'
    //    });

    //    CKFinder.setupCKEditor(firstEditor, '../../Scripts/ckfinder/');
    //}

    ////Create the editor for the shell scheme section
    //var secondEditor = CKEDITOR.replace('Show_ShellScheme', {
    //    width: 755,
    //    height: 150,
    //    extraPlugins: 'autogrow',
    //    autoGrow_minHeight: 150,
    //    autoGrow_onStartup: true,
    //    removePlugins: 'resize'
    //});

    //CKFinder.setupCKEditor(secondEditor, '../../Scripts/ckfinder/');

    //Create the editor for the additional notes & requests section
    //var thirdEditor = CKEDITOR.replace('Show_AdditionalNotesAndRequests', {
    //    width: 755,
    //    height: 150,
    //    extraPlugins: 'autogrow',
    //    autoGrow_minHeight: 150,
    //    autoGrow_onStartup: true,
    //    removePlugins: 'resize'
    //});

    //CKFinder.setupCKEditor(thirdEditor, '../../Scripts/ckfinder/');

    //Create the editor for the shell scheme section
    var fourthEditor = CKEDITOR.replace('Show_ConfirmationEmail', {
        width: 755,
        height: 150,
        extraPlugins: 'autogrow',
        autoGrow_minHeight: 150,
        autoGrow_onStartup: true,
        removePlugins: 'resize',
        filebrowserBrowseUrl: '../../Scripts/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserFlashBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserUploadUrl: '../../Scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Flash'
    });

    CKFinder.setupCKEditor(fourthEditor, '../../Scripts/ckfinder/');

    //var fifthEditor = CKEDITOR.replace('Show_OptionBText', {
    //    width: 755,
    //    height: 150,
    //    extraPlugins: 'autogrow',
    //    autoGrow_minHeight: 150,
    //    autoGrow_onStartup: true,
    //    removePlugins: 'resize'
    //});

    //CKFinder.setupCKEditor(fifthEditor, '../../Scripts/ckfinder/');
}

//Form Functions

function setupFormForm() {
    resetFormDivWidth();
}

// Form Field Functions //

function reloadFormFieldList() {
    var currentForm = $("#FormField_FormId").val();
    if (currentForm != undefined) {
        $.ajax({
            url: "FormFieldList/" + currentForm,
            success: function (data) {
                $("#form-field-list").html(data);
                hookUpDataTypeDropDown();
            }
        });
    }
}

function hookUpDataTypeDropDown() {
    checkFormFieldDataType();

    $("#FormField_DataType").change(function () {
        checkFormFieldDataType();
    });

    //Validation Drop Down
    showHideConfirmationField();

    $("#FormField_ValidationType").change(function () {
        showHideConfirmationField();
    });

    //Madatory Checkbox & Fields
    if ($("#FormField_Mandatory").is(':checked'))
        $("#mandatory-only-if").show();  // checked
    else
        $("#mandatory-only-if").hide();

    $('#FormField_Mandatory').click(function () {
        $("#mandatory-only-if").toggle(this.checked);
    });
}

function checkFormFieldDataType() {
    $("#text-content-div").hide();
    $("#form-field-item-list").hide();
    $("#edit-form-field-item").html('');
    $("#mandatory-checkbox").hide();
    $("#span-two-columns-checkbox").hide();
    $("#validation-drop-down").hide();
    $("#text-field-drop-down").hide();

    var dataType = $("#FormField_DataType option:selected").val();
    if (dataType == 'ContentSection') {
        createCKEditorForFormField(100);
        $("#text-content-div").show();
    }
    else if (dataType == "DropDown" || dataType == "CheckboxArray" || dataType == "RadioButton") {
        $("#mandatory-checkbox").show();
        $("#span-two-columns-checkbox").show();
        showAndPopulateFormFieldItems();
    }
    else if (dataType == "TextField") {
        $("#span-two-columns-checkbox").show();
        $("#text-field-drop-down").show();
    }
    else if (dataType == "Checkbox") {
        $("#mandatory-checkbox").show();
        $("#span-two-columns-checkbox").show();
    }
    else {
        $("#mandatory-checkbox").show();
        $("#span-two-columns-checkbox").show();
        $("#validation-drop-down").show();
    }
}

function showHideConfirmationField() {
    var validationType = $("#FormField_ValidationType option:selected")[0].value;
    if (validationType == "ConfirmSameAsField") {
        $("#confirmation-field-drop-down").show();
    }
    else {
        $("#confirmation-field-drop-down").hide();
    }
}

function createCKEditorForFormField() {
    var editor = CKEDITOR.replace('FormField_TextContent', {
        width: 755,
        height: 150,
        toolbar: [
            ['Source', 'Undo', 'Redo'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
            ['NumberedList', 'BulletedList', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight'],
            ['Link', 'Unlink', 'Image'],
            ['Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor', 'Maximize']
        ],
        extraPlugins: 'autogrow',
        autoGrow_minHeight: 150,
        autoGrow_onStartup: true,
        removePlugins: 'resize',
        filebrowserBrowseUrl: '../../Scripts/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserFlashBrowseUrl: '../../Scripts/ckfinder/ckfinder.html?Type=',
        filebrowserUploadUrl: '../../Scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '../../Scripts/ckfinder/core/connector/asp/connector.php?command=QuickUpload&type=Flash'
    });

    CKFinder.setupCKEditor(editor, '../../Scripts/ckfinder/');
}

function reloadEditFormField() {
    var currentFormField = $("#FormField_Id").val();
    if (currentFormField != undefined) {
        $.ajax({
            url: "GetNewFormFieldOrder/" + currentFormField,
            success: function (data) {
                $("#FormField_Order").val(data);
            }
        })
    }
}

function showAndPopulateFormFieldItems() {
    var currentFormField = $("#FormField_Id").val();
    $.ajax({
        url: "FormFieldItemList/" + currentFormField,
        success: function (data) {
            $("#form-field-item-list").html(data);
            $("#form-field-item-list").show();
        }
    })
}

function reloadFormFieldItemList() {
    var currentFormField = $("#FormField_Id").val();
    if (currentFormField != undefined) {
        $.ajax({
            url: "FormFieldItemList/" + currentFormField,
            success: function (data) {
                $("#form-field-item-list").html(data);
            }
        })
    }
}

function updateFormDivWidth() {
    var width = $("#formGridWidth").val();
    $("#exhibitor-list").css("width", width);
    clearSearchBoxes();
}

function resetFormDivWidth() {
    $("#exhibitor-width").css("width", "");
}

function toggleExportCsv(exhibitorId) {
    //get the little csv image link
    var link = $("#" + exhibitorId);
    //get the export link
    var exportLink = $("#exportLink");
    //get the href part of the export link
    var href = exportLink.attr('href');

    if (link.hasClass('chosen')) {
        //remove the class
        link.removeClass('chosen');
        //remove the exhibitor from the list if there is more than one and in the middle
        var newHref = href.replace("|" + exhibitorId, '');
        //remove the exhibitor from the list if there is more than one and at the start
        newHref = newHref.replace(exhibitorId + "|", '');
        //if there was not more than one, this link of code will remove the exhibitor instead
        newHref = newHref.replace(exhibitorId, '');
        //remove the current exhibitor from the list and update the href part of the link
        exportLink.attr('href', newHref);
    }
    else {
        //add the class
        link.addClass('chosen');
        //get the href length
        var hrefLength = href.length;
        //get the starting index of the exhibitors ids
        var exhibitorIdsStart = href.indexOf('exhibitorsToExport=') + 19;
        //get the exhibitor ids
        var exhibitorIds = href.substr(exhibitorIdsStart, hrefLength - exhibitorIdsStart);
        //add the new exhibitor id to the list
        if (exhibitorIds == null || exhibitorIds == '') {
            exhibitorIds = exhibitorId;
        }
        else {
            exhibitorIds = exhibitorIds + "|" + exhibitorId;
        }
        //get the first part of the link
        var hrefFirstPart = href.substr(0, exhibitorIdsStart);
        //update the export link with the new href part
        exportLink.attr('href', hrefFirstPart + exhibitorIds);
    }
}

function updateDateLastSent(id) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    //if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    today = dd + '/' + mm + '/' + yyyy;
    $('span#' + id).html(today);
}
//@Ajax.ActionLink("Add Field", "CreateFormField/" + Model.Form.Id.ToString(), new AjaxOptions() { UpdateTargetId = "edit-form-field", OnSuccess = "reloadFormFieldList()", LoadingElementId = "waiting" })
function createFormField(formSectionId) {
    var formFieldId = generateUUID();
    $.ajax({
        url: "CreateFormField?formSectionId=" + formSectionId + "&formFieldId=" + formFieldId,
        success: function (data) {
            $("#edit-form-field").html(data);
            reloadFormFieldList();
        },
        complete: function () {
            setTimeout(function () {
                $.ajax({
                    url: "CreateFormFieldValues/" + formFieldId
                });
            }, 200);
        }
    });
}

function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};
