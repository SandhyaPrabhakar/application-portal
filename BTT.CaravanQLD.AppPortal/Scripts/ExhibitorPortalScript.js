﻿/* Exhibitor Portal Functions */

function openFileUploaderForExhibitor(showId, formFieldId) {
    var finder = new CKFinder();
    finder.basePath = '../../Scripts/ckfinder-ex/';
    finder.startupPath = 'Exhibitors:/' + showId + '/';
    finder.selectActionData = formFieldId;
    finder.selectActionFunction = function (fileUrl, data) {
        var filename = fileUrl.replace(/^.*[\\\/]/, '').replace(/%20/g, ' ');
        var formFieldId = data['selectActionData'];
        $("div#" + formFieldId).html(filename);
        $("input[name='" + formFieldId + "']").val(filename);
    };
    finder.popup();
}