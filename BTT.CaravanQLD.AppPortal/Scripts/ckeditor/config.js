/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.contentsCss = "../../Content/typography.css";
    config.width = 755;
    //config.resize_dir = 'both';
};
