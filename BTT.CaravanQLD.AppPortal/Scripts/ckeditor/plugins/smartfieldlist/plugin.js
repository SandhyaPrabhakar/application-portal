﻿//This is adapted from here:http://tizardsbriefcase.com/908/javascript/custom-dropdown-in-ckeditor-4

CKEDITOR.plugins.add('smartfieldlist',
{
    requires: ['richcombo'], //, 'styles' ],
    init: function (editor) {
        var config = editor.config,
           lang = editor.lang.format;
        var tags = []; //new Array();

        $.ajax({
            url: "GetSmartFieldArray/",
            cache: false,
            success: function (data) {
                $.each(data, function (i, smartField) {
                    tags.push([smartField[0], smartField[1], smartField[2]]);
                });
            }
        });

        // Create style objects for all defined styles.
        editor.ui.addRichCombo('smartfieldlist',
            {
                label: "Smart Fields",
                title: "Insert Smart Fields",
                voiceLabel: "Insert Smart Fields",
                className: 'cke_format',
                multiSelect: false,
                panel:
                {
                    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
                    voiceLabel: editor.lang.panelVoiceLabel
                },
                init: function () {
                    this.startGroup("Smart Fields");
                    //this.add('value', 'drop_text', 'drop_label');
                    for (var this_tag in tags) {
                        this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
                    }
                },
                onClick: function (value) {
                    editor.focus();
                    editor.fire('saveSnapshot');
                    editor.insertHtml(value);
                    editor.fire('saveSnapshot');
                }
            });
    }
});
