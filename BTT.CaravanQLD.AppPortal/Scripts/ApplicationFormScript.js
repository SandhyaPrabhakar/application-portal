﻿$(document).ready(function (){
    //showHideDetailSections();
    addCapitalizationFixToInputs();
    allCAPSToInputs();
    allLowerCaseToInputs();
    setInterval(saveAppFormTimer, 300000);
});

//function showShowContactDetails() {
//    $("#show-contact-details").show("slow");
//}

//function hideShowContactDetails() {
//    $("#show-contact-details").hide("slow");
//}

//function showHideShowContactDetails() {
//    //This is the yes radio button
//    var showContactDetails = $("#ExApp_IsShowContact")[0].checked;

//    if (showContactDetails == false) {
//        $("#show-contact-details").show("slow");
//    }
//    else {
//        $("#show-contact-details").hide("slow");
//    }
//}

//function showCaravanQLDMemberDetails() {
//    $("#caravan-queensland-member").show("slow");
//}

//function hideCaravanQLDMemberDetails() {
//    $("#caravan-queensland-member").hide("slow");
//}

//function showHideCaravanQLDMemberDetails() {
//    //This is the yes radio button
//    var showcaravanQLDMemberDetails = $("#ExApp_CQMember")[0].checked;

//    if (showcaravanQLDMemberDetails == true) {
//        $("#caravan-queensland-member").show("slow");
//    }
//    else {
//        $("#caravan-queensland-member").hide("slow");
//    }
//}

//function showReciprocalMemberDetails() {
//    $("#reciprocal-member").show("slow");
//}

//function hideReciprocalMemberDetails() {
//    $("#reciprocal-member").hide("slow");
//}

//function showHideReciprocalMemberDetails() {
//    //This is the yes radio button
//    var showReciprocalMemberDetails = $("#ExApp_InterstateMember")[0].checked;

//    if (showReciprocalMemberDetails == true) {
//        $("#reciprocal-member").show("slow");
//    }
//    else {
//        $("#reciprocal-member").hide("slow");
//    }
//}

//function showHideCreditCardDetails() {
//    var payByCreditCard = $("#ExApp_SiteInfo_PayByCreditCard")[0].checked;

//    if (payByCreditCard == true) {
//        $("#credit-card-details").show("slow");
//        $("#ExApp_SiteInfo_PayByCheque")[0].checked = false;
//        showHideChequeDetails();
//    }
//    else {
//        $("#credit-card-details").hide("slow");
//    }
//}

//function showHideChequeDetails() {
//    var payByCheque = $("#ExApp_SiteInfo_PayByCheque")[0].checked;

//    if (payByCheque == true) {
//        $("#cheque-details").show("slow");
//        $("#ExApp_SiteInfo_PayByCreditCard")[0].checked = false;
//        showHideCreditCardDetails();
//    }
//    else {
//        $("#cheque-details").hide("slow");
//    }
//}

//function showHideDetailSections() {
//    //showHideCreditCardDetails();
//    //showHideChequeDetails();
//    showHideShowContactDetails();
//    showHideCaravanQLDMemberDetails();
//    showHideReciprocalMemberDetails();
//}

function addCapitalizationFixToInputs() {
    $(".capitalize").blur(function () {
        //this.value = titleCaps(this.value);
        this.value = toTitleCase(this.value);
    });
}

function allCAPSToInputs() {
    $(".all-caps").blur(function () {
        this.value = this.value.toUpperCase();
    });
}

function allLowerCaseToInputs() {
    $(".all-lower").blur(function () {
        this.value = this.value.toLowerCase();
    });
}

/*
 * Title Caps
 * 
 * Ported to JavaScript By John Resig - http://ejohn.org/ - 21 May 2008
 * Original by John Gruber - http://daringfireball.net/ - 10 May 2008
 * License: http://www.opensource.org/licenses/mit-license.php
 */

(function () {
    var small = "(aan|af|av|am|bij|da|de|del|della|den|der|des|di|dos|du|het|in|la|las|le|li|lo|onder|op|over|te|ten|ter|tot|uit|uijt|van|vom|von|voor|z|zum|zur|a|an|and|as|at|but|by|en|for|if|in|of|on|or|the|to|v[.]?|via|vs[.]?)";
    var punct = "([!\"#$%&'()*+,./:;<=>?@[\\\\\\]^_`{|}~-]*)";

    this.titleCaps = function (title) {
        //title = lower(title);

        var parts = [], split = /[:.;?!] |(?: |^)["Ò]/g, index = 0;

        while (true) {
            var m = split.exec(title);

            parts.push(title.substring(index, m ? m.index : title.length)
				.replace(/\b([A-Za-z][a-z.'Õ]*)\b/g, function (all) {
				    return /[A-Za-z]\.[A-Za-z]/.test(all) ? all : upper(all);
				})
				.replace(RegExp("\\b" + small + "\\b", "ig"), lower)
				.replace(RegExp("^" + punct + small + "\\b", "ig"), function (all, punct, word) {
				    return punct + upper(word);
				})
				.replace(RegExp("\\b" + small + punct + "$", "ig"), upper));

            index = split.lastIndex;

            if (m) parts.push(m[0]);
            else break;
        }

        return parts.join("").replace(/ V(s?)\. /ig, " v$1. ")
			.replace(/(['Õ])S\b/ig, "$1s")
			.replace(/\b(AT&T|Q&A)\b/ig, function (all) {
			    return all.toUpperCase();
			});
    };

    function lower(word) {
        return word.toLowerCase();
    }

    function upper(word) {
        return word.substr(0, 1).toUpperCase() + word.substr(1);
    }
})();

function toTitleCase(value) {
    var i, str, lowers, uppers;
    str = value.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });

    // Certain minor words should be left lowercase unless 
    // they are the first or last words in the string
    lowers = ['A', 'An', 'The', 'And', 'But', 'Or', 'For', 'Nor', 'As', 'At', 
    'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto', 'To', 'With',
    'Aan', 'Af', 'Av', 'Am', 'Bij', 'Da', 'De', 'Del', 'Della', 'Den', 'Der', 'Des', 'Di', 'Dos', 'Du', 'Het', 'La', 'Las',
    'Le', 'Li', 'Lo', 'Onder', 'Op', 'Over', 'Te', 'Ten', 'Ter', 'Tot', 'Uit', 'Uijt', 'Van', 'Vom', 'Von', 'Voor', 'Z', 'Zum', 'Zur', 'En', 'If', 'Of', 'Via']; 
    for (i = 0; i < lowers.length; i++)
        str = str.replace(new RegExp('\\s' + lowers[i] + '\\s', 'g'), 
            function(txt) {
                return txt.toLowerCase();
            });

    // Certain words such as initialisms or acronyms should be left uppercase
    uppers = ['Id', 'Tv', 'Itunes', 'Ipad', 'Iphone', 'Ipod'];
    for (i = 0; i < uppers.length; i++)
        str = str.replace(new RegExp('\\b' + uppers[i] + '\\b', 'g'), 
            uppers[i].toUpperCase());

    str = str.replace('ITUNES', 'iTunes');
    str = str.replace('IPAD', 'iPad');
    str = str.replace('IPHONE', 'iPhone');
    str = str.replace('IPOD', 'iPod');

    return str;
}

function saveAppFormTimer() {
    $("#ValidateAppForm").val('False');
    if ($('.validation-summary-errors')[0] == null) {
        $("#ClearModelState").val('True');
    } else {
        $("#ClearModelState").val('False');
    }

    $("#app-form").submit();
}

