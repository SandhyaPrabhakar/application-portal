﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult LoginBox()
        {
            var model = new LoginBoxModel();

            if (!String.IsNullOrEmpty(User.Identity.Name) && User.Identity.Name != AppConstants.AdminUserName)
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    model.Email = GetExhibitorEmailAndLogin(User.Identity.Name);
                }
            }

            return PartialView(model);
        }

        public string GetExhibitorEmailAndLogin(string ShowId)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = "";
                var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                if (!String.IsNullOrEmpty(emailSetting.ExhibitorEmailAndLoginField))
                {
                    var formFieldId = Guid.Parse(emailSetting.ExhibitorEmailAndLoginField);
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == ShowId && x.LinkedShowId == currentShow.Id);
                    var formFieldValue = db.FormFieldValues.First(x => x.FormFieldId == formFieldId && x.ExhibitorApplicationId == exhibitor.Id);

                    if (formFieldValue != null && !String.IsNullOrEmpty(formFieldValue.Value))
                    {
                        email = formFieldValue.Value;
                    }
                }

                return email;
            }
        }

        //public ActionResult Login()
        //{
        //    try
        //    {
        //        var url = Request.Params["ReturnUrl"];
        //        if (!String.IsNullOrEmpty(url))
        //        {
        //            if (url.Contains("DataPortal"))
        //                return RedirectToAction("AdminLogin");
        //        }
        //    }
        //    catch (Exception exc)
        //    {

        //    }

        //    using (var db = new CaravanQLDContext())
        //    {
        //        var loginPage = db.ContentPages.FirstOrDefault(x => x.IsCurrent && x.PageType == PageType.Login);
        //        var model = new AccountModel()
        //        {
        //            Page = loginPage
        //        };
        //        return View(model);
        //    }
        //}

        public ActionResult Login(string id)
        {
            try
            {
                var url = Request.Params["ReturnUrl"];
                if (!String.IsNullOrEmpty(url))
                {
                    if (url.Contains("DataPortal"))
                        return RedirectToAction("AdminLogin");
                }
            }
            catch (Exception exc)
            {

            }

            using (var db = new CaravanQLDContext())
            {
               var show = db.Shows.FirstOrDefault(x => x.LinkIdentifier == id);
                if (show != null && (DateTime.Now.Date >= show.StartDate.Date && DateTime.Now.Date <= show.EndDate.Date))
                {
                    Session[AppConstants.CurrentShowSession] = show.Id;
                    var loginPage = db.ContentPages.FirstOrDefault(x => x.LinkedShowId == show.Id && x.IsCurrent && x.PageType == PageType.Login);
                    var model = new AccountModel()
                    {
                        Page = loginPage
                    };
                    return View(model);
                }
                else
                {
                    return RedirectToAction("IncorrectUrl");
                }
            }
        }

        [HttpPost]
        public ActionResult Login(AccountModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var exhibitor = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.ShowId == model.ShowID).FirstOrDefault();
                    if (exhibitor == null || model.CompanyEmail != GetExhibitorEmailAndLogin(model.ShowID))
                    {
                        ModelState.AddModelError("ShowID", "Login details are incorrect.");
                        model.Page = db.ContentPages.FirstOrDefault(x => x.IsCurrent && x.PageType == PageType.Login && x.LinkedShowId == currentShow.Id);
                    }
                    else 
                    {
                        FormsAuthentication.SetAuthCookie(model.ShowID, false);
                        var pageToRedirectTo = db.ContentPages.Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).FirstOrDefault();
                        if (pageToRedirectTo == null)
                        {
                            return RedirectToAction("Index", "Apply_Now");
                        }
                        else
                        {
                            return RedirectToRoute(new { action = "Page", page = pageToRedirectTo.LinkName });
                        }
                    }
                }
            }

            return View(model);
        }

        public ActionResult AdminLogin()
        {
            var model = new AdminAccountModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult AdminLogin(AdminAccountModel model)
        {
            if (ModelState.IsValid)
            {
                //Caravan QLD for Data Portal
                if (model.UserName == AppConstants.AdminUserName)
                {
                    if (model.Password == AppConstants.AdminPassword)
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        return RedirectToAction("ChooseShow", "DataPortal");
                    }
                }
            }

            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account", new { id = GetCurrentShow().LinkIdentifier });
        }

        public ActionResult AdminLogout()
        {
            FormsAuthentication.SignOut();
            Session[AppConstants.CurrentShowAdminSession] = null;

            return RedirectToAction("AdminLogin", "Account");
        }

        public ActionResult IncorrectUrl()
        {
            return View();
        }

        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
