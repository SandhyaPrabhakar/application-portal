﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class Apply_NowController : Controller
    {
        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult Index()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var applicationPage = db.ContentPages.FirstOrDefault(x => x.IsCurrent && x.PageType == PageType.Application && x.LinkedShowId == currentShow.Id);

                var model = new ApplyNowModel() 
                {
                    Page = applicationPage,
                    IsPortalAdmin = User.Identity.Name == AppConstants.AdminUserName
                };

                return View(model);
            }
        }

        public ActionResult ApplicationForm()
        {
            //Need to also check if they have previously already submitted an application
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                var model = new ApplicationFormModel()
                {
                    Exhibitor = exhibitor,
                    FormSections = formSections,
                    FormFieldItems = formFieldItems,
                    FormFieldValues = formFieldValues,
                    ApplicationAlreadySubmitted = exhibitor.DateSubmitted != null ? true : false,
                    ValidateAppForm = true
                };

                return PartialView(model);
            }
        }


        [HttpPost]
        public ActionResult ApplicationForm(ApplicationFormModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                if (!ModelState.IsValid)
                {
                    ModelState.Clear();
                }

                var currentShow = GetCurrentShow();
                var request = Request;
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == model.Exhibitor.Id && x.LinkedShowId == currentShow.Id);
                var formFields = db.FormFields.Include("Form").Include("FormFieldItems").Where(x => x.Form.LinkedShowId == currentShow.Id && x.Form.IsCurrent).OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                exhibitor.AcceptedTermsAndConditions = model.Exhibitor.AcceptedTermsAndConditions;
                if (!exhibitor.AcceptedTermsAndConditions)
                {
                    ModelState.AddModelError("Exhibitor.AcceptedTermsAndConditions", "Please confirm that you have read and accepted the terms and conditions.");
                }

                foreach (var ff in formFields)
                {
                    var isFieldMandatory = ff.Mandatory;

                    //Check and see if the field needs based on conditions to be mandatory.
                    if (isFieldMandatory && ff.MandatoryFormFieldId != null)
                    {
                        var mandatoryFormFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.MandatoryFormFieldId && x.ExhibitorApplicationId == exhibitor.Id);
                        if (mandatoryFormFieldValue != null)
                        {
                            if (ff.MandatoryFormFieldValue == mandatoryFormFieldValue.Value)
                            {
                                isFieldMandatory = true;
                            }
                            else
                            {
                                isFieldMandatory = false;
                            }
                        }
                    }
                    

                    if (ff.DataType == FormDataType.TextBox || ff.DataType == FormDataType.TextArea)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);

                        if (!String.IsNullOrEmpty(newValue))
                        {
                            if (ff.ValidationType == ValidationType.Email)
                            {
                                formFieldValue.Value = newValue;

                                if (!AppMethods.IsEmailAddress(newValue))
                                {
                                    ModelState.AddModelError(ff.Id.ToString(), String.Format(AppConstants.ValidEmailAddressErrorMessage, ff.Label));
                                }
                            }
                            else if (ff.ValidationType == ValidationType.Phone)
                            {
                                formFieldValue.Value = newValue;
                                if (!AppMethods.IsPhoneNumber(newValue))
                                {
                                    ModelState.AddModelError(ff.Id.ToString(), String.Format(AppConstants.ValidPhoneNumberErrorMessage, ff.Label));
                                }
                            }
                            else if (ff.ValidationType == ValidationType.ConfirmSameAsField)
                            {
                                var confirmationFieldId = Guid.Parse(ff.ConfirmationField);
                                var confirmationField = db.FormFields.FirstOrDefault(x => x.Id == confirmationFieldId);
                                var confirmationFieldValue = db.FormFieldValues.FirstOrDefault(x => x.FormFieldId == confirmationField.Id && x.ExhibitorApplicationId == exhibitor.Id);
                                formFieldValue.Value = newValue;

                                if (confirmationFieldValue == null | newValue != confirmationFieldValue.Value)
                                {
                                    ModelState.AddModelError(ff.Id.ToString(), String.Format(AppConstants.CompareEmailsErrorMessage, ff.Label, confirmationField.Label));
                                }
                            }
                            else
                            {
                                formFieldValue.Value = newValue;
                            }
                        }
                        else if (String.IsNullOrEmpty(newValue) && isFieldMandatory)
                        {
                            formFieldValue.Value = "";

                            ModelState.AddModelError(ff.Id.ToString(), "Please enter a value.");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.DropDown)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && isFieldMandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please select an option from the list");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.Checkbox)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = ff.Label;
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.CheckboxArray)
                    {
                        var formFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == ff.Id).ToList();
                        var newValue = "";
                        for (int i = 0; i < formFieldItems.Count; i++)
                        {
                            var itemValue = request.Form.Get(formFieldItems[i].Id.ToString());
                            if (itemValue == "True" && String.IsNullOrEmpty(newValue))
                            {
                                newValue = String.Format("'{0}'", formFieldItems[i].Value);
                            }
                            else if (itemValue == "True" && !String.IsNullOrEmpty(newValue))
                            {
                                newValue += String.Format("|'{0}'", formFieldItems[i].Value);
                            }
                        }

                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);

                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && isFieldMandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please select at least one option from the list below");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.RadioButton)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && isFieldMandatory)
                        {
                            formFieldValue.Value = "";
                            ModelState.AddModelError(ff.Id.ToString(), "Please select one of these options");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                }

                db.SaveChanges();

                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFieldItems2 = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                model.Exhibitor = exhibitor;
                model.FormSections = formSections;
                model.FormFieldItems = formFieldItems2;
                model.FormFieldValues = formFieldValues;

                if (ModelState.IsValid && model.ValidateAppForm)
                {
                    return JavaScript("window.location = '" + Url.Action("Index", "Confirmation") + "'");
                }
                else if (!model.ValidateAppForm)
                {
                    //if was valid, but now is not - clear
                    if (model.ClearModelState)
                    {
                        ModelState.Clear();
                    }

                    model.ValidateAppForm = true;

                    return PartialView(model);
                }
                else
                {
                    return PartialView(model);
                }
            }

        }

            //ValidateExhibitorDetails(model);
            //ValidateContactPersonDetails(model);
            //if (currentShow.ShowType == ShowType.Main)
            //    ValidateMemberCRVAContributorDetails(model);
            //ValidateSitePreferences(model);
            //ValidateProductsAndServices(model);
            //ValidateTermsAndConditions(model);
            //if (currentShow.ShowType == ShowType.Main)
            //    ValidateSecurityDepositRefund(model);

            //if (ModelState.IsValid)
            //{
            //    using (var db = new CaravanQLDContext())
            //    {
            //        var exApp = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").FirstOrDefault(x => x.Id == model.ExApp.Id);
            //        exApp.TradingName = String.IsNullOrEmpty(model.ExApp.TradingName) ? "" : model.ExApp.TradingName;
            //        exApp.ShowListingName = String.IsNullOrEmpty(model.ExApp.ShowListingName) ? "" : model.ExApp.ShowListingName;
            //        exApp.AddressLine1 = String.IsNullOrEmpty(model.ExApp.AddressLine1) ? "" : model.ExApp.AddressLine1;
            //        exApp.AddressLine2 = String.IsNullOrEmpty(model.ExApp.AddressLine2) ? "" : model.ExApp.AddressLine2;
            //        exApp.Suburb = String.IsNullOrEmpty(model.ExApp.Suburb) ? "" : model.ExApp.Suburb;
            //        exApp.State = String.IsNullOrEmpty(model.ExApp.State) ? "" : model.ExApp.State;
            //        exApp.Country = String.IsNullOrEmpty(model.ExApp.Country) ? "" : model.ExApp.Country;
            //        exApp.Postcode = String.IsNullOrEmpty(model.ExApp.Postcode) ? "" : model.ExApp.Postcode;
            //        exApp.ExhibitorPhone = String.IsNullOrEmpty(model.ExApp.ExhibitorPhone) ? "" : model.ExApp.ExhibitorPhone;
            //        exApp.ExhibitorFax = String.IsNullOrEmpty(model.ExApp.ExhibitorFax) ? "" : model.ExApp.ExhibitorFax;
            //        exApp.ExhibitorEmail = String.IsNullOrEmpty(model.ExApp.ExhibitorEmail) ? "" : model.ExApp.ExhibitorEmail;
            //        exApp.Website = String.IsNullOrEmpty(model.ExApp.Website) ? "" : model.ExApp.Website;
                    
            //        exApp.Title = String.IsNullOrEmpty(model.ExApp.Title) ? "" : model.ExApp.Title;
            //        exApp.FirstName = String.IsNullOrEmpty(model.ExApp.FirstName) ? "" : model.ExApp.FirstName;
            //        exApp.Surname = String.IsNullOrEmpty(model.ExApp.Surname) ? "" : model.ExApp.Surname;
            //        exApp.Mobile = String.IsNullOrEmpty(model.ExApp.Mobile) ? "" : model.ExApp.Mobile;
            //        exApp.Phone = String.IsNullOrEmpty(model.ExApp.Phone) ? "" : model.ExApp.Phone;
            //        exApp.Email = String.IsNullOrEmpty(model.ExApp.Email) ? "" : model.ExApp.Email;
            //        exApp.IsShowContact = model.ExApp.IsShowContact;
            //        exApp.ShowContactTitle = String.IsNullOrEmpty(model.ExApp.ShowContactTitle) || model.ExApp.IsShowContact ? "" : model.ExApp.ShowContactTitle;
            //        exApp.ShowContactFirstName = String.IsNullOrEmpty(model.ExApp.ShowContactFirstName) || model.ExApp.IsShowContact ? "" : model.ExApp.ShowContactFirstName;
            //        exApp.ShowContactSurname = String.IsNullOrEmpty(model.ExApp.ShowContactSurname) || model.ExApp.IsShowContact ? "" : model.ExApp.ShowContactSurname;
            //        exApp.ShowContactMobile = String.IsNullOrEmpty(model.ExApp.ShowContactMobile) || model.ExApp.IsShowContact ? "" : model.ExApp.ShowContactMobile;
            //        if (currentShow.ShowType == ShowType.Main)
            //        {
            //            exApp.CQMember = model.ExApp.CQMember;
            //            exApp.CQMemberType = String.IsNullOrEmpty(model.ExApp.CQMemberType) ? "" : model.ExApp.CQMemberType;
            //            exApp.CQMembershipID = String.IsNullOrEmpty(model.ExApp.CQMembershipID) || !model.ExApp.CQMember ? "" : model.ExApp.CQMembershipID;
            //            exApp.InterstateMember = model.ExApp.InterstateMember;
            //            exApp.InterstateMemberType = String.IsNullOrEmpty(model.ExApp.InterstateMemberType) ? "" : model.ExApp.InterstateMemberType;
            //            exApp.InterstateAssociation = String.IsNullOrEmpty(model.ExApp.InterstateAssociation) || !model.ExApp.InterstateMember ? "" : model.ExApp.InterstateAssociation;
            //            exApp.Contributed = model.ExApp.Contributed;
            //        }

            //        exApp.SiteInfo.MarqueeRequired = model.ExApp.SiteInfo.MarqueeRequired;
            //        exApp.SiteInfo.StandChoice1 = String.IsNullOrEmpty(model.ExApp.SiteInfo.StandChoice1) ? "" : model.ExApp.SiteInfo.StandChoice1;
            //        exApp.SiteInfo.StandChoice2 = String.IsNullOrEmpty(model.ExApp.SiteInfo.StandChoice2) ? "" : model.ExApp.SiteInfo.StandChoice2;
            //        exApp.SiteInfo.StandChoice3 = String.IsNullOrEmpty(model.ExApp.SiteInfo.StandChoice3) ? "" : model.ExApp.SiteInfo.StandChoice3;
            //        exApp.SiteInfo.Frontage = String.IsNullOrEmpty(model.ExApp.SiteInfo.Frontage) ? "" : model.ExApp.SiteInfo.Frontage;
            //        exApp.SiteInfo.Depth = String.IsNullOrEmpty(model.ExApp.SiteInfo.Depth) ? "" : model.ExApp.SiteInfo.Depth;
            //        exApp.SiteInfo.FrontageRequested = String.IsNullOrEmpty(model.ExApp.SiteInfo.FrontageRequested) ? "" : model.ExApp.SiteInfo.FrontageRequested;
            //        exApp.SiteInfo.DepthRequested = String.IsNullOrEmpty(model.ExApp.SiteInfo.DepthRequested) ? "" : model.ExApp.SiteInfo.DepthRequested;
            //        exApp.SiteInfo.Indoors = model.ExApp.SiteInfo.Indoors;
            //        exApp.SiteInfo.Outdoors = model.ExApp.SiteInfo.Outdoors;
            //        exApp.SiteInfo.OutdoorsWithMarquee = model.ExApp.SiteInfo.OutdoorsWithMarquee;
            //        exApp.SiteInfo.ShellScheme = model.ExApp.SiteInfo.ShellScheme;
            //        exApp.SiteInfo.Amplification = model.ExApp.SiteInfo.Amplification;

            //        exApp.ProductInfo.Products = String.IsNullOrEmpty(model.ExApp.ProductInfo.Products) ? "" : model.ExApp.ProductInfo.Products;
            //        exApp.ProductInfo.CaravanBrand1 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CaravanBrand1) ? "" : model.ExApp.ProductInfo.CaravanBrand1;
            //        exApp.ProductInfo.CaravanBrand2 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CaravanBrand2) ? "" : model.ExApp.ProductInfo.CaravanBrand2;
            //        exApp.ProductInfo.CaravanBrand3 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CaravanBrand3) ? "" : model.ExApp.ProductInfo.CaravanBrand3;
            //        exApp.ProductInfo.CaravanBrand4 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CaravanBrand4) ? "" : model.ExApp.ProductInfo.CaravanBrand4;
            //        exApp.ProductInfo.CaravanBrand5 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CaravanBrand5) ? "" : model.ExApp.ProductInfo.CaravanBrand5;
            //        exApp.ProductInfo.CaravanBrand6 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CaravanBrand6) ? "" : model.ExApp.ProductInfo.CaravanBrand6;
            //        exApp.ProductInfo.CamperTrailerBrand1 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CamperTrailerBrand1) ? "" : model.ExApp.ProductInfo.CamperTrailerBrand1;
            //        exApp.ProductInfo.CamperTrailerBrand2 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CamperTrailerBrand2) ? "" : model.ExApp.ProductInfo.CamperTrailerBrand2;
            //        exApp.ProductInfo.CamperTrailerBrand3 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CamperTrailerBrand3) ? "" : model.ExApp.ProductInfo.CamperTrailerBrand3;
            //        exApp.ProductInfo.CamperTrailerBrand4 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CamperTrailerBrand4) ? "" : model.ExApp.ProductInfo.CamperTrailerBrand4;
            //        exApp.ProductInfo.CamperTrailerBrand5 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CamperTrailerBrand5) ? "" : model.ExApp.ProductInfo.CamperTrailerBrand5;
            //        exApp.ProductInfo.CamperTrailerBrand6 = String.IsNullOrEmpty(model.ExApp.ProductInfo.CamperTrailerBrand6) ? "" : model.ExApp.ProductInfo.CamperTrailerBrand6;
            //        exApp.ProductInfo.MotorhomeBrand1 = String.IsNullOrEmpty(model.ExApp.ProductInfo.MotorhomeBrand1) ? "" : model.ExApp.ProductInfo.MotorhomeBrand1;
            //        exApp.ProductInfo.MotorhomeBrand2 = String.IsNullOrEmpty(model.ExApp.ProductInfo.MotorhomeBrand2) ? "" : model.ExApp.ProductInfo.MotorhomeBrand2;
            //        exApp.ProductInfo.MotorhomeBrand3 = String.IsNullOrEmpty(model.ExApp.ProductInfo.MotorhomeBrand3) ? "" : model.ExApp.ProductInfo.MotorhomeBrand3;
            //        exApp.ProductInfo.MotorhomeBrand4 = String.IsNullOrEmpty(model.ExApp.ProductInfo.MotorhomeBrand4) ? "" : model.ExApp.ProductInfo.MotorhomeBrand4;
            //        exApp.ProductInfo.FifthwheelerBrand1 = String.IsNullOrEmpty(model.ExApp.ProductInfo.FifthwheelerBrand1) ? "" : model.ExApp.ProductInfo.FifthwheelerBrand1;
            //        exApp.ProductInfo.FifthwheelerBrand2 = String.IsNullOrEmpty(model.ExApp.ProductInfo.FifthwheelerBrand2) ? "" : model.ExApp.ProductInfo.FifthwheelerBrand2;
            //        exApp.ProductInfo.FifthwheelerBrand3 = String.IsNullOrEmpty(model.ExApp.ProductInfo.FifthwheelerBrand3) ? "" : model.ExApp.ProductInfo.FifthwheelerBrand3;
            //        exApp.ProductInfo.FifthwheelerBrand4 = String.IsNullOrEmpty(model.ExApp.ProductInfo.FifthwheelerBrand4) ? "" : model.ExApp.ProductInfo.FifthwheelerBrand4;

            //        exApp.ProductInfo.AccessoriesParts = model.ExApp.ProductInfo.AccessoriesParts;
            //        exApp.ProductInfo.AdvisoryServices = model.ExApp.ProductInfo.AdvisoryServices;
            //        exApp.ProductInfo.AnnexesAwnings = model.ExApp.ProductInfo.AnnexesAwnings;
            //        exApp.ProductInfo.ArtUnionsFundraising = model.ExApp.ProductInfo.ArtUnionsFundraising;
            //        exApp.ProductInfo.CabinsHomes = model.ExApp.ProductInfo.CabinsHomes;
            //        exApp.ProductInfo.CamperTrailers = model.ExApp.ProductInfo.CamperTrailers;
            //        exApp.ProductInfo.CampingEquipmentTents = model.ExApp.ProductInfo.CampingEquipmentTents;
            //        exApp.ProductInfo.CaravansPopTops = model.ExApp.ProductInfo.CaravansPopTops;
            //        exApp.ProductInfo.CarportsCaravanCovers = model.ExApp.ProductInfo.CarportsCaravanCovers;
            //        exApp.ProductInfo.ClothingHats = model.ExApp.ProductInfo.ClothingHats;
            //        exApp.ProductInfo.Clubs = model.ExApp.ProductInfo.Clubs;
            //        exApp.ProductInfo.Communication = model.ExApp.ProductInfo.Communication;
            //        exApp.ProductInfo.FifthWheelers = model.ExApp.ProductInfo.FifthWheelers;
            //        exApp.ProductInfo.Hire = model.ExApp.ProductInfo.Hire;
            //        exApp.ProductInfo.Insurance = model.ExApp.ProductInfo.Insurance;
            //        exApp.ProductInfo.MarineFishingBoatTrailers = model.ExApp.ProductInfo.MarineFishingBoatTrailers;
            //        exApp.ProductInfo.MotorhomesCampervansConversions = model.ExApp.ProductInfo.MotorhomesCampervansConversions;
            //        exApp.ProductInfo.Publications = model.ExApp.ProductInfo.Publications;
            //        exApp.ProductInfo.RoofTopCampers = model.ExApp.ProductInfo.RoofTopCampers;
            //        exApp.ProductInfo.SlideOnTraytopCampers = model.ExApp.ProductInfo.SlideOnTraytopCampers;
            //        exApp.ProductInfo.TouringTourismCaravanParks = model.ExApp.ProductInfo.TouringTourismCaravanParks;
            //        exApp.ProductInfo.TowingEquipment = model.ExApp.ProductInfo.TowingEquipment;
            //        exApp.ProductInfo.VehiclesVehicleAccessories4WDAccessories = model.ExApp.ProductInfo.VehiclesVehicleAccessories4WDAccessories;
            //        exApp.ProductInfo.OtherProductsServices = model.ExApp.ProductInfo.OtherProductsServices;
            //        exApp.ProductInfo.AccessHeight = String.IsNullOrEmpty(model.ExApp.ProductInfo.AccessHeight) ? "" : model.ExApp.ProductInfo.AccessHeight;

            //        if (currentShow.ShowType == ShowType.Main)
            //        {
            //            exApp.SiteInfo.BSB = String.IsNullOrEmpty(model.ExApp.SiteInfo.BSB) ? "" : model.ExApp.SiteInfo.BSB;
            //            exApp.SiteInfo.AccountNo = String.IsNullOrEmpty(model.ExApp.SiteInfo.AccountNo) ? "" : model.ExApp.SiteInfo.AccountNo;
            //            exApp.SiteInfo.AccountName = String.IsNullOrEmpty(model.ExApp.SiteInfo.AccountName) ? "" : model.ExApp.SiteInfo.AccountName;
            //            exApp.SiteInfo.BankName = String.IsNullOrEmpty(model.ExApp.SiteInfo.BankName) ? "" : model.ExApp.SiteInfo.BankName;
            //            exApp.SiteInfo.BranchName = String.IsNullOrEmpty(model.ExApp.SiteInfo.BranchName) ? "" : model.ExApp.SiteInfo.BranchName;
            //        }

            //        exApp.NotesAndRequests = String.IsNullOrEmpty(model.ExApp.NotesAndRequests) ? "" : model.ExApp.NotesAndRequests;
            //        exApp.AcceptedTermsAndConditions = model.ExApp.AcceptedTermsAndConditions;
            //        db.SaveChanges();

        //private void ValidateExhibitorDetails(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (!AppMethods.IsTextValid(app.TradingName, 150))
        //        ModelState.AddModelError("ExApp.TradingName", String.Format(AppConstants.TextErrorMessage, "Trading Name", "150"));

        //    if (!AppMethods.IsTextValid(app.ShowListingName, 150))
        //        ModelState.AddModelError("ExApp.ShowListingName", String.Format(AppConstants.TextErrorMessage, "Show Listing Name", "150"));

        //    if (!AppMethods.IsTextValid(app.AddressLine1, 150))
        //        ModelState.AddModelError("ExApp.AddressLine1", String.Format(AppConstants.TextErrorMessage, "Address Line 1", "150"));

        //    if (!AppMethods.IsLessThanMax(app.AddressLine2, 150))
        //        ModelState.AddModelError("ExApp.AddressLine2", String.Format(AppConstants.LessThanMaxErrorMessage, "Trading Name", "150"));

        //    if (!AppMethods.IsTextValid(app.Suburb, 50))
        //        ModelState.AddModelError("ExApp.Suburb", String.Format(AppConstants.TextErrorMessage, "Suburb / Town", "50"));

        //    if (!AppMethods.IsTextValid(app.State, 50))
        //        ModelState.AddModelError("ExApp.State", String.Format(AppConstants.TextErrorMessage, "State", "50"));

        //    if (!AppMethods.IsTextValid(app.Country, 50))
        //        ModelState.AddModelError("ExApp.Country", String.Format(AppConstants.TextErrorMessage, "Country", "50"));

        //    if (!AppMethods.IsTextValid(app.Postcode, 10))
        //        ModelState.AddModelError("ExApp.Postcode", String.Format(AppConstants.TextErrorMessage, "Postcode", "10"));

        //    if (!AppMethods.IsPhoneNumber(app.ExhibitorPhone))
        //        ModelState.AddModelError("ExApp.ExhibitorPhone", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Exhibitor Phone"));

        //    if (!String.IsNullOrEmpty(app.ExhibitorFax) && !AppMethods.IsPhoneNumber(app.ExhibitorFax))
        //        ModelState.AddModelError("ExApp.ExhibitorFax", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Exhibitor Fax"));

        //    if (!AppMethods.IsEmailAddress(app.ExhibitorEmail))
        //        ModelState.AddModelError("ExApp.ExhibitorEmail", String.Format(AppConstants.ValidEmailAddressErrorMessage, "Exhibitor Email"));

        //    if (AppMethods.IsEmailAddress(app.ExhibitorEmail) && !AppMethods.IsSameEmail(model.ConfirmExhibitorEmail, app.ExhibitorEmail))
        //        ModelState.AddModelError("ConfirmExhibitorEmail", String.Format(AppConstants.CompareEmailsErrorMessage, "Exhibitor Email"));

        //    if (!String.IsNullOrEmpty(app.Website) && !AppMethods.IsUrl(app.Website))
        //        ModelState.AddModelError("ExApp.Website", String.Format(AppConstants.UrlErrorMessage, "Website"));
        //}

        //private void ValidateContactPersonDetails(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (!AppMethods.IsTextValid(app.Title, 50))
        //        ModelState.AddModelError("ExApp.Title", String.Format(AppConstants.TextErrorMessage, "Title / Position", "50"));

        //    if (!AppMethods.IsTextValid(app.FirstName, 50))
        //        ModelState.AddModelError("ExApp.FirstName", String.Format(AppConstants.TextErrorMessage, "First Name", "50"));

        //    if (!AppMethods.IsTextValid(app.Surname, 50))
        //        ModelState.AddModelError("ExApp.Surname", String.Format(AppConstants.TextErrorMessage, "Surname", "50"));

        //    if (!AppMethods.IsPhoneNumber(app.Mobile))
        //        ModelState.AddModelError("ExApp.Mobile", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Mobile"));

        //    if (!AppMethods.IsPhoneNumber(app.Phone))
        //        ModelState.AddModelError("ExApp.Phone", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Phone"));

        //    if (!AppMethods.IsEmailAddress(app.Email))
        //        ModelState.AddModelError("ExApp.Email", String.Format(AppConstants.ValidEmailAddressErrorMessage, "Email"));

        //    if (AppMethods.IsEmailAddress(app.Email) && !AppMethods.IsSameEmail(model.ConfirmContactEmail, app.Email))
        //        ModelState.AddModelError("ConfirmContactEmail", String.Format(AppConstants.CompareEmailsErrorMessage, "Email"));

        //    //If there is a different show contact, we need further validation of the details
        //    if (!app.IsShowContact)
        //    {
        //        if (!AppMethods.IsTextValid(app.ShowContactTitle, 50))
        //            ModelState.AddModelError("ExApp.ShowContactTitle", String.Format(AppConstants.TextErrorMessage, "Title / Position", "50"));

        //        if (!AppMethods.IsTextValid(app.ShowContactFirstName, 50))
        //            ModelState.AddModelError("ExApp.ShowContactFirstName", String.Format(AppConstants.TextErrorMessage, "First Name", "50"));

        //        if (!AppMethods.IsTextValid(app.ShowContactSurname, 50))
        //            ModelState.AddModelError("ExApp.ShowContactSurname", String.Format(AppConstants.TextErrorMessage, "Surname", "50"));

        //        if (!AppMethods.IsPhoneNumber(app.ShowContactMobile))
        //            ModelState.AddModelError("ExApp.ShowContactMobile", String.Format(AppConstants.ValidPhoneNumberErrorMessage, "Mobile"));
        //    }
        //}

        //private void ValidateMemberCRVAContributorDetails(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (app.CQMember)
        //    {
        //        if (String.IsNullOrEmpty(app.CQMemberType))
        //            ModelState.AddModelError("ExApp.CQMemberType", AppConstants.MembershipTypeErrorMessage);

        //        if (!AppMethods.IsTextValid(app.CQMembershipID, 10))
        //            ModelState.AddModelError("ExApp.CQMembershipID", String.Format(AppConstants.RequiredErrorMessage, "CQ Membership ID"));
        //    }

        //    if (app.InterstateMember)
        //    {
        //        if (String.IsNullOrEmpty(app.InterstateMemberType))
        //            ModelState.AddModelError("ExApp.InterstateMemberType", AppConstants.MembershipTypeErrorMessage);

        //        if (!AppMethods.IsTextValid(app.InterstateAssociation, 2055))
        //            ModelState.AddModelError("ExApp.InterstateAssociation", String.Format(AppConstants.RequiredErrorMessage, "Association"));
        //    }
        //}

        ////This is missing the Marquee Required Field
        //private void ValidateSitePreferences(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (model.OptionA)
        //    {
        //        if (String.IsNullOrEmpty(app.SiteInfo.StandChoice1) || String.IsNullOrEmpty(app.SiteInfo.StandChoice2) || String.IsNullOrEmpty(app.SiteInfo.StandChoice3))
        //            ModelState.AddModelError("OptionA", "You have selected Option A and need to fill all 3 stand choices.");

        //        var isError = false;

        //        if (!String.IsNullOrEmpty(app.SiteInfo.Frontage))
        //        {
        //            decimal frontage;
        //            isError = !decimal.TryParse(app.SiteInfo.Frontage, out frontage);
        //        }

        //        if (!String.IsNullOrEmpty(app.SiteInfo.Depth))
        //        {
        //            decimal depth;
        //            isError = isError ? isError : !decimal.TryParse(app.SiteInfo.Depth, out depth);
        //        }

        //        if (isError)
        //            ModelState.AddModelError("ExApp.SiteInfo.Frontage", String.Format(AppConstants.DecimalErrorMEssage, "Frontage and Depth"));

        //        //Clear Option B Fields
        //        model.ExApp.SiteInfo.FrontageRequested = "";
        //        model.ExApp.SiteInfo.DepthRequested = "";
        //        model.ExApp.SiteInfo.Indoors = false;
        //        model.ExApp.SiteInfo.Outdoors = false;
        //    }
        //    else
        //    {
        //        var isError = false;

        //        if (!String.IsNullOrEmpty(app.SiteInfo.FrontageRequested))
        //        {
        //            decimal frontageRequested;
        //            isError = isError ? isError : !decimal.TryParse(app.SiteInfo.FrontageRequested, out frontageRequested);
        //        }

        //        if (!String.IsNullOrEmpty(app.SiteInfo.DepthRequested))
        //        {
        //            decimal depthRequested;
        //            isError = isError ? isError : !decimal.TryParse(app.SiteInfo.DepthRequested, out depthRequested);
        //        }

        //        if (!app.SiteInfo.Indoors && !app.SiteInfo.Outdoors && !app.SiteInfo.OutdoorsWithMarquee)
        //            isError = true;

        //        if (isError)
        //            ModelState.AddModelError("OptionA", "You have selected Option B and need to fill the frontage, depth, and stand location.");

        //        //Clear Option A Fields
        //        model.ExApp.SiteInfo.StandChoice1 = "";
        //        model.ExApp.SiteInfo.StandChoice2 = "";
        //        model.ExApp.SiteInfo.StandChoice3 = "";
        //        model.ExApp.SiteInfo.Frontage = "";
        //        model.ExApp.SiteInfo.Depth = "";
        //    }
        //}

        //private void ValidateProductsAndServices(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (!AppMethods.IsTextValid(app.ProductInfo.Products, 2055))
        //        ModelState.AddModelError("ExApp.ProductInfo.Products", String.Format(AppConstants.TextErrorMessage, "Products / Services", "2,055"));

        //    if (!String.IsNullOrEmpty(app.ProductInfo.Products))
        //        app.ProductInfo.Products.Replace("\r\n", " ");

        //    if (!AppMethods.IsBrandValid(app.ProductInfo.CaravanBrand1) || !AppMethods.IsBrandValid(app.ProductInfo.CaravanBrand2) || !AppMethods.IsBrandValid(app.ProductInfo.CaravanBrand3) || !AppMethods.IsBrandValid(app.ProductInfo.CaravanBrand4)
        //         || !AppMethods.IsBrandValid(app.ProductInfo.CaravanBrand5) || !AppMethods.IsBrandValid(app.ProductInfo.CaravanBrand6) || !AppMethods.IsBrandValid(app.ProductInfo.CamperTrailerBrand1) || !AppMethods.IsBrandValid(app.ProductInfo.CamperTrailerBrand2)
        //         || !AppMethods.IsBrandValid(app.ProductInfo.CamperTrailerBrand3) || !AppMethods.IsBrandValid(app.ProductInfo.CamperTrailerBrand4) || !AppMethods.IsBrandValid(app.ProductInfo.CamperTrailerBrand5)
        //         || !AppMethods.IsBrandValid(app.ProductInfo.CamperTrailerBrand6) || !AppMethods.IsBrandValid(app.ProductInfo.MotorhomeBrand1) || !AppMethods.IsBrandValid(app.ProductInfo.MotorhomeBrand2) || !AppMethods.IsBrandValid(app.ProductInfo.MotorhomeBrand3)
        //         || !AppMethods.IsBrandValid(app.ProductInfo.MotorhomeBrand4) || !AppMethods.IsBrandValid(app.ProductInfo.FifthwheelerBrand1) || !AppMethods.IsBrandValid(app.ProductInfo.FifthwheelerBrand2)
        //         || !AppMethods.IsBrandValid(app.ProductInfo.FifthwheelerBrand3) || !AppMethods.IsBrandValid(app.ProductInfo.FifthwheelerBrand4))
        //        ModelState.AddModelError("ExApp.ProductInfo", "One or more of the brands below contains more than 50 characters.");

        //    if (!app.ProductInfo.AccessoriesParts && !app.ProductInfo.AdvisoryServices && !app.ProductInfo.AnnexesAwnings && !app.ProductInfo.ArtUnionsFundraising && !app.ProductInfo.CabinsHomes && !app.ProductInfo.CamperTrailers
        //         && !app.ProductInfo.CampingEquipmentTents && !app.ProductInfo.CaravansPopTops && !app.ProductInfo.CarportsCaravanCovers && !app.ProductInfo.ClothingHats && !app.ProductInfo.Clubs
        //         && !app.ProductInfo.Communication && !app.ProductInfo.FifthWheelers && !app.ProductInfo.Hire && !app.ProductInfo.Insurance && !app.ProductInfo.MarineFishingBoatTrailers 
        //         && !app.ProductInfo.MotorhomesCampervansConversions && !app.ProductInfo.OtherProductsServices && !app.ProductInfo.Publications && !app.ProductInfo.RoofTopCampers
        //         && !app.ProductInfo.SlideOnTraytopCampers && !app.ProductInfo.TouringTourismCaravanParks && !app.ProductInfo.TowingEquipment && !app.ProductInfo.VehiclesVehicleAccessories4WDAccessories)
        //        ModelState.AddModelError("ExApp.ProductInfo.AccessoriesParts", "You need to pick at least one market segment from the list below.");

        //    if (!String.IsNullOrEmpty(app.ProductInfo.AccessHeight))
        //    {
        //        decimal accessHeight;
        //        if (!decimal.TryParse(app.ProductInfo.AccessHeight, out accessHeight))
        //            ModelState.AddModelError("ExApp.ProductInfo.AccessHeight", String.Format(AppConstants.DecimalErrorMEssage, "Access Height"));
        //    }

        //    if (!String.IsNullOrEmpty(app.NotesAndRequests) && app.NotesAndRequests.Length > 2055)
        //        ModelState.AddModelError("ExApp.NotesAndRequests", "Additional Notes / Requests must be less than 2,055 characters.");

        //    if (!String.IsNullOrEmpty(app.NotesAndRequests))
        //        app.NotesAndRequests.Replace("\r\n", " ");
        //}

        //private void ValidateTermsAndConditions(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (!app.AcceptedTermsAndConditions)
        //        ModelState.AddModelError("ExApp.AcceptedTermsAndConditions", "You must accept our terms and conditions to submit your application.");
        //}

        //private void ValidateSecurityDepositRefund(ApplicationFormModel model)
        //{
        //    var app = model.ExApp;
        //    if (!AppMethods.IsTextValid(app.SiteInfo.BSB, 10))
        //        ModelState.AddModelError("ExApp.SiteInfo.BSB", String.Format(AppConstants.TextErrorMessage, "BSB", "10"));
        //    if (!AppMethods.IsTextValid(app.SiteInfo.AccountNo, 20))
        //        ModelState.AddModelError("ExApp.SiteInfo.AccountNo", String.Format(AppConstants.TextErrorMessage, "Account No", "20"));
        //    if (!AppMethods.IsTextValid(app.SiteInfo.AccountName, 50))
        //        ModelState.AddModelError("ExApp.SiteInfo.AccountName", String.Format(AppConstants.TextErrorMessage, "Account Name", "50"));
        //    if (!AppMethods.IsTextValid(app.SiteInfo.BankName, 50))
        //        ModelState.AddModelError("ExApp.SiteInfo.BankName", String.Format(AppConstants.TextErrorMessage, "Bank Name", "50"));
        //    if (!AppMethods.IsLessThanMax(app.SiteInfo.BranchName, 50))
        //        ModelState.AddModelError("ExApp.SiteInfo.BranchName", String.Format(AppConstants.LessThanMaxErrorMessage, "Branch Name", "50"));
        //}

    }
}
