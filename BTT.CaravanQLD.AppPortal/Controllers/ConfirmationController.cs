﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class ConfirmationController : Controller
    {
        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult Index()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var confirmationPage = db.ContentPages.FirstOrDefault(x => x.IsCurrent && x.PageType == PageType.Confirmation && x.LinkedShowId == currentShow.Id);
                var exApp = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var model = new ConfirmationModel()
                {
                    HasSubmittedApp = exApp.DateSubmitted == null ? false : true,
                    Page = confirmationPage
                };

                return View(model);
            }
        }

        public ActionResult ConfirmationForm()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == User.Identity.Name && x.LinkedShowId == currentShow.Id);
                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                var model = new ConfirmationFormModel()
                {
                    Exhibitor = exhibitor,
                    FormSections = formSections,
                    FormFieldItems = formFieldItems,
                    FormFieldValues = formFieldValues,
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult ConfirmationForm(ConfirmationFormModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").FirstOrDefault(x => x.Id == model.Exhibitor.Id);
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                exhibitor.DateSubmitted = DateTime.Now;
                SendWebProspectEmail(exhibitor, emailSettings);
                SendExhibitorConfirmationEmail(exhibitor, emailSettings);
                db.SaveChanges();

                return RedirectToAction("Success");
            }
        }

        private void SendWebProspectEmail(ExhibitorApplication exApp, EmailSetting emailSettings)
        {
            #if Debug
                SmtpClient client = new SmtpClient("mail.bigpond.com");
            #else
                SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                //SmtpClient client = new SmtpClient("mail.bigpond.com");
            #endif
            MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
            MailAddress to = new MailAddress(emailSettings.ApplicationEmailAddress);
            MailMessage message = new MailMessage(from, to);
            message.IsBodyHtml = true;
            message.Body = GetWebProspectEmail(exApp);
            message.Subject = "New Application - Caravanning Queensland";
            client.Send(message);
        }

        private string GetWebProspectEmail(ExhibitorApplication exApp)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = db.Shows.Where(x => x.Id == exApp.LinkedShowId).FirstOrDefault();
                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IsCurrent).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exApp.Id).ToList();

                string messageContents = "";

                messageContents += String.Format("//{0}{1}", "Show ID", InsertNewLine());
                messageContents += String.Format("{0}:{1}{2}", currentShow.ShowId, exApp.ShowId, InsertNewLine());
                messageContents += String.Format("{0}:{1}{2}", currentShow.DateSubmitted, Convert.ToDateTime(exApp.DateSubmitted).ToString("dd/MM/yyyy"), InsertNewLine());

                foreach (var formSection in formSections)
                {
                    //messageContents += String.Format("//{0}{1}", formSection.Name.Replace("/", " "), InsertNewLine());

                    foreach (var formField in formSection.FormFields.OrderBy(x => x.Order))
                    {
                        if (formField.DataType != FormDataType.ContentSection && !String.IsNullOrEmpty(formField.WebProspectFieldName))
                        {
                            var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == formField.Id);

                            if (formField.DataType == FormDataType.CheckboxArray)
                            {
                                var formFieldValueText = formFieldValue.Value.Replace("'", "").Replace("|", ", ");
                                messageContents += @String.Format("{0}:<span>{1}</span>{2}", formField.WebProspectFieldName, formFieldValueText, InsertNewLine());
                            }
                            else
                            {
                                messageContents += String.Format("{0}:<span>{1}</span>{2}", formField.WebProspectFieldName, formFieldValue.Value, InsertNewLine());
                            }
                        }
                    }
                }

                return messageContents;

                //messageContents += "//Exhibitor Details" + InsertNewLine();
                //messageContents += currentShow.ShowId + ":" + exApp.ShowId + InsertNewLine();
                //messageContents += currentShow.TradingName + ":" + exApp.TradingName + InsertNewLine();
                //messageContents += currentShow.ShowListingName + ":" + exApp.ShowListingName + InsertNewLine();
                //messageContents += currentShow.AddressLine1 + ":" + exApp.AddressLine1 + InsertNewLine();
                //messageContents += currentShow.AddressLine2 + ":" + exApp.AddressLine2 + InsertNewLine();
                //messageContents += currentShow.Suburb + ":" + exApp.Suburb + InsertNewLine();
                //messageContents += currentShow.State + ":" + exApp.State + InsertNewLine();
                //messageContents += currentShow.Postcode + ":" + exApp.Postcode + InsertNewLine();
                //messageContents += currentShow.ExhibitorPhone + ":" + exApp.ExhibitorPhone + InsertNewLine();
                //messageContents += currentShow.ExhibitorFax + ":" + exApp.ExhibitorFax + InsertNewLine();
                //messageContents += currentShow.ExhibitorEmail + ":" + exApp.ExhibitorEmail + InsertNewLine();
                //messageContents += currentShow.Website + ":" + exApp.Website + InsertNewLine();
                ////Contact Person Details
                //messageContents += InsertNewLine() + "//Contact Person Details" + InsertNewLine();
                //messageContents += currentShow.Title + ":" + exApp.Title + InsertNewLine();
                //messageContents += currentShow.FirstName + ":" + exApp.FirstName + InsertNewLine();
                //messageContents += currentShow.Surname + ":" + exApp.Surname + InsertNewLine();
                //messageContents += currentShow.Mobile + ":" + exApp.Mobile + InsertNewLine();
                //messageContents += currentShow.Phone + ":" + exApp.Phone + InsertNewLine();
                //messageContents += currentShow.Email + ":" + exApp.Email + InsertNewLine();
                ////Show Contact Person Details
                //messageContents += InsertNewLine() + "//Show Contact Person Details" + InsertNewLine();
                //messageContents += currentShow.IsShowContact + ":" + ConvertBoolToYesNo(exApp.IsShowContact) + InsertNewLine();
                //messageContents += currentShow.ShowContactTitle + ":" + exApp.ShowContactTitle + InsertNewLine();
                //messageContents += currentShow.ShowContactFirstName + ":" + exApp.ShowContactFirstName + InsertNewLine();
                //messageContents += currentShow.ShowContactSurname + ":" + exApp.ShowContactSurname + InsertNewLine();
                //messageContents += currentShow.ShowContactMobile + ":" + exApp.ShowContactMobile + InsertNewLine();
                //if (GetCurrentShow().ShowType == ShowType.Main)
                //{
                //    //Member / CRVA Contributor
                //    messageContents += InsertNewLine() + "//Member / CIAA Contributor" + InsertNewLine();
                //    messageContents += currentShow.IsCQMember + ":" + ConvertBoolToYesNo(exApp.CQMember) + InsertNewLine();
                //    messageContents += currentShow.CaravanQLDMember + ":" + (!String.IsNullOrEmpty(exApp.CQMemberType) && exApp.CQMemberType == "Full Member" ? "Full Member" : "") + InsertNewLine();
                //    messageContents += currentShow.CaravanQLDAssociate + ":" + (!String.IsNullOrEmpty(exApp.CQMemberType) && exApp.CQMemberType == "Associate Member" ? "Associate Member" : "") + InsertNewLine();
                //    messageContents += currentShow.CaravanQLDMembershipID + ":" + exApp.CQMembershipID + InsertNewLine();
                //    messageContents += currentShow.IsInterstateMember + ":" + ConvertBoolToYesNo(exApp.InterstateMember) + InsertNewLine();
                //    messageContents += currentShow.InterstateMember + ":" + (!String.IsNullOrEmpty(exApp.InterstateMemberType) && exApp.InterstateMemberType == "Full Member" ? "Full Member" : "") + InsertNewLine();
                //    messageContents += currentShow.InterstateAssociate + ":" + (!String.IsNullOrEmpty(exApp.InterstateMemberType) && exApp.InterstateMemberType == "Associate Member" ? "Associate Member" : "") + InsertNewLine();
                //    messageContents += currentShow.InterstateMemberStatus + ":" + exApp.InterstateAssociation + InsertNewLine();
                //    messageContents += currentShow.CRVAContributor + ":" + ConvertBoolToYesNo(exApp.Contributed) + InsertNewLine();
                //}
                ////Site Preference
                //messageContents += InsertNewLine() + "//Site Preference" + InsertNewLine();
                //if (GetCurrentShow().ShowType == ShowType.Sales || GetCurrentShow().OnlyOptionB)
                //{
                //    messageContents += currentShow.MarqueeRequired + ":" + ConvertBoolToYesNo(exApp.SiteInfo.MarqueeRequired) + InsertNewLine();
                //}
                //messageContents += currentShow.StandChoice1 + ":" + exApp.SiteInfo.StandChoice1 + InsertNewLine();
                //messageContents += currentShow.StandChoice2 + ":" + exApp.SiteInfo.StandChoice2 + InsertNewLine();
                //messageContents += currentShow.StandChoice3 + ":" + exApp.SiteInfo.StandChoice3 + InsertNewLine();
                //messageContents += currentShow.FrontageRequired + ":" + exApp.SiteInfo.Frontage + InsertNewLine();
                //messageContents += currentShow.DepthRequired + ":" + exApp.SiteInfo.Depth + InsertNewLine();
                //messageContents += currentShow.FrontageRequested + ":" + exApp.SiteInfo.FrontageRequested + InsertNewLine();
                //messageContents += currentShow.DepthRequested + ":" + exApp.SiteInfo.DepthRequested + InsertNewLine();
                //messageContents += currentShow.LocationRequested + ":" + ReturnLocationRequested(exApp) + InsertNewLine() + InsertNewLine();
                //messageContents += currentShow.ShellSchemeWPF + ":" + ConvertBoolToYesNo(exApp.SiteInfo.ShellScheme) + InsertNewLine();
                //messageContents += currentShow.AmplificationRequired + ":" + ConvertBoolToYesNo(exApp.SiteInfo.Amplification) + InsertNewLine();
                ////Products / Services
                //messageContents += InsertNewLine() + "//Products / Services" + InsertNewLine();
                //messageContents += currentShow.Products + ":" + exApp.ProductInfo.Products + InsertNewLine();
                //messageContents += currentShow.CaravanBrand1 + ":" + exApp.ProductInfo.CaravanBrand1 + InsertNewLine();
                //messageContents += currentShow.CaravanBrand2 + ":" + exApp.ProductInfo.CaravanBrand2 + InsertNewLine();
                //messageContents += currentShow.CaravanBrand3 + ":" + exApp.ProductInfo.CaravanBrand3 + InsertNewLine();
                //messageContents += currentShow.CaravanBrand4 + ":" + exApp.ProductInfo.CaravanBrand4 + InsertNewLine();
                //messageContents += currentShow.CaravanBrand5 + ":" + exApp.ProductInfo.CaravanBrand5 + InsertNewLine();
                //messageContents += currentShow.CaravanBrand6 + ":" + exApp.ProductInfo.CaravanBrand6 + InsertNewLine();
                //messageContents += currentShow.CamperTrailerBrand1 + ":" + exApp.ProductInfo.CamperTrailerBrand1 + InsertNewLine();
                //messageContents += currentShow.CamperTrailerBrand2 + ":" + exApp.ProductInfo.CamperTrailerBrand2 + InsertNewLine();
                //messageContents += currentShow.CamperTrailerBrand3 + ":" + exApp.ProductInfo.CamperTrailerBrand3 + InsertNewLine();
                //messageContents += currentShow.CamperTrailerBrand4 + ":" + exApp.ProductInfo.CamperTrailerBrand4 + InsertNewLine();
                //messageContents += currentShow.CamperTrailerBrand5 + ":" + exApp.ProductInfo.CamperTrailerBrand5 + InsertNewLine();
                //messageContents += currentShow.CamperTrailerBrand6 + ":" + exApp.ProductInfo.CamperTrailerBrand6 + InsertNewLine();
                //messageContents += currentShow.MotorhomeBrand1 + ":" + exApp.ProductInfo.MotorhomeBrand1 + InsertNewLine();
                //messageContents += currentShow.MotorhomeBrand2 + ":" + exApp.ProductInfo.MotorhomeBrand2 + InsertNewLine();
                //messageContents += currentShow.MotorhomeBrand3 + ":" + exApp.ProductInfo.MotorhomeBrand3 + InsertNewLine();
                //messageContents += currentShow.MotorhomeBrand4 + ":" + exApp.ProductInfo.MotorhomeBrand4 + InsertNewLine();
                //messageContents += currentShow.FifthwheelerBrand1 + ":" + exApp.ProductInfo.FifthwheelerBrand1 + InsertNewLine();
                //messageContents += currentShow.FifthwheelerBrand2 + ":" + exApp.ProductInfo.FifthwheelerBrand2 + InsertNewLine();
                //messageContents += currentShow.FifthwheelerBrand3 + ":" + exApp.ProductInfo.FifthwheelerBrand3 + InsertNewLine();
                //messageContents += currentShow.FifthwheelerBrand4 + ":" + exApp.ProductInfo.FifthwheelerBrand4 + InsertNewLine();
                //messageContents += currentShow.AccessoriesParts + ":" + (exApp.ProductInfo.AccessoriesParts ? "Accessories / Parts" : "") + InsertNewLine();
                //messageContents += currentShow.AdvisoryServices + ":" + (exApp.ProductInfo.AdvisoryServices ? "Advisory Services" : "") + InsertNewLine();
                //messageContents += currentShow.AnnexesAwnings + ":" + (exApp.ProductInfo.AnnexesAwnings ? "Annexes / Awnings" : "") + InsertNewLine();
                //messageContents += currentShow.ArtUnionsFundraising + ":" + (exApp.ProductInfo.ArtUnionsFundraising ? "Art Unions / Fundraising" : "") + InsertNewLine();
                //messageContents += currentShow.CabinsHomes + ":" + (exApp.ProductInfo.CabinsHomes ? "Cabins / Homes" : "") + InsertNewLine();
                //messageContents += currentShow.CamperTrailers + ":" + (exApp.ProductInfo.CamperTrailers ? "Camper Trailers" : "") + InsertNewLine();
                //messageContents += currentShow.CampingEquipmentTents + ":" + (exApp.ProductInfo.CampingEquipmentTents ? "Camping Equipment / Tents" : "") + InsertNewLine();
                //messageContents += currentShow.CaravansPopTops + ":" + (exApp.ProductInfo.CaravansPopTops ? "Caravans / Pop Tops" : "") + InsertNewLine();
                //messageContents += currentShow.CarportsCaravanCovers + ":" + (exApp.ProductInfo.CarportsCaravanCovers ? "Carports / Caravan Covers" : "") + InsertNewLine();
                //messageContents += currentShow.ClothingHats + ":" + (exApp.ProductInfo.ClothingHats ? "Clothing / Hats" : "") + InsertNewLine();
                //messageContents += currentShow.Clubs + ":" + (exApp.ProductInfo.Clubs ? "Clubs" : "") + InsertNewLine();
                //messageContents += currentShow.Communication + ":" + (exApp.ProductInfo.Communication ? "Communication" : "") + InsertNewLine();
                //messageContents += currentShow.FifthWheelers + ":" + (exApp.ProductInfo.FifthWheelers ? "Fifth Wheelers" : "") + InsertNewLine();
                //messageContents += currentShow.Hire + ":" + (exApp.ProductInfo.Hire ? "Hire" : "") + InsertNewLine();
                //messageContents += currentShow.Insurance + ":" + (exApp.ProductInfo.Insurance ? "Insurance" : "") + InsertNewLine();
                //messageContents += currentShow.MarineFishingBoatTrailers + ":" + (exApp.ProductInfo.MarineFishingBoatTrailers ? "Marine / Fishing / Boat Trailers" : "") + InsertNewLine();
                //messageContents += currentShow.MotorhomesCampervansConversions + ":" + (exApp.ProductInfo.MotorhomesCampervansConversions ? "Motorhomes / Campervans & Conversions" : "") + InsertNewLine();
                //messageContents += currentShow.Publications + ":" + (exApp.ProductInfo.Publications ? "Publications" : "") + InsertNewLine();
                //messageContents += currentShow.RoofTopCampers + ":" + (exApp.ProductInfo.RoofTopCampers ? "Roof Top Campers" : "") + InsertNewLine();
                //messageContents += currentShow.SlideOnTrayTopCampers + ":" + (exApp.ProductInfo.SlideOnTraytopCampers ? "Slide On / Traytop Campers" : "") + InsertNewLine();
                //messageContents += currentShow.TouringTourismCaravanParks + ":" + (exApp.ProductInfo.TouringTourismCaravanParks ? "Touring / Tourism / Caravan Parks" : "") + InsertNewLine();
                //messageContents += currentShow.TowingEquipment + ":" + (exApp.ProductInfo.TowingEquipment ? "Towing Equipment" : "") + InsertNewLine();
                //messageContents += currentShow.VehiclesVehicleAccessories4WD + ":" + (exApp.ProductInfo.VehiclesVehicleAccessories4WDAccessories ? "Vehicles / Vehicle Acc. / 4WD Acc." : "") + InsertNewLine();
                //messageContents += currentShow.OtherProductsServices + ":" + (exApp.ProductInfo.OtherProductsServices ? "Other Products / Services" : "") + InsertNewLine();
                //messageContents += currentShow.AccessHeightRequired + ":" + exApp.ProductInfo.AccessHeight + InsertNewLine();
                //if (GetCurrentShow().ShowType == ShowType.Main)
                //{
                //    //Security Deposit Refund
                //    messageContents += InsertNewLine() + "//Security Deposit Refund" + InsertNewLine();
                //    messageContents += currentShow.BankName + ":" + exApp.SiteInfo.BankName + InsertNewLine();
                //    messageContents += currentShow.BranchName + ":" + exApp.SiteInfo.BranchName + InsertNewLine();
                //    messageContents += currentShow.BSB + ":" + exApp.SiteInfo.BSB + InsertNewLine();
                //    messageContents += currentShow.AccountNo + ":" + exApp.SiteInfo.AccountNo + InsertNewLine();
                //    messageContents += currentShow.AccountName + ":" + exApp.SiteInfo.AccountName + InsertNewLine();
                //}
                ////Additional Notes / Requests
                //messageContents += InsertNewLine() + "//Additional Notes / Requests" + InsertNewLine();
                //messageContents += currentShow.NotesAndRequests + ":" + exApp.NotesAndRequests + InsertNewLine();
                //messageContents += currentShow.DateSubmitted + ":" + ((DateTime)exApp.DateSubmitted).ToString("dd/MM/yyyy") + InsertNewLine();
                //
                //return messageContents;
            }
        }

        private string ConvertBoolToYesNo(bool value)
        {
            if (value)
                return "Yes";
            else
                return "No";
        }

        private string InsertNewLine()
        {
            //return Environment.NewLine;
            return "<br />";
        }

        private string ReturnLocationRequested(ExhibitorApplication exApp)
        {
            if (!exApp.SiteInfo.Indoors && !exApp.SiteInfo.Outdoors && !exApp.SiteInfo.OutdoorsWithMarquee)
                return "";
            else if (exApp.SiteInfo.Indoors && !exApp.SiteInfo.Outdoors && !exApp.SiteInfo.OutdoorsWithMarquee)
                return "Indoors";
            else if (!exApp.SiteInfo.Indoors && exApp.SiteInfo.Outdoors && !exApp.SiteInfo.OutdoorsWithMarquee)
                return "Outdoors";
            else if (!exApp.SiteInfo.Indoors && !exApp.SiteInfo.Outdoors && exApp.SiteInfo.OutdoorsWithMarquee)
                return "Outdoors w/ Marquee";
            else if (exApp.SiteInfo.Indoors && exApp.SiteInfo.Outdoors && !exApp.SiteInfo.OutdoorsWithMarquee)
                return "Indoors and Outdoors";
            else if (exApp.SiteInfo.Indoors && !exApp.SiteInfo.Outdoors && exApp.SiteInfo.OutdoorsWithMarquee)
                return "Ind and Out w/ Marquee";
            else if (!exApp.SiteInfo.Indoors && exApp.SiteInfo.Outdoors && exApp.SiteInfo.OutdoorsWithMarquee)
                return "Out and Out w/ Marquee";
            else
                return "Ind, Out and Out w/ Marquee";
        }

        private void SendExhibitorConfirmationEmail(ExhibitorApplication exApp, EmailSetting emailSettings)
        {
#if Debug
                SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
#else
            SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
            client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
            //SmtpClient client = new SmtpClient("mail.bigpond.com");

#endif
            MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
            //NEED TO CHANGE THIS ADDRESS TO A LIVE EMAIL ADDRESS, CAREFUL WHEN TESTING THIS
            MailAddress to = new MailAddress(GetExhibitorEmailAndLogin(exApp, emailSettings));
            MailMessage message = new MailMessage(from, to);
            message.Body = GetExhibitorConfirmationEmail(exApp);
            message.IsBodyHtml = true;
            message.Subject = "Confirmation - Caravanning Queensland";
            client.Send(message);
        }

        private string GetExhibitorEmailAndLogin(ExhibitorApplication exApp, EmailSetting emailSetting)
        {
            using (var db = new CaravanQLDContext())
            {
                var email = "";

                if (!String.IsNullOrEmpty(emailSetting.ExhibitorEmailAndLoginField))
                {
                    var formFieldId = Guid.Parse(emailSetting.ExhibitorEmailAndLoginField);
                    var formFieldValue = db.FormFieldValues.First(x => x.FormFieldId == formFieldId && x.ExhibitorApplicationId == exApp.Id);

                    if (formFieldValue != null && !String.IsNullOrEmpty(formFieldValue.Value))
                    {
                        email = formFieldValue.Value;
                    }
                }

                return email;
            }
        }

        private string GetExhibitorConfirmationEmail(ExhibitorApplication exApp)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = db.Shows.FirstOrDefault(x => x.Id == exApp.LinkedShowId);
                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IsCurrent).OrderBy(x => x.Order).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exApp.Id).ToList();

                var headingStyle = "style='color: #2F3370'";
                var tableStart = "<table style='width: 750px;'>";
                var tableEnd = "</table>";
                var tableRow = "<tr><td style='width: 250px; vertical-align: top;'><b>{0}</b></td><td>{1}</td></tr>";
                var tableRowForContent = "<tr><td colspan='2' style='width: 750px; vertical-align: top;'>{0}</td></tr>";
                var messageContents = "<div style='font-family: Arial;'><style>h1, h2, h3, h4, h5 { color: #2F3370; }</style>";
                if (!String.IsNullOrEmpty(currentShow.ConfirmationEmail))
                {
                    messageContents += currentShow.ConfirmationEmail;
                }
                else
                {
                    messageContents += "<p>Thank-you for submitting your application for a site at our upcoming show. Your registration has been saved successfully.</p>";
                    messageContents += "<p>Please see below a copy of your Application Form for your records.</p>";
                    messageContents += "<p>Your application will be considered in line with our Allocation Policy. We will be processing these in batches and as such you may not hear from us for a short time.</p>";
                    messageContents += "<p>If you wish to clarify anything on this confirmation, please reply to <a href='mailto:info@caravanqld.com.au'>info@caravanqld.com.au</a></p>";
                    messageContents += "<p>We look forward to working with you.</p>";
                    messageContents += "<p>Caravanning Queensland Team</p>";
                }

                for (int i = 0; i < formSections.Count; i++)
                {
                    //messageContents += String.Format("<h2 {0}>{1}</h2>", headingStyle, formSection.Name);
                    messageContents += tableStart;

                    foreach (var formField in formSections[i].FormFields.OrderBy(x => x.Order))
                    {
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == formField.Id).Value;

                        if (formField.DataType == FormDataType.ContentSection || formField.DataType == FormDataType.TextField)
                        {
                            messageContents += String.Format(tableRowForContent, AppMethods.ReplaceSmartFields(currentShow, exApp, formSections, formFieldValues, formField.TextContent));
                        }
                        else if (formField.DataType == FormDataType.TextBox || formField.DataType == FormDataType.TextArea 
                            || formField.DataType == FormDataType.DropDown || formField.DataType == FormDataType.RadioButton)
                        {
                            messageContents += String.Format(tableRow, formField.Label, formFieldValue);
                        }
                        else if (formField.DataType == FormDataType.Checkbox)
                        {
                            if (String.IsNullOrEmpty(formFieldValue))
                            {
                                messageContents += String.Format(tableRow, formField.Label, "No");
                            }
                            else
                            {
                                messageContents += String.Format(tableRow, formField.Label, "Yes");
                            }
                        }
                        else if (formField.DataType == FormDataType.CheckboxArray)
                        {
                            formFieldValue = formFieldValue.Replace("'", "").Replace("|", ", ");
                            messageContents += String.Format(tableRow, formField.Label, formFieldValue);
                        }
                    }

                    //Accepted Terms & Conditions for the last form section
                    if (i == (formSections.Count - 1))
                    {
                        messageContents += String.Format(tableRow, "Accepted Terms and Conditions", ConvertBoolToYesNo(exApp.AcceptedTermsAndConditions));
                    }

                    messageContents += tableEnd;
                }



                //messageContents += String.Format("<h2 {0}>Exhibitor Details</h2>", headingStyle);
                //messageContents += tableStart;
                //messageContents += String.Format(tableRow, "ShowID", exApp.ShowId);
                //messageContents += String.Format(tableRow, "Trading Name", exApp.TradingName);
                //messageContents += String.Format(tableRow, "Show Listing Name", exApp.ShowListingName);
                //messageContents += String.Format(tableRow, "Address Line 1", exApp.AddressLine1);
                //messageContents += String.Format(tableRow, "Address Line 2", exApp.AddressLine2);
                //messageContents += String.Format(tableRow, "Suburb / Town", exApp.Suburb);
                //messageContents += String.Format(tableRow, "State", exApp.State);
                //messageContents += String.Format(tableRow, "Country", exApp.Country);
                //messageContents += String.Format(tableRow, "Postcode", exApp.Postcode);
                //messageContents += String.Format(tableRow, "Exhibitor Phone", exApp.ExhibitorPhone);
                //messageContents += String.Format(tableRow, "Exhibitor Fax", exApp.ExhibitorFax);
                //messageContents += String.Format(tableRow, "Exhibitor Email", exApp.ExhibitorEmail);
                //messageContents += String.Format(tableRow, "Website", exApp.Website);
                //messageContents += tableEnd;

                //messageContents += String.Format("<h2 {0}>Contact Person Details</h2>", headingStyle);
                //messageContents += tableStart;
                //messageContents += String.Format(tableRow, "Title / Position", exApp.Title);
                //messageContents += String.Format(tableRow, "First Name", exApp.FirstName);
                //messageContents += String.Format(tableRow, "Surname", exApp.Surname);
                //messageContents += String.Format(tableRow, "Mobile", exApp.Mobile);
                //messageContents += String.Format(tableRow, "Phone", exApp.Phone);
                //messageContents += String.Format(tableRow, "Email", exApp.Email);
                //messageContents += String.Format(tableRow, "Is Show Contact", ConvertBoolToYesNo(exApp.IsShowContact));
                //messageContents += tableEnd;

                //if (!exApp.IsShowContact)
                //{
                //    messageContents += String.Format("<h2 {0}>Show Contact Person Details</h2>", headingStyle);
                //    messageContents += tableStart;
                //    messageContents += String.Format(tableRow, "Title / Position", exApp.ShowContactTitle);
                //    messageContents += String.Format(tableRow, "First Name", exApp.ShowContactFirstName);
                //    messageContents += String.Format(tableRow, "Surname", exApp.ShowContactSurname);
                //    messageContents += String.Format(tableRow, "Mobile", exApp.ShowContactMobile);
                //    messageContents += tableEnd;
                //}

                //if (GetCurrentShow().ShowType == ShowType.Main)
                //{
                //    messageContents += String.Format("<h2 {0}>Member / CIAA Contributor</h2>", headingStyle);
                //    messageContents += tableStart;
                //    messageContents += String.Format(tableRow, "CQ Member", ConvertBoolToYesNo(exApp.CQMember));
                //    if (exApp.CQMember)
                //    {
                //        messageContents += String.Format(tableRow, "CQ Membership Type", exApp.CQMemberType);
                //        messageContents += String.Format(tableRow, "CQ Membership ID", exApp.CQMembershipID);
                //    }
                //    messageContents += String.Format(tableRow, "Reciprocal Member", ConvertBoolToYesNo(exApp.InterstateMember));
                //    if (exApp.InterstateMember)
                //    {
                //        messageContents += String.Format(tableRow, "Reciprocal Membership Type", exApp.InterstateMemberType);
                //        messageContents += String.Format(tableRow, "Reciprocal Association", exApp.InterstateAssociation);
                //    }
                //    messageContents += String.Format(tableRow, "CIAA Contribution", ConvertBoolToYesNo(exApp.Contributed));
                //    messageContents += tableEnd;
                //}

                //messageContents += String.Format("<h2 {0}>Site Preference</h2>", headingStyle);
                //messageContents += tableStart;
                //if (GetCurrentShow().ShowType == ShowType.Sales || GetCurrentShow().OnlyOptionB)
                //{
                //    messageContents += String.Format(tableRow, "Marquee Required", ConvertBoolToYesNo(exApp.SiteInfo.MarqueeRequired));
                //}
                //if (!String.IsNullOrEmpty(exApp.SiteInfo.StandChoice1))
                //{
                //    //Option A
                //    messageContents += String.Format(tableRow, "Stand Choice 1", exApp.SiteInfo.StandChoice1);
                //    messageContents += String.Format(tableRow, "Stand Choice 2", exApp.SiteInfo.StandChoice2);
                //    messageContents += String.Format(tableRow, "Stand Choice 3", exApp.SiteInfo.StandChoice3);
                //    messageContents += String.Format(tableRow, "Preferred Frontage", exApp.SiteInfo.Frontage);
                //    messageContents += String.Format(tableRow, "Preferred Depth", exApp.SiteInfo.Depth);
                //}
                //else
                //{
                //    //Option B
                //    messageContents += String.Format(tableRow, "Frontage Requested", exApp.SiteInfo.FrontageRequested);
                //    messageContents += String.Format(tableRow, "Depth Requested", exApp.SiteInfo.DepthRequested);
                //    messageContents += String.Format(tableRow, "Indoors", ConvertBoolToYesNo(exApp.SiteInfo.Indoors));
                //    messageContents += String.Format(tableRow, "Outdoors", ConvertBoolToYesNo(exApp.SiteInfo.Outdoors));
                //    if (GetCurrentShow().ShowType == ShowType.Sales)
                //        messageContents += String.Format(tableRow, "Outdoors With Marquee", ConvertBoolToYesNo(exApp.SiteInfo.OutdoorsWithMarquee));
                //}
                //messageContents += tableEnd;

                //messageContents += String.Format("<h2 {0}>Shell Scheme</h2>", headingStyle);
                //messageContents += tableStart;
                //messageContents += String.Format(tableRow, "Shell Scheme", ConvertBoolToYesNo(exApp.SiteInfo.ShellScheme));
                //messageContents += tableEnd;

                //messageContents += String.Format("<h2 {0}>Amplification</h2>", headingStyle);
                //messageContents += tableStart;
                //messageContents += String.Format(tableRow, "Amplification", ConvertBoolToYesNo(exApp.SiteInfo.Amplification));
                //messageContents += tableEnd;

                //messageContents += String.Format("<h2 {0}>Products / Services</h2>", headingStyle);
                //messageContents += tableStart;
                //messageContents += String.Format(tableRow, "Products / Services", exApp.ProductInfo.Products);
                //messageContents += String.Format(tableRow, "Caravan Brand 1", exApp.ProductInfo.CaravanBrand1);
                //messageContents += String.Format(tableRow, "Caravan Brand 2", exApp.ProductInfo.CaravanBrand2);
                //messageContents += String.Format(tableRow, "Caravan Brand 3", exApp.ProductInfo.CaravanBrand3);
                //messageContents += String.Format(tableRow, "Caravan Brand 4", exApp.ProductInfo.CaravanBrand4);
                //messageContents += String.Format(tableRow, "Caravan Brand 5", exApp.ProductInfo.CaravanBrand5);
                //messageContents += String.Format(tableRow, "Caravan Brand 6", exApp.ProductInfo.CaravanBrand6);
                //messageContents += String.Format(tableRow, "Camper Trailer Brand 1", exApp.ProductInfo.CamperTrailerBrand1);
                //messageContents += String.Format(tableRow, "Camper Trailer Brand 2", exApp.ProductInfo.CamperTrailerBrand2);
                //messageContents += String.Format(tableRow, "Camper Trailer Brand 3", exApp.ProductInfo.CamperTrailerBrand3);
                //messageContents += String.Format(tableRow, "Camper Trailer Brand 4", exApp.ProductInfo.CamperTrailerBrand4);
                //messageContents += String.Format(tableRow, "Camper Trailer Brand 5", exApp.ProductInfo.CamperTrailerBrand5);
                //messageContents += String.Format(tableRow, "Camper Trailer Brand 6", exApp.ProductInfo.CamperTrailerBrand6);
                //messageContents += String.Format(tableRow, "Fifthwheeler Brand 1", exApp.ProductInfo.FifthwheelerBrand1);
                //messageContents += String.Format(tableRow, "Fifthwheeler Brand 2", exApp.ProductInfo.FifthwheelerBrand2);
                //messageContents += String.Format(tableRow, "Fifthwheeler Brand 3", exApp.ProductInfo.FifthwheelerBrand3);
                //messageContents += String.Format(tableRow, "Fifthwheeler Brand 4", exApp.ProductInfo.FifthwheelerBrand4);
                //messageContents += String.Format(tableRow, "Motorhome Brand 1", exApp.ProductInfo.MotorhomeBrand1);
                //messageContents += String.Format(tableRow, "Motorhome Brand 2", exApp.ProductInfo.MotorhomeBrand2);
                //messageContents += String.Format(tableRow, "Motorhome Brand 3", exApp.ProductInfo.MotorhomeBrand3);
                //messageContents += String.Format(tableRow, "Motorhome Brand 4", exApp.ProductInfo.MotorhomeBrand4);

                //messageContents += String.Format(tableRow, "Accessories / Parts", ConvertBoolToYesNo(exApp.ProductInfo.AccessoriesParts));
                //messageContents += String.Format(tableRow, "Advisory Services", ConvertBoolToYesNo(exApp.ProductInfo.AdvisoryServices));
                //messageContents += String.Format(tableRow, "Annexes / Awnings", ConvertBoolToYesNo(exApp.ProductInfo.AnnexesAwnings));
                //messageContents += String.Format(tableRow, "Art Unions / Fundraising", ConvertBoolToYesNo(exApp.ProductInfo.ArtUnionsFundraising));
                //messageContents += String.Format(tableRow, "Cabins / Homes", ConvertBoolToYesNo(exApp.ProductInfo.CabinsHomes));
                //messageContents += String.Format(tableRow, "Camper Trailers", ConvertBoolToYesNo(exApp.ProductInfo.CamperTrailers));
                //messageContents += String.Format(tableRow, "Camping Equipment / Tents", ConvertBoolToYesNo(exApp.ProductInfo.CampingEquipmentTents));
                //messageContents += String.Format(tableRow, "Caravans / Pop Tops", ConvertBoolToYesNo(exApp.ProductInfo.CaravansPopTops));
                //messageContents += String.Format(tableRow, "Carports / Carvan Covers", ConvertBoolToYesNo(exApp.ProductInfo.CarportsCaravanCovers));
                //messageContents += String.Format(tableRow, "Clothing / Hats", ConvertBoolToYesNo(exApp.ProductInfo.ClothingHats));
                //messageContents += String.Format(tableRow, "Clubs", ConvertBoolToYesNo(exApp.ProductInfo.Clubs));
                //messageContents += String.Format(tableRow, "Communication", ConvertBoolToYesNo(exApp.ProductInfo.Communication));
                //messageContents += String.Format(tableRow, "Fifth Wheelers", ConvertBoolToYesNo(exApp.ProductInfo.FifthWheelers));
                //messageContents += String.Format(tableRow, "Hire", ConvertBoolToYesNo(exApp.ProductInfo.Hire));
                //messageContents += String.Format(tableRow, "Insurance", ConvertBoolToYesNo(exApp.ProductInfo.Insurance));
                //messageContents += String.Format(tableRow, "Marine / Fishing / Boat Trailers", ConvertBoolToYesNo(exApp.ProductInfo.MarineFishingBoatTrailers));
                //messageContents += String.Format(tableRow, "Motorhomes / Campervans & Conversions", ConvertBoolToYesNo(exApp.ProductInfo.MotorhomesCampervansConversions));
                //messageContents += String.Format(tableRow, "Publications", ConvertBoolToYesNo(exApp.ProductInfo.Publications));
                //messageContents += String.Format(tableRow, "Roof Top Campers", ConvertBoolToYesNo(exApp.ProductInfo.RoofTopCampers));
                //messageContents += String.Format(tableRow, "Slide On / Traytop Campers", ConvertBoolToYesNo(exApp.ProductInfo.SlideOnTraytopCampers));
                //messageContents += String.Format(tableRow, "Touring / Tourism / Caravan Parks", ConvertBoolToYesNo(exApp.ProductInfo.TouringTourismCaravanParks));
                //messageContents += String.Format(tableRow, "Towing Equipment", ConvertBoolToYesNo(exApp.ProductInfo.TowingEquipment));
                //messageContents += String.Format(tableRow, "Vehicles / Vehicle Acc. / 4WD Acc.", ConvertBoolToYesNo(exApp.ProductInfo.VehiclesVehicleAccessories4WDAccessories));
                //messageContents += String.Format(tableRow, "Other Products / Services", ConvertBoolToYesNo(exApp.ProductInfo.OtherProductsServices));
                //messageContents += String.Format(tableRow, "Access Height", exApp.ProductInfo.AccessHeight);
                //messageContents += tableEnd;

                //if (GetCurrentShow().ShowType == ShowType.Main)
                //{
                //    messageContents += String.Format("<h2 {0}>Security Deposit Refund</h2>", headingStyle);
                //    messageContents += tableStart;
                //    messageContents += String.Format(tableRow, "BSB", exApp.SiteInfo.BSB);
                //    messageContents += String.Format(tableRow, "Account No", exApp.SiteInfo.AccountNo);
                //    messageContents += String.Format(tableRow, "Account Name", exApp.SiteInfo.AccountName);
                //    messageContents += String.Format(tableRow, "Bank Name", exApp.SiteInfo.BankName);
                //    messageContents += String.Format(tableRow, "Branch Name", exApp.SiteInfo.BranchName);
                //    messageContents += tableEnd;
                //}

                //messageContents += String.Format("<h2 {0}>Additional Notes / Requests</h2>", headingStyle);
                //messageContents += tableStart;
                //messageContents += String.Format(tableRow, "Additional Notes & Requests", exApp.NotesAndRequests);
                //messageContents += String.Format(tableRow, "Accepted Terms and Conditions", ConvertBoolToYesNo(exApp.AcceptedTermsAndConditions));
                //messageContents += tableEnd;
                //messageContents += "</div>";

                return messageContents;
            }
        }

        public JsonResult ResendConfirmationEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                try
                {
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == id);
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == exhibitor.LinkedShowId);

                    SendExhibitorConfirmationEmail(exhibitor, emailSettings);

                    return Json("succeeded", JsonRequestBehavior.AllowGet);
                }
                catch (Exception exc)
                {
                    return Json("succeeded");
                }
            }
        }

        public JsonResult ResendWebProspectEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                try
                {
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == id);
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == exhibitor.LinkedShowId);

                    SendWebProspectEmail(exhibitor, emailSettings);

                    return Json("succeeded", JsonRequestBehavior.AllowGet);
                }
                catch (Exception exc)
                {
                    return Json("succeeded");
                }
            }
        }

        public ActionResult MakeChanges()
        {
            return JavaScript("window.location = '" + Url.Action("Index", "Apply_Now") + "'");
        }

        public ActionResult Success()
        {
            return View();
        }
    }
}
