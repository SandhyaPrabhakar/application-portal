﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.AppPortal.Models;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            using (var db = new CaravanQLDContext())
            {
                var applications = db.ExhibitorApplications.ToList();
                var model = new TestModel()
                {
                    CountOfApps = applications.Count
                };

                return View(model);
            }
        }

    }
}
