﻿using BTT.CaravanQLD.AppPortal.Models;
using BTT.CaravanQLD.DataAccess;
using BTT.CaravanQLD.Model;
using BTT.CsvUtils;
using BTT.JQGrid.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class DataPortalController : Controller
    {
        private bool IsUserPortalAdmin()
        {
            if (User.Identity.Name == AppConstants.AdminUserName)
                return true;
            else 
                return false;
        }

        private bool HasSelectedShow()
        {
            if (Session[AppConstants.CurrentShowAdminSession] != null)
                return true;
            else
                return false;
        }

        public ActionResult Index()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return RedirectToAction("Exhibitors", "DataPortal");
        }

        #region Exhibitors

        public ActionResult Exhibitors(Guid? ExhibitorId, bool ForwardToExhibitor = false)
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                var model = new ExhibitorsModel()
                {
                    ForwardToExhibitor = ForwardToExhibitor,
                    ExhibitorId = (Guid)(ExhibitorId == null ? Guid.Empty : ExhibitorId)
                };

                return View(model);
            }
        }

        public ActionResult ExhibitorList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var portalSetting = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                if (portalSetting == null)
                {
                    portalSetting = new PortalSetting();
                    portalSetting.LinkedShowId = currentShow.Id;
                    db.PortalSettings.Add(portalSetting);
                    db.SaveChanges();
                }

                var exhibitorList = new List<ExhibitorApplication>();
                if (portalSetting.SortByFormField != null)
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted == null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                    else
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted != null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                }
                else
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted == null).OrderBy(x => x.ShowId).ToList();
                    }
                    else
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted != null).OrderBy(x => x.ShowId).ToList();
                    }
                }

                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var exhibitorIds = exhibitorList.Select(y => y.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => exhibitorIds.Contains(x.ExhibitorApplicationId)).ToList();

                //Get the size of the grid
                var formGridWidth = 0;

                for (int i = 0; i < formSections.Count; i++)
                {
                    //Add a row for the Show ID
                    formGridWidth = i == 0 ? 75 : formGridWidth;
                    //Add a row for date completed if showing submitted exhibitors
                    formGridWidth = i == 0 && portalSetting.ExhibitorType == ExhibitorType.SubmittedExhibitors ? formGridWidth + 150 : formGridWidth;

                    foreach (var formField in formSections[i].FormFields)
                    {
                        if (formField.DataType == FormDataType.TextBox)
                        {
                            formGridWidth = formGridWidth + 250;
                        }
                        else if (formField.DataType == FormDataType.DropDown || formField.DataType == FormDataType.RadioButton || formField.DataType == FormDataType.Checkbox)
                        {
                            formGridWidth = formGridWidth + 200;
                        }
                        else if (formField.DataType == FormDataType.TextArea)
                        {
                            formGridWidth = formGridWidth + 400;
                        }
                        else if (formField.DataType == FormDataType.CheckboxArray)
                        {
                            formGridWidth = formGridWidth + 300;
                        }
                    }
                }

                var model = new ExhibitorListModel()
                {
                    PortalSetting = portalSetting,
                    FormSections = formSections,
                    FormFieldValues = formFieldValues,
                    Exhibitors = exhibitorList,
                    GridWidth = formGridWidth <= 500 ? 500 : formGridWidth
                };

                return PartialView(model);
            }
        }

        public ActionResult ChangeExhibitorList(ExhibitorType exhibitorType)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var portalSetting = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                portalSetting.ExhibitorType = exhibitorType;
                db.SaveChanges();

                var exhibitorList = new List<ExhibitorApplication>();

                if (portalSetting.SortByFormField != null)
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted == null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                    else
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted != null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                }
                else
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted == null).OrderBy(x => x.ShowId).ToList();
                    }
                    else
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted != null).OrderBy(x => x.ShowId).ToList();
                    }
                }

                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var exhibitorIds = exhibitorList.Select(y => y.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => exhibitorIds.Contains(x.ExhibitorApplicationId)).ToList();

                //Get the size of the grid
                var formGridWidth = 0;

                for (int i = 0; i < formSections.Count; i++)
                {
                    //Add a row for the Show ID
                    formGridWidth = i == 0 ? 75 : formGridWidth;
                    //Add a row for date completed if showing submitted exhibitors
                    formGridWidth = i == 0 && portalSetting.ExhibitorType == ExhibitorType.SubmittedExhibitors ? formGridWidth + 150 : formGridWidth;

                    foreach (var formField in formSections[i].FormFields)
                    {
                        if (formField.DataType == FormDataType.TextBox)
                        {
                            formGridWidth = formGridWidth + 250;
                        }
                        else if (formField.DataType == FormDataType.DropDown || formField.DataType == FormDataType.RadioButton || formField.DataType == FormDataType.Checkbox)
                        {
                            formGridWidth = formGridWidth + 200;
                        }
                        else if (formField.DataType == FormDataType.TextArea)
                        {
                            formGridWidth = formGridWidth + 400;
                        }
                        else if (formField.DataType == FormDataType.CheckboxArray)
                        {
                            formGridWidth = formGridWidth + 300;
                        }
                    }
                }

                var model = new ExhibitorListModel()
                {
                    PortalSetting = portalSetting,
                    FormSections = formSections,
                    FormFieldValues = formFieldValues,
                    Exhibitors = exhibitorList,
                    GridWidth = formGridWidth
                };

                return PartialView("ExhibitorList", model); 
            }
        }

        public ActionResult ExhibitorListForNew()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitors = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").Where(x => x.DateSubmitted == null && x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ShowId).ToList();

                var model = new ExhibitorListForNewModel()
                {
                    Exhibitors = exhibitors
                };

                return PartialView(model);
            }
        }

        public ActionResult ExhibitorListForSubmitted()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitors = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").Where(x => x.DateSubmitted != null && x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ShowId).ToList();

                var model = new ExhibitorListForSubmittedModel()
                {
                    Exhibitors = exhibitors
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult ExhibitorListForSearch()
        {
            using (var db = new CaravanQLDContext())
            {
                var request = Request;
                var searchTerm = "";

                var currentShow = GetCurrentShow();
                var portalSetting = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var exhibitorList = new List<ExhibitorApplication>();

                var showIdSearch = request.Form.Get("showId");
                if (!String.IsNullOrEmpty(showIdSearch))
                {
                    searchTerm = showIdSearch.ToLower();
                    if (portalSetting.SortByFormField != null)
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.ShowId.ToLower().Contains(searchTerm)).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                    else
                    {
                        exhibitorList = exhibitors.Where(x => x.LinkedShowId == currentShow.Id && x.ShowId.ToLower().Contains(searchTerm)).OrderBy(x => x.ShowId).ToList();
                    }
                }

                var dateSubmittedSearch = request.Form.Get("dateSubmitted");
                if (!String.IsNullOrEmpty(dateSubmittedSearch))
                {
                    searchTerm = dateSubmittedSearch;
                    var searchDate = DateTime.Parse(dateSubmittedSearch);
                    if (portalSetting.SortByFormField != null)
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                        //Linq to Entities doesn't support .AddDays(x)
                        exhibitorList = exhibitorList.Where(x => x.DateSubmitted >= searchDate && x.DateSubmitted < searchDate.AddDays(1)).ToList();
                    }
                    else
                    {
                        exhibitorList = exhibitors.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted >= searchDate && x.DateSubmitted < searchDate.AddDays(1)).OrderBy(x => x.ShowId).ToList();
                    }
                }

                var formFields = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id).ToList();

                foreach (var ff in formFields)
                {
                    var formFieldSearch = request.Form.Get(ff.Id.ToString());
                    if (!String.IsNullOrEmpty(formFieldSearch))
                    {
                        searchTerm = formFieldSearch;
                        var formFieldValueIds = db.FormFieldValues.Where(x => x.FormFieldId == ff.Id && x.Value.ToLower().Contains(formFieldSearch.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();

                        if (portalSetting.SortByFormField != null)
                        {
                            var peanuts = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                                && x.ExhibitorApplication.LinkedShowId == currentShow.Id).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication);
                            exhibitorList = peanuts.Where(x => formFieldValueIds.Contains(x.Id)).ToList();
                        }
                        else
                        {
                            exhibitorList = exhibitors.Where(x => x.LinkedShowId == currentShow.Id && formFieldValueIds.Contains(x.Id)).OrderBy(x => x.ShowId).ToList();
                        }

                        break;
                    }
                }

                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var exhibitorIds = exhibitorList.Select(y => y.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => exhibitorIds.Contains(x.ExhibitorApplicationId)).ToList();

                //Get the size of the grid
                var formGridWidth = 0;

                for (int i = 0; i < formSections.Count; i++)
                {
                    //Add a row for the Show ID
                    formGridWidth = i == 0 ? 75 : formGridWidth;
                    //Add a row for date completed if showing submitted exhibitors
                    formGridWidth = i == 0 && portalSetting.ExhibitorType == ExhibitorType.SubmittedExhibitors ? formGridWidth + 150 : formGridWidth;

                    foreach (var formField in formSections[i].FormFields)
                    {
                        if (formField.DataType == FormDataType.TextBox)
                        {
                            formGridWidth = formGridWidth + 250;
                        }
                        else if (formField.DataType == FormDataType.DropDown || formField.DataType == FormDataType.RadioButton || formField.DataType == FormDataType.Checkbox)
                        {
                            formGridWidth = formGridWidth + 200;
                        }
                        else if (formField.DataType == FormDataType.TextArea)
                        {
                            formGridWidth = formGridWidth + 400;
                        }
                        else if (formField.DataType == FormDataType.CheckboxArray)
                        {
                            formGridWidth = formGridWidth + 300;
                        }
                    }
                }

                var model = new ExhibitorListModel()
                {
                    PortalSetting = portalSetting,
                    FormSections = formSections,
                    FormFieldValues = formFieldValues,
                    Exhibitors = exhibitorList,
                    GridWidth = formGridWidth
                };

                return PartialView("ExhibitorList", model);
            }
        }

        public ActionResult SortExhibitorListBy(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var portalSetting = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);

                if (id == Guid.Empty || formField == null)
                {
                    portalSetting.FormFieldId = null;
                }
                else
                {
                    portalSetting.FormFieldId = formField.Id;
                }

                db.SaveChanges();

                var exhibitorList = new List<ExhibitorApplication>();

                if (portalSetting.SortByFormField != null)
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted == null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                    else
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted != null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                }
                else
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted == null).OrderBy(x => x.ShowId).ToList();
                    }
                    else
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted != null).OrderBy(x => x.ShowId).ToList();
                    }
                }

                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var exhibitorIds = exhibitorList.Select(y => y.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => exhibitorIds.Contains(x.ExhibitorApplicationId)).ToList();

                //Get the size of the grid
                var formGridWidth = 0;

                for (int i = 0; i < formSections.Count; i++)
                {
                    //Add a row for the Show ID
                    formGridWidth = i == 0 ? 75 : formGridWidth;
                    //Add a row for date completed if showing submitted exhibitors
                    formGridWidth = i == 0 && portalSetting.ExhibitorType == ExhibitorType.SubmittedExhibitors ? formGridWidth + 150 : formGridWidth;

                    foreach (var ff in formSections[i].FormFields)
                    {
                        if (ff.DataType == FormDataType.TextBox)
                        {
                            formGridWidth = formGridWidth + 250;
                        }
                        else if (ff.DataType == FormDataType.DropDown || ff.DataType == FormDataType.RadioButton || ff.DataType == FormDataType.Checkbox)
                        {
                            formGridWidth = formGridWidth + 200;
                        }
                        else if (ff.DataType == FormDataType.TextArea)
                        {
                            formGridWidth = formGridWidth + 400;
                        }
                        else if (ff.DataType == FormDataType.CheckboxArray)
                        {
                            formGridWidth = formGridWidth + 300;
                        }
                    }
                }

                var model = new ExhibitorListModel()
                {
                    PortalSetting = portalSetting,
                    FormSections = formSections,
                    FormFieldValues = formFieldValues,
                    Exhibitors = exhibitorList,
                    GridWidth = formGridWidth
                };

                return PartialView("ExhibitorList", model);
            }
        }

        public ActionResult EditExhibitor(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == id);
                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                var model = new EditExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    FormSections = formSections,
                    FormFieldItems = formFieldItems,
                    FormFieldValues = formFieldValues
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditExhibitor(EditExhibitorModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var request = Request;
                var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == model.Exhibitor.Id && x.LinkedShowId == currentShow.Id);
                var formFields = db.FormFields.Include("Form").Include("FormFieldItems").Where(x => x.Form.LinkedShowId == currentShow.Id && x.Form.IsCurrent && x.Form.IncludeInExhibitorList).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                exhibitor.ShowId = model.Exhibitor.ShowId;

                foreach (var ff in formFields)
                {
                    if (ff.DataType == FormDataType.TextBox || ff.DataType == FormDataType.TextArea)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            //ModelState.AddModelError(ff.Id.ToString(), "Please enter a value");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.DropDown)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            //ModelState.AddModelError(ff.Id.ToString(), "Please select an option from the list");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.Checkbox)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = ff.Label;
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.CheckboxArray)
                    {
                        var formFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == ff.Id).ToList();
                        var newValue = "";
                        for (int i = 0; i < formFieldItems.Count; i++)
                        {
                            var itemValue = request.Form.Get(formFieldItems[i].Id.ToString());
                            if (itemValue == "True" && String.IsNullOrEmpty(newValue))
                            {
                                newValue = String.Format("'{0}'", formFieldItems[i].Value);
                            }
                            else if (itemValue == "True" && !String.IsNullOrEmpty(newValue))
                            {
                                newValue += String.Format("|'{0}'", formFieldItems[i].Value);
                            }
                        }

                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);

                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            //ModelState.AddModelError(ff.Id.ToString(), "Please select at least one option from the list below");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.RadioButton)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                            //ModelState.AddModelError(ff.Id.ToString(), "Please select one of these options");
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                }

                db.SaveChanges();

                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var formFieldItems2 = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                model = new EditExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    FormSections = formSections,
                    FormFieldItems = formFieldItems2,
                    FormFieldValues = formFieldValues
                };
                    
                return PartialView(model);
            }
        }

        public ActionResult ResetExhibitorApplication(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").FirstOrDefault(x => x.Id == id);
                exhibitor.SiteInfo = new SiteInfo();
                exhibitor.ProductInfo = new ProductInfo();
                exhibitor.AcceptedTermsAndConditions = false;
                exhibitor.CQMembershipID = "";
                exhibitor.DateSubmitted = null;
                exhibitor.Contributed = false;
                exhibitor.CQMember = false;
                exhibitor.CQMembershipID = "";
                exhibitor.CQMemberType = "";
                exhibitor.InterstateMember = false;
                exhibitor.InterstateMemberType = "";
                exhibitor.InterstateAssociation = "";
                exhibitor.IsShowContact = false;
                exhibitor.ShowContactTitle = "";
                exhibitor.ShowContactFirstName = "";
                exhibitor.ShowContactSurname = "";
                exhibitor.ShowContactMobile = "";
                db.SaveChanges();

                var model = new EditExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    //StateList = AppMethods.CreateListOfStates()
                };

                return PartialView("EditExhibitor", model);
            }
        }

        public ActionResult ResetExhibitorDateSubmitted(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();

                var exhibitor = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").FirstOrDefault(x => x.Id == id);
                exhibitor.AcceptedTermsAndConditions = false;
                exhibitor.DateSubmitted = null;
                db.SaveChanges();

                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                var model = new EditExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    FormSections = formSections,
                    FormFieldItems = formFieldItems,
                    FormFieldValues = formFieldValues
                };

                return PartialView("EditExhibitor", model);
            }
        }

        public ActionResult DeleteExhibitor(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var exhibitor = db.ExhibitorApplications.Include("EmailLogs").Include("ExhibitorApplicationEmails").Include("SiteInfo").Include("ProductInfo").FirstOrDefault(x => x.Id == id);

                for (int i = exhibitor.ExhibitorApplicationEmails.Count - 1; i >= 0; i--)
                {
                    db.ExhibitorApplicationEmails.Remove(exhibitor.ExhibitorApplicationEmails.ToList()[i]);
                }

                for (int i = exhibitor.EmailLogs.Count - 1; i >= 0; i--)
                {
                    db.EmailLogs.Remove(exhibitor.EmailLogs.ToList()[i]);
                }

                if (exhibitor.SiteInfo != null)
                    db.SiteInfos.Remove(exhibitor.SiteInfo);

                if (exhibitor.ProductInfo != null)
                    db.ProductInfos.Remove(exhibitor.ProductInfo);

                db.ExhibitorApplications.Remove(exhibitor);
                db.SaveChanges();

                var currentShow = GetCurrentShow();
                var portalSetting = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                if (portalSetting == null)
                {
                    portalSetting = new PortalSetting();
                    portalSetting.LinkedShowId = currentShow.Id;
                    db.PortalSettings.Add(portalSetting);
                    db.SaveChanges();
                }

                var exhibitorList = new List<ExhibitorApplication>();
                if (portalSetting.SortByFormField != null)
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted == null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                    else
                    {
                        exhibitorList = db.FormFieldValues.Include("ExhibitorApplication").Where(x => x.FormFieldId == portalSetting.FormFieldId
                            && x.ExhibitorApplication.LinkedShowId == currentShow.Id && x.ExhibitorApplication.DateSubmitted != null).OrderBy(x => x.Value).Select(x => x.ExhibitorApplication).ToList();
                    }
                }
                else
                {
                    if (portalSetting.ExhibitorType == ExhibitorType.NewExhibitors)
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted == null).OrderBy(x => x.ShowId).ToList();
                    }
                    else
                    {
                        exhibitorList = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id && x.DateSubmitted != null).OrderBy(x => x.ShowId).ToList();
                    }
                }

                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var exhibitorIds = exhibitorList.Select(y => y.Id).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => exhibitorIds.Contains(x.ExhibitorApplicationId)).ToList();

                //Get the size of the grid
                var formGridWidth = 0;

                for (int i = 0; i < formSections.Count; i++)
                {
                    //Add a row for the Show ID
                    formGridWidth = i == 0 ? 75 : formGridWidth;
                    //Add a row for date completed if showing submitted exhibitors
                    formGridWidth = i == 0 && portalSetting.ExhibitorType == ExhibitorType.SubmittedExhibitors ? formGridWidth + 150 : formGridWidth;

                    foreach (var formField in formSections[i].FormFields)
                    {
                        if (formField.DataType == FormDataType.TextBox)
                        {
                            formGridWidth = formGridWidth + 250;
                        }
                        else if (formField.DataType == FormDataType.DropDown || formField.DataType == FormDataType.RadioButton || formField.DataType == FormDataType.Checkbox)
                        {
                            formGridWidth = formGridWidth + 200;
                        }
                        else if (formField.DataType == FormDataType.TextArea)
                        {
                            formGridWidth = formGridWidth + 400;
                        }
                        else if (formField.DataType == FormDataType.CheckboxArray)
                        {
                            formGridWidth = formGridWidth + 300;
                        }
                    }
                }

                var model = new ExhibitorListModel()
                {
                    PortalSetting = portalSetting,
                    FormSections = formSections,
                    FormFieldValues = formFieldValues,
                    Exhibitors = exhibitorList,
                    GridWidth = formGridWidth
                };

                return PartialView("ExhibitorList", model);
            }
        }

        public ActionResult AddNewExhibitor()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitor = new ExhibitorApplication();
                exhibitor.ShowId = GetNextShowID();
                exhibitor.DateAdded = DateTime.Now;
                exhibitor.LinkedShowId = currentShow.Id;

                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();

                var model = new AddNewExhibitorModel()
                {
                    Exhibitor = exhibitor,
                    FormFieldItems = formFieldItems,
                    FormSections = formSections
                };

                return PartialView("AddNewExhibitor", model);
            }
        }

        [HttpPost]
        public ActionResult AddNewExhibitor(AddNewExhibitorModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var request = Request;
                var exhibitor = new ExhibitorApplication();
                exhibitor.ShowId = model.Exhibitor.ShowId;
                exhibitor.LinkedShowId = currentShow.Id;
                db.ExhibitorApplications.Add(exhibitor);

                db.SaveChanges();

                var formFields = db.FormFields.Include("Form").Include("FormFieldItems").Where(x => x.Form.LinkedShowId == currentShow.Id).ToList();
                foreach (var formField in formFields)
                {
                    var formFieldValue = new FormFieldValue()
                    {
                        ExhibitorApplicationId = exhibitor.Id,
                        FormFieldId = formField.Id
                    };

                    db.FormFieldValues.Add(formFieldValue);
                }

                db.SaveChanges();

                formFields = formFields.Where(x => x.Form.LinkedShowId == currentShow.Id && x.Form.IsCurrent && x.Form.IncludeInExhibitorList).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();

                foreach (var ff in formFields)
                {
                    if (ff.DataType == FormDataType.TextBox || ff.DataType == FormDataType.TextArea)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.DropDown)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.Checkbox)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = ff.Label;
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.CheckboxArray)
                    {
                        var formFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == ff.Id).ToList();
                        var newValue = "";
                        for (int i = 0; i < formFieldItems.Count; i++)
                        {
                            var itemValue = request.Form.Get(formFieldItems[i].Id.ToString());
                            if (itemValue == "True" && String.IsNullOrEmpty(newValue))
                            {
                                newValue = String.Format("'{0}'", formFieldItems[i].Value);
                            }
                            else if (itemValue == "True" && !String.IsNullOrEmpty(newValue))
                            {
                                newValue += String.Format("|'{0}'", formFieldItems[i].Value);
                            }
                        }

                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);

                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                    else if (ff.DataType == FormDataType.RadioButton)
                    {
                        var newValue = request.Form.Get(ff.Id.ToString());
                        var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == ff.Id);
                        if (!String.IsNullOrEmpty(newValue))
                        {
                            formFieldValue.Value = newValue;
                        }
                        else if (String.IsNullOrEmpty(newValue) && ff.Mandatory)
                        {
                            formFieldValue.Value = "";
                        }
                        else
                        {
                            formFieldValue.Value = "";
                        }
                    }
                }

                db.SaveChanges();

                ModelState.Clear();

                var newExhibitor = new ExhibitorApplication();
                newExhibitor.ShowId = GetNextShowID();
                newExhibitor.DateAdded = DateTime.Now;
                newExhibitor.LinkedShowId = currentShow.Id;

                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id && x.IncludeInExhibitorList).OrderBy(x => x.Order).ToList();
                var formFieldItems2 = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();

                model = new AddNewExhibitorModel()
                {
                    Exhibitor = newExhibitor,
                    FormFieldItems = formFieldItems2,
                    FormSections = formSections
                };

                return PartialView(model);
            }
        }

        public string GetNextShowID()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var showIDs = db.ExhibitorApplications.Select(x => x.ShowId.Remove(0, 3)).ToList();
                if (showIDs.Count > 0)
                {
                    var numberList = new List<int>();
                    foreach (var num in showIDs)
                    {
                        numberList.Add(Convert.ToInt32(num));
                    }

                    return "CQS" + (numberList.Max() + 1).ToString();
                }
                else
                    return "CQS1";
            }
        }

        #endregion

        #region Imports

        public ActionResult Imports()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult ImportNewExhibitors()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var formFieldsForImport = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection && x.DataType != FormDataType.TextField && x.IncludeInImport)
                    .OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();

                var model = new ImportNewExhibitorsModel()
                {
                    FormSections = formSections,
                    FormFieldsForImport = formFieldsForImport
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult ImportNewExhibitors(string fakeOptionalParam = "")
        {
            var file = Request.Files[0];
            if (file != null && file.ContentLength > 0)
            {
                if (Path.GetExtension(file.FileName) == ".csv")
                {
                    using (CsvReader reader = new CsvReader(file.InputStream))
                    {
                        var db = new CaravanQLDContext();
                        var currentShow = GetCurrentShow();
                        var formFields = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id).OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();
                        var formFieldsForImport = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection && x.DataType != FormDataType.TextField && x.IncludeInImport)
                            .OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();
                        db.Dispose();

                        foreach (string[] elements in reader.RowEnumerator)
                        {
                            if (reader.RowIndex == 1) //skip the first row with column headings
                            {
                                continue;
                            }

                            if (!String.IsNullOrEmpty(elements[0]))
                            {
                                db = new CaravanQLDContext();
                                db.Configuration.AutoDetectChangesEnabled = false;

                                //Add exhibitor
                                var exhibitor = new ExhibitorApplication();
                                exhibitor.ShowId = elements[0];
                                exhibitor.LinkedShowId = currentShow.Id;
                                exhibitor.SiteInfo = new SiteInfo();
                                exhibitor.ProductInfo = new ProductInfo();
                                db.ExhibitorApplications.Add(exhibitor);

                                //Create form field values & add info
                                foreach (var formField in formFields)
                                {
                                    var formFieldValue = new FormFieldValue()
                                    {
                                        ExhibitorApplicationId = exhibitor.Id,
                                        FormFieldId = formField.Id,
                                        Value = formField.IncludeInImport ? elements[(formFieldsForImport.IndexOf(formField) + 1)] : ""
                                    };

                                    db.FormFieldValues.Add(formFieldValue);
                                }

                                db.SaveChanges();
                                db.Dispose();
                            }
                        }
                    }
                }
            }

            return RedirectToAction("Exhibitors", "DataPortal");
        }

        public ActionResult AddToFormFieldImport(Guid FormFieldId)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formField = db.FormFields.FirstOrDefault(x => x.Id == FormFieldId);
                if (formField.IncludeInImport)
                {
                    formField.IncludeInImport = false;
                }
                else
                {
                    formField.IncludeInImport = true;
                }

                db.SaveChanges();

                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFieldsForImport = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection && x.IncludeInImport)
                    .OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();

                var model = new ImportNewExhibitorsModel()
                {
                    FormSections = formSections,
                    FormFieldsForImport = formFieldsForImport
                };

                return PartialView("ImportNewExhibitors", model);
            }
        }

        public JsonResult ExportAllData()
        {
            using (var file = System.IO.File.Open(Path.Combine(Request.PhysicalApplicationPath, "Files", "files", String.Format("exported-applications {0}.csv", DateTime.Now.ToString("dd-MM-yyyy"))), FileMode.Create))
            {
                using (var csvWriter = new CsvWriter(file))
                {
                    csvWriter.Write(CreateHeaderRow());

                    using (var db = new CaravanQLDContext())
                    {
                        var currentShow = GetCurrentShow();
                        var exApps = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName);
                        foreach (var exApp in exApps)
                        {
                            try
                            {
                                csvWriter.Write(CreateExhibitorRow(exApp));
                            }
                            catch (Exception exc)
                            {

                            }
                        }
                    }
                }
            }

            return Json("Your file has been created. You can download your file by browsing with the File Uploader to the 'files' folder.", JsonRequestBehavior.AllowGet);
        }

        private string[] CreateHeaderRow()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formFields = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection).OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();

                string[] fields = new string[formFields.Count];
                fields[0] = "Show ID";
                fields[1] = "Date Submitted";

                for (int i = 0; i < formFields.Count; i++)
                {
                    fields[i + 2] = formFields[i].Name;
                }

                return fields;
            }
        }

        private string[] CreateExhibitorRow(ExhibitorApplication exApp)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formFields = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection).OrderBy(x => x.Form.Order).ThenBy(x => x.Order).ToList();
                var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exApp.Id).ToList();

                string[] fields = new string[formFields.Count];
                fields[0] = exApp.ShowId;
                fields[1] = exApp.DateSubmitted == null ? "" : ((DateTime)exApp.DateSubmitted).ToString("dd-MM-yyyy");

                for (int i = 0; i < formFields.Count; i++)
                {
                    var formFieldValue = formFieldValues.FirstOrDefault(x => x.FormFieldId == formFields[i].Id).Value;
                    fields[i + 2] = CheckStringForNull(formFieldValue);
                }

                return fields;
            }

            //string[] fields = new string[94];
            //fields[0] = exApp.ShowId;
            //fields[1] = CheckStringForNull(exApp.TradingName);
            //fields[2] = CheckStringForNull(exApp.ShowListingName);
            //fields[3] = CheckStringForNull(exApp.AddressLine1);
            //fields[4] = CheckStringForNull(exApp.AddressLine2);
            //fields[5] = CheckStringForNull(exApp.Suburb);
            //fields[6] = CheckStringForNull(exApp.State);
            //fields[7] = CheckStringForNull(exApp.Country);
            //fields[8] = CheckStringForNull(exApp.Postcode);
            //fields[9] = CheckStringForNull(exApp.ExhibitorPhone);
            //fields[10] = CheckStringForNull(exApp.ExhibitorFax);
            //fields[11] = CheckStringForNull(exApp.ExhibitorEmail);
            //fields[12] = CheckStringForNull(exApp.Website);
            //fields[13] = exApp.DateAdded == null ? "" : ((DateTime)exApp.DateAdded).ToString("dd-MM-yyyy");
            //fields[14] = exApp.DateSubmitted == null ? "" : ((DateTime)exApp.DateSubmitted).ToString("dd-MM-yyyy");
            //fields[15] = ConvertBoolToYesNo(exApp.AcceptedTermsAndConditions);
            //fields[16] = CheckStringForNull(exApp.Title);
            //fields[17] = CheckStringForNull(exApp.FirstName);
            //fields[18] = CheckStringForNull(exApp.Surname);
            //fields[19] = CheckStringForNull(exApp.Mobile);
            //fields[20] = CheckStringForNull(exApp.Phone);
            //fields[21] = CheckStringForNull(exApp.Email);
            //fields[22] = ConvertBoolToYesNo(exApp.IsShowContact);
            //fields[23] = CheckStringForNull(exApp.ShowContactTitle);
            //fields[24] = CheckStringForNull(exApp.ShowContactFirstName);
            //fields[25] = CheckStringForNull(exApp.ShowContactSurname);
            //fields[26] = CheckStringForNull(exApp.ShowContactMobile);
            //fields[27] = ConvertBoolToYesNo(exApp.CQMember);
            //fields[28] = CheckStringForNull(exApp.CQMemberType);
            //fields[29] = CheckStringForNull(exApp.CQMembershipID);
            //fields[30] = ConvertBoolToYesNo(exApp.InterstateMember);
            //fields[31] = CheckStringForNull(exApp.InterstateMemberType);
            //fields[32] = CheckStringForNull(exApp.InterstateAssociation);
            //fields[33] = ConvertBoolToYesNo(exApp.Contributed);
            //fields[34] = CheckStringForNull(exApp.SiteInfo.StandChoice1);
            //fields[35] = CheckStringForNull(exApp.SiteInfo.StandChoice2);
            //fields[36] = CheckStringForNull(exApp.SiteInfo.StandChoice3);
            //fields[37] = CheckStringForNull(exApp.SiteInfo.Frontage);
            //fields[38] = CheckStringForNull(exApp.SiteInfo.Depth);
            //fields[39] = CheckStringForNull(exApp.SiteInfo.FrontageRequested);
            //fields[40] = CheckStringForNull(exApp.SiteInfo.DepthRequested);
            //fields[41] = ConvertBoolToYesNo(exApp.SiteInfo.Indoors);
            //fields[42] = ConvertBoolToYesNo(exApp.SiteInfo.Outdoors);
            //fields[43] = ConvertBoolToYesNo(exApp.SiteInfo.ShellScheme);
            //fields[44] = ConvertBoolToYesNo(exApp.SiteInfo.Amplification);
            //fields[45] = CheckStringForNull(exApp.NotesAndRequests);
            //fields[46] = CheckStringForNull(exApp.ProductInfo.Products);
            //fields[47] = CheckStringForNull(exApp.ProductInfo.CaravanBrand1);
            //fields[48] = CheckStringForNull(exApp.ProductInfo.CaravanBrand2);
            //fields[49] = CheckStringForNull(exApp.ProductInfo.CaravanBrand3);
            //fields[50] = CheckStringForNull(exApp.ProductInfo.CaravanBrand4);
            //fields[51] = CheckStringForNull(exApp.ProductInfo.CaravanBrand5);
            //fields[52] = CheckStringForNull(exApp.ProductInfo.CaravanBrand6);
            //fields[53] = CheckStringForNull(exApp.ProductInfo.CamperTrailerBrand1);
            //fields[54] = CheckStringForNull(exApp.ProductInfo.CamperTrailerBrand2);
            //fields[55] = CheckStringForNull(exApp.ProductInfo.CamperTrailerBrand3);
            //fields[56] = CheckStringForNull(exApp.ProductInfo.CamperTrailerBrand4);
            //fields[57] = CheckStringForNull(exApp.ProductInfo.CamperTrailerBrand5);
            //fields[58] = CheckStringForNull(exApp.ProductInfo.CamperTrailerBrand6);
            //fields[59] = CheckStringForNull(exApp.ProductInfo.MotorhomeBrand1);
            //fields[60] = CheckStringForNull(exApp.ProductInfo.MotorhomeBrand2);
            //fields[61] = CheckStringForNull(exApp.ProductInfo.MotorhomeBrand3);
            //fields[62] = CheckStringForNull(exApp.ProductInfo.MotorhomeBrand4);
            //fields[63] = CheckStringForNull(exApp.ProductInfo.FifthwheelerBrand1);
            //fields[64] = CheckStringForNull(exApp.ProductInfo.FifthwheelerBrand2);
            //fields[65] = CheckStringForNull(exApp.ProductInfo.FifthwheelerBrand3);
            //fields[66] = CheckStringForNull(exApp.ProductInfo.FifthwheelerBrand4);
            //fields[67] = ConvertBoolToYesNo(exApp.ProductInfo.AccessoriesParts);
            //fields[68] = ConvertBoolToYesNo(exApp.ProductInfo.AdvisoryServices);
            //fields[69] = ConvertBoolToYesNo(exApp.ProductInfo.AnnexesAwnings);
            //fields[70] = ConvertBoolToYesNo(exApp.ProductInfo.ArtUnionsFundraising);
            //fields[71] = ConvertBoolToYesNo(exApp.ProductInfo.CabinsHomes);
            //fields[72] = ConvertBoolToYesNo(exApp.ProductInfo.CamperTrailers);
            //fields[73] = ConvertBoolToYesNo(exApp.ProductInfo.CampingEquipmentTents);
            //fields[74] = ConvertBoolToYesNo(exApp.ProductInfo.CaravansPopTops);
            //fields[75] = ConvertBoolToYesNo(exApp.ProductInfo.CarportsCaravanCovers);
            //fields[76] = ConvertBoolToYesNo(exApp.ProductInfo.ClothingHats);
            //fields[77] = ConvertBoolToYesNo(exApp.ProductInfo.Clubs);
            //fields[78] = ConvertBoolToYesNo(exApp.ProductInfo.Communication);
            //fields[79] = ConvertBoolToYesNo(exApp.ProductInfo.FifthWheelers);
            //fields[80] = ConvertBoolToYesNo(exApp.ProductInfo.Hire);
            //fields[81] = ConvertBoolToYesNo(exApp.ProductInfo.Insurance);
            //fields[82] = ConvertBoolToYesNo(exApp.ProductInfo.MarineFishingBoatTrailers);
            //fields[83] = ConvertBoolToYesNo(exApp.ProductInfo.MotorhomesCampervansConversions);
            //fields[84] = ConvertBoolToYesNo(exApp.ProductInfo.Publications);
            //fields[85] = ConvertBoolToYesNo(exApp.ProductInfo.RoofTopCampers);
            //fields[86] = ConvertBoolToYesNo(exApp.ProductInfo.SlideOnTraytopCampers);
            //fields[87] = ConvertBoolToYesNo(exApp.ProductInfo.TouringTourismCaravanParks);
            //fields[88] = ConvertBoolToYesNo(exApp.ProductInfo.TowingEquipment);
            //fields[89] = ConvertBoolToYesNo(exApp.ProductInfo.VehiclesVehicleAccessories4WDAccessories);
            //fields[90] = ConvertBoolToYesNo(exApp.ProductInfo.OtherProductsServices);
            //fields[91] = CheckStringForNull(exApp.ProductInfo.AccessHeight);
            //
            //return fields;
        }

        //private string ConvertBoolToYesNo(bool value)
        //{
        //    if (value)
        //        return "Yes";
        //    else
        //        return "No";
        //}

        private string CheckStringForNull(string value)
        {
            if (String.IsNullOrEmpty(value))
                return "";
            else
                return value;
        }

        #endregion

        #region Emails

        public ActionResult Emails(Guid? EmailId, bool ForwardToEmail = false)
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                var model = new EmailsModel()
                {
                    ForwardToEmail = ForwardToEmail,
                    EmailId = (Guid)(EmailId == null ? Guid.Empty : EmailId)
                };

                return View(model);
            }
        }

        public ActionResult EmailList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView(model);
            }
        }

        public ActionResult ChangeEmailListSortOrder(bool SortByDateCreated)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                emailSettings.SortByDateCreated = SortByDateCreated;
                db.SaveChanges();

                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView("EmailList", model);
            }
        }

        public ActionResult EmailListByDateCreated()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderByDescending(x => x.DateCreated).ToList();
                var monthList = new List<DateTime>();
                //Get the current month and remove all days, hours, minutes, seconds, and milliseconds
                var timeNow = DateTime.Now;
                var timeToSubtract = new TimeSpan(timeNow.Day - 1, timeNow.Hour, timeNow.Minute, timeNow.Second, timeNow.Millisecond);
                var thisMonth = timeNow.Subtract(timeToSubtract);

                monthList.Add(thisMonth);
                //Add the preceeding 11 months to the month list
                for (int i = 1; i < 12; i++)
                {
                    monthList.Add(thisMonth.AddMonths(-i));
                }

                var model = new EmailListByDateCreated()
                {
                    Emails = emails,
                    Last12Months = monthList
                };

                return PartialView(model);
            }
        }

        public ActionResult EmailListBySubject()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Subject).ToList();
                var model = new EmailListBySubjectModel()
                {
                    Emails = emails
                };

                return PartialView(model);
            }
        }

        public ActionResult CreateNewEmail()
        {
            using (var db = new CaravanQLDContext())
            {
                var email = new Email();
                email.LinkedShowId = GetCurrentShow().Id;
                db.Emails.Add(email);
                db.SaveChanges();

                var model = new EditEmailModel()
                {
                    Email = email
                };

                return PartialView("EditEmail", model);
            }
        }

        public ActionResult SetInitialEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                foreach (var email in emails)
                {
                    email.IsInitialEmail = false;
                }

                var initialEmail = emails.FirstOrDefault(x => x.Id == id);
                initialEmail.IsInitialEmail = true;
                db.SaveChanges();

                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView("EmailList", model);
            }
        }

        public ActionResult EditEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var model = new EditEmailModel()
                {
                    Email = email
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditEmail(EditEmailModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                    //If this is now the initial email, set all others to false
                    if (model.Email.IsInitialEmail)
                    {
                        foreach (var e in emails)
                        {
                            e.IsInitialEmail = false;
                        }
                    }

                    var email = emails.FirstOrDefault(x => x.Id == model.Email.Id);
                    email.Subject = model.Email.Subject;
                    email.Body = model.Email.Body;
                    email.IsInitialEmail = model.Email.IsInitialEmail;
                    db.SaveChanges();
                }
            }

            return PartialView(model);
        }

        public ActionResult SendTestEmail(Guid id)
        {
            try
            {
                using (var db = new CaravanQLDContext())
                {
                    var email = db.Emails.FirstOrDefault(x => x.Id == id);
                    if (email != null)
                    {
                        var currentShow = GetCurrentShow();
                        var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                        var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id).ToList();
                        
                        if (!String.IsNullOrEmpty(emailSettings.TestEmailAddress))
                        {
                            //Get the test exhibitor
                            var testExhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == emailSettings.TestExhibitorShowID && x.LinkedShowId == currentShow.Id);

#if Debug
                                SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
#else
                            SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                            client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
#endif
                            MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                            MailAddress to = new MailAddress(emailSettings.TestEmailAddress);
                            MailMessage message = new MailMessage(from, to);

                            var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == testExhibitor.Id).ToList();
                            var newBody = AppMethods.ReplaceSmartFields(currentShow, testExhibitor, formSections, formFieldValues, email.Body) + GetEmailFooter();
                            
                            message.Body = newBody;
                            message.Subject = email.Subject;
                            message.IsBodyHtml = true;
                            client.Send(message);
                        }
                    }
                };
            }
            catch (Exception exc)
            {
                //Triggers the failure message
                return Json(new { success = false });
            }

            //Triggers the success message
            var model = new EmailsModel()
            {
                ForwardToEmail = false,
                EmailId = Guid.Empty
            };
            return PartialView("Emails", model);
        }

        public ActionResult SendEmail(Guid id)
        {
            var success = true;

            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                if (email != null)
                {
                    var currentShow = GetCurrentShow();
                    var emailExs = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).Select(x => x.ExhibitorApplicationId);
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    var exhibitors = db.ExhibitorApplications.Where(x => emailExs.Contains(x.Id));
                    var failedExhibitors = new List<ExhibitorApplication>();
                    var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id).ToList();

                    foreach (var ex in exhibitors)
                    {
                        try
                        {
#if Debug
                                SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
#else
                            SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                            client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
#endif
                            MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                            var toAddress = GetExhibitorEmailAndLogin(ex.ShowId);
                            MailAddress to = new MailAddress(toAddress);
                            MailMessage message = new MailMessage(from, to);

                            var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == ex.Id).ToList();
                            var newBody = AppMethods.ReplaceSmartFields(currentShow, ex, formSections, formFieldValues, email.Body) + GetEmailFooter();

                            message.Body = newBody;
                            message.Subject = email.Subject;
                            message.IsBodyHtml = true;
                            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                            client.Send(message);

                            var emailLog = new EmailLog()
                            {
                                DateSent = DateTime.Now,
                                EmailId = id,
                                ExhibitorApplicationId = ex.Id,
                                LinkedShowId = currentShow.Id
                            };

                            db.EmailLogs.Add(emailLog);
                        }
                        catch (Exception exc)
                        {
                            failedExhibitors.Add(ex);
                        }
                    }

                    db.SaveChanges();

                    //Send an email to Caravan Queensland with the results of the email
                    var completionEmail = email.Subject + " has been sent.";
                    if (failedExhibitors.Count <= 0)
                    {
                        completionEmail += "<p>There were no problems sending this email.</p>";
                    }
                    else
                    {
                        completionEmail += "<p>The email failed to send to the following exhibitors.</p>";
                        foreach (var failedEx in failedExhibitors)
                        {
                            var toAddress = GetExhibitorEmailAndLogin(failedEx.ShowId); ;
                            completionEmail += "<br />" + (!String.IsNullOrEmpty(GetEmailLogsDisplayValue(failedEx.ShowId)) ? GetEmailLogsDisplayValue(failedEx.ShowId) : toAddress);
                        }
                    }

#if Debug
                        SmtpClient client2 = new SmtpClient("smtp.virginbroadband.com.au");
#else
                    SmtpClient client2 = new SmtpClient(AppConstants.SmptHostName, 587);
                    client2.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
#endif
                    MailAddress from2 = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                    MailAddress to2 = new MailAddress(emailSettings.AdminEmailAddress);
                    MailMessage message2 = new MailMessage(from2, to2);
                    message2.Body = completionEmail;
                    message2.Subject = "Email Report - " + email.Subject;
                    message2.IsBodyHtml = true;
                    client2.Send(message2);
                }
            };

            //Triggers the failure message or Triggers the success message
            if (!success)
                return Json(new { success = false });
            else
            {
                var model = new EmailsModel()
                {
                    ForwardToEmail = false,
                    EmailId = Guid.Empty
                };
                return PartialView("Emails", model);
            }
        }

        public string GetExhibitorEmailAndLogin(string ShowID)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = "";
                var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                try
                {
                    if (!String.IsNullOrEmpty(emailSetting.ExhibitorEmailAndLoginField))
                    {
                        var formFieldId = Guid.Parse(emailSetting.ExhibitorEmailAndLoginField);
                        var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == ShowID && x.LinkedShowId == currentShow.Id);
                        var formFieldValue = db.FormFieldValues.First(x => x.FormFieldId == formFieldId && x.ExhibitorApplicationId == exhibitor.Id);

                        if (formFieldValue != null && !String.IsNullOrEmpty(formFieldValue.Value))
                        {
                            email = formFieldValue.Value;
                        }
                    }
                }
                catch (Exception exc)
                {

                }

                return email;
            }
        }

        public string GetEmailLogsDisplayValue(string ShowID)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = "";
                var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                try
                {
                    if (!String.IsNullOrEmpty(emailSetting.EmailLogsDisplayField))
                    {
                        var formFieldId = Guid.Parse(emailSetting.EmailLogsDisplayField);
                        var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.ShowId == ShowID && x.LinkedShowId == currentShow.Id);
                        var formFieldValue = db.FormFieldValues.First(x => x.FormFieldId == formFieldId && x.ExhibitorApplicationId == exhibitor.Id);

                        if (formFieldValue != null && !String.IsNullOrEmpty(formFieldValue.Value))
                        {
                            email = formFieldValue.Value;
                        }
                    }
                }
                catch (Exception exc)
                {

                }

                return email;
            }
        }

        public ActionResult SendOneEmail(Guid EmailId, Guid ExhibitorId)
        {
            var success = true;

            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == EmailId);
                if (email != null)
                {
                    var currentShow = GetCurrentShow();
                    var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    var exhibitor = db.ExhibitorApplications.FirstOrDefault(x => x.Id == ExhibitorId);
                    var failedExhibitors = new List<ExhibitorApplication>();
                    var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == currentShow.Id).ToList();

                    try
                    {
#if Debug
                            SmtpClient client = new SmtpClient("smtp.virginbroadband.com.au");
#else
                        SmtpClient client = new SmtpClient(AppConstants.SmptHostName, 587);
                        client.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
#endif
                        MailAddress from = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                        var toAddress = GetExhibitorEmailAndLogin(exhibitor.ShowId);
                        MailAddress to = new MailAddress(toAddress);
                        MailMessage message = new MailMessage(from, to);

                        var formFieldValues = db.FormFieldValues.Where(x => x.ExhibitorApplicationId == exhibitor.Id).ToList();
                        var newBody = AppMethods.ReplaceSmartFields(currentShow, exhibitor, formSections, formFieldValues, email.Body) + GetEmailFooter();
                        
                        message.Body = newBody;
                        message.Subject = email.Subject;
                        message.IsBodyHtml = true;
                        message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        client.Send(message);

                        var emailLog = new EmailLog()
                        {
                            DateSent = DateTime.Now,
                            EmailId = EmailId,
                            ExhibitorApplicationId = ExhibitorId,
                            LinkedShowId = currentShow.Id
                        };

                        db.EmailLogs.Add(emailLog);
                    }
                    catch (Exception exc)
                    {
                        success = false;
                        failedExhibitors.Add(exhibitor);
                    }

                    db.SaveChanges();

                    //Send an email to Caravan Queensland with the results of the email
                    //var completionEmail = email.Subject + " has been sent.";
                    //if (failedExhibitors.Count <= 0)
                    //{
                    //    completionEmail += "<p>There were no problems sending this email.</p>";
                    //}
                    //else
                    //{
                    //    completionEmail += "<p>The email failed to send to the following exhibitors.</p>";
                    //    foreach (var failedEx in failedExhibitors)
                    //    {
                    //        completionEmail += "<br />" + (!String.IsNullOrEmpty(failedEx.ShowListingName) ? failedEx.ShowListingName : failedEx.Email);
                    //    }
                    //}

                    //#if Debug
                    //    SmtpClient client2 = new SmtpClient("smtp.virginbroadband.com.au");
                    //#else
                    //SmtpClient client2 = new SmtpClient(AppConstants.SmptHostName);
                    //client2.Credentials = new NetworkCredential(AppConstants.SmptUsername, AppConstants.SmptPassword);
                    //#endif
                    //MailAddress from2 = new MailAddress(emailSettings.AdminEmailAddress, emailSettings.EmailSender);
                    //MailAddress to2 = new MailAddress(emailSettings.AdminEmailAddress);
                    //MailMessage message2 = new MailMessage(from2, to2);
                    //message2.Body = completionEmail;
                    //message2.Subject = "Email Report - " + email.Subject;
                    //message2.IsBodyHtml = true;
                    //client2.Send(message2);
                }
            };

            //Triggers the failure message or Triggers the success message
            if (!success)
                return Json(new { success = false });
            else
            {
                var model = new EmailsModel()
                {
                    ForwardToEmail = false,
                    EmailId = Guid.Empty
                };
                return PartialView("Emails", model);
            }
        }

        private string GetEmailFooter()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var emailFooter = String.Format("<hr /><p align='center' style='color:#A9A9A9;'>{0}<br />Phone: {1} | Fax: {2} | {3} <br /> Email: <a href='mailto:{4}'>{4}</a> | Web: <a href='{5}'>{5}</a></p>",
                    emailSettings.CompanyName, emailSettings.ContactNumber, emailSettings.ContactFax, emailSettings.ContactPostalAddress, emailSettings.ContactEmail, emailSettings.Website);

                return emailFooter;
            }
        }

        public ActionResult DeleteEmail(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                db.Emails.Remove(email);
                db.SaveChanges();

                var currentShow = GetCurrentShow();
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);

                var model = new EmailListModel()
                {
                    SortByDateCreated = emailSettings.SortByDateCreated
                };

                return PartialView("EmailList", model);
            }
        }

        public ActionResult ClearEmailLogs(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var model = new LogListForEmailModel()
                {
                    EmailLogs = new List<EmailLog>(),
                    EmailId = id
                };

                return PartialView("LogListForEmail", model);
            }
        }

        public ActionResult ClearAllEmailLogs()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var model = new LogListForAllModel()
                {
                    EmailLogs = new List<EmailLog>()
                };

                return PartialView("LogListForAll", model);
            }
        }

        public ActionResult ClearExhibitorLogs(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Where(x => x.ExhibitorApplicationId == id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var model = new LogListForExhibitorModel()
                {
                    EmailLogs = new List<EmailLog>(),
                    ExhibitorId = id
                };

                return PartialView("LogListForExhibitor", model);
            }
        }

        public ActionResult ChangeEmailsExhibitors(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();
                var formFieldValues = GetDisplayFormFieldsList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs,
                    FormFieldValues = formFieldValues
                };

                return PartialView(model);
            }
        }

        public List<FormFieldValue> GetDisplayFormFieldsList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formFieldValues = new List<FormFieldValue>();
                var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                try
                {
                    if (!String.IsNullOrEmpty(emailSetting.EmailLogsDisplayField))
                    {
                        var formFieldId = Guid.Parse(emailSetting.EmailLogsDisplayField);
                        formFieldValues = db.FormFieldValues.Include("ExhibitorApplication").Include("FormField").Where(x => x.FormFieldId == formFieldId && x.ExhibitorApplication.LinkedShowId == currentShow.Id).OrderBy(x => x.Value).ToList();
                    }
                }
                catch (Exception exc)
                {

                }

                return formFieldValues;
            }
        }

        public List<FormFieldValue> GetEmailFormFieldsList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formFieldValues = new List<FormFieldValue>();
                var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                try
                {
                    if (!String.IsNullOrEmpty(emailSetting.ExhibitorEmailAndLoginField))
                    {
                        var formFieldId = Guid.Parse(emailSetting.ExhibitorEmailAndLoginField);
                        formFieldValues = db.FormFieldValues.Include("ExhibitorApplication").Include("FormField").Where(x => x.FormFieldId == formFieldId && x.ExhibitorApplication.LinkedShowId == currentShow.Id).OrderBy(x => x.Value).ToList();
                    }
                }
                catch (Exception exc)
                {

                }

                return formFieldValues;
            }
        }

        public ActionResult AddExhibitorToEmail(Guid EmailId, Guid ExhibitorId)
        {
            using (var db = new CaravanQLDContext())
            {
                var existingEmailEx = db.ExhibitorApplicationEmails.FirstOrDefault(x => x.EmailId == EmailId && x.ExhibitorApplicationId == ExhibitorId);

                if (existingEmailEx == null)
                {
                    var emailEx = new ExhibitorApplicationEmail()
                    {
                        EmailId = EmailId,
                        ExhibitorApplicationId = ExhibitorId
                    };

                    db.ExhibitorApplicationEmails.Add(emailEx);
                }
                else
                {
                    db.ExhibitorApplicationEmails.Remove(existingEmailEx);
                }

                db.SaveChanges();

                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == EmailId);
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == EmailId).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();
                var formFieldValues = GetDisplayFormFieldsList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs,
                    FormFieldValues = formFieldValues
                };

                return PartialView("ChangeEmailsExhibitors", model);
            }
        }

        public ActionResult SelectAllExhibitors(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();

                foreach (var exhibitor in exhibitors)
                {
                    var existingEmailEx = exhibitorEmails.FirstOrDefault(x => x.EmailId == id && x.ExhibitorApplicationId == exhibitor.Id);

                    if (existingEmailEx == null)
                    {
                        var emailEx = new ExhibitorApplicationEmail()
                        {
                            EmailId = id,
                            ExhibitorApplicationId = exhibitor.Id
                        };

                        db.ExhibitorApplicationEmails.Add(emailEx);
                    }
                }

                db.SaveChanges();
                exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var formFieldValues = GetDisplayFormFieldsList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs,
                    FormFieldValues = formFieldValues
                };

                return PartialView("ChangeEmailsExhibitors", model);
            }
        }

        public ActionResult DeselectAllExhibitors(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var email = db.Emails.FirstOrDefault(x => x.Id == id);
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.TradingName).ThenBy(x => x.ExhibitorEmail).ToList();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var emailLogs = db.EmailLogs.Where(x => x.EmailId == email.Id).OrderBy(x => x.DateSent).ToList();

                foreach (var exhibitor in exhibitors)
                {
                    var existingEmailEx = exhibitorEmails.FirstOrDefault(x => x.EmailId == id && x.ExhibitorApplicationId == exhibitor.Id);

                    if (existingEmailEx != null)
                    {
                        db.ExhibitorApplicationEmails.Remove(existingEmailEx);
                    }
                }

                db.SaveChanges();
                exhibitorEmails = db.ExhibitorApplicationEmails.Where(x => x.EmailId == id).ToList();
                var formFieldValues = GetDisplayFormFieldsList();

                var model = new ChangeEmailsExhibitors()
                {
                    CurrentEmail = email,
                    ExhibitorApplicationEmails = exhibitorEmails,
                    EmailLogs = emailLogs,
                    FormFieldValues = formFieldValues
                };

                return PartialView("ChangeEmailsExhibitors", model);
            }
        }

        public ActionResult LogListForExhibitor(Guid id, int page = 1)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Include("Email").Where(x => x.ExhibitorApplicationId == id).OrderByDescending(x => x.DateSent).ToList();
                var currentShow = GetCurrentShow();
                var numLogs = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id).LogsPerPage;
                var displayFormFieldValues = GetDisplayFormFieldsList();
                var emailFormFieldValues = GetEmailFormFieldsList();

                var model = new LogListForExhibitorModel()
                {
                    CurrentPage = page,
                    TotalRecords = emailLogs.Count,
                    LogsPerPage = numLogs,
                    ExhibitorId = id,
                    EmailLogs = emailLogs.GetRange(((page - 1) * numLogs), (emailLogs.Count > page * numLogs ? numLogs : (emailLogs.Count - ((page - 1) * numLogs)))),
                    DisplayFormFieldValues = displayFormFieldValues,
                    EmailFormFieldValues = emailFormFieldValues
                };

                return PartialView(model);
            }
        }

        public ActionResult LogListForEmail(Guid id, int page = 1)
        {
            using (var db = new CaravanQLDContext())
            {
                var emailLogs = db.EmailLogs.Include("Email").Where(x => x.EmailId == id).OrderByDescending(x => x.DateSent).ToList();
                var currentShow = GetCurrentShow();
                var numLogs = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id).LogsPerPage;
                var displayFormFieldValues = GetDisplayFormFieldsList();
                var emailFormFieldValues = GetEmailFormFieldsList();

                var model = new LogListForEmailModel()
                {
                    CurrentPage = page,
                    TotalRecords = emailLogs.Count,
                    LogsPerPage = numLogs,
                    EmailId = id,
                    EmailLogs = emailLogs.GetRange(((page - 1) * numLogs), (emailLogs.Count > page * numLogs ? numLogs : (emailLogs.Count - ((page - 1) * numLogs)))),
                    DisplayFormFieldValues = displayFormFieldValues,
                    EmailFormFieldValues = emailFormFieldValues
                };

                return PartialView(model);
            }
        }

        public ActionResult LogListForAll(int page = 1)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var emailLogs = db.EmailLogs.Include("Email").Where(x => x.LinkedShowId == currentShow.Id).OrderByDescending(x => x.DateSent).ToList();
                var numLogs = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id).LogsPerPage;
                var displayFormFieldValues = GetDisplayFormFieldsList();
                var emailFormFieldValues = GetEmailFormFieldsList();

                var model = new LogListForAllModel()
                {
                    CurrentPage = page,
                    TotalRecords = emailLogs.Count,
                    LogsPerPage = numLogs,
                    EmailLogs = emailLogs.GetRange(((page - 1) * numLogs), (emailLogs.Count > page * numLogs ? numLogs : (emailLogs.Count - ((page - 1) * numLogs)))),
                    DisplayFormFieldValues = displayFormFieldValues,
                    EmailFormFieldValues = emailFormFieldValues
                };

                return PartialView(model);
            }
        }

        public ActionResult LogListForSearch(string searchType, Guid searchId, string date, string subject, string tradingName, string contactEmail)
        {
            using (var db = new CaravanQLDContext())
            {
                var searchResult = new List<EmailLog>();
                var displayFormFieldValues = GetDisplayFormFieldsList();
                var emailFormFieldValues = GetEmailFormFieldsList();

                if (searchType == "email")
                {
                    var emailLogs = db.EmailLogs.Include("Email").Where(x => x.EmailId == searchId).OrderByDescending(x => x.DateSent).ToList();

                    if (!String.IsNullOrEmpty(date))
                    {
                        var searchDate = DateTime.Parse(date);
                        searchResult = emailLogs.Where(x => x.DateSent >= searchDate && x.DateSent < searchDate.AddDays(1)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(subject))
                    {
                        searchResult = emailLogs.Where(x => x.Email.Subject.ToLower().Contains(subject.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(tradingName))
                    {
                        var searchDisplayFormFieldValues = displayFormFieldValues.Where(x => x.Value.ToLower().Contains(tradingName.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResult = emailLogs.Where(x => searchDisplayFormFieldValues.Contains(x.ExhibitorApplicationId)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(contactEmail))
                    {
                        var searchEmailFormFieldValues = emailFormFieldValues.Where(x => x.Value.ToLower().Contains(contactEmail.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResult = emailLogs.Where(x => searchEmailFormFieldValues.Contains(x.ExhibitorApplicationId)).ToList();
                    }
                }
                else if (searchType == "exhibitor")
                {
                    var emailLogs = db.EmailLogs.Include("Email").Where(x => x.ExhibitorApplication.Id == searchId).OrderByDescending(x => x.DateSent).ToList();

                    if (!String.IsNullOrEmpty(date))
                    {
                        var searchDate = DateTime.Parse(date);
                        searchResult = emailLogs.Where(x => x.DateSent >= searchDate && x.DateSent < searchDate.AddDays(1)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(subject))
                    {
                        searchResult = emailLogs.Where(x => x.Email.Subject.ToLower().Contains(subject.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(tradingName))
                    {
                        var searchDisplayFormFieldValues = displayFormFieldValues.Where(x => x.Value.ToLower().Contains(tradingName.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResult = emailLogs.Where(x => searchDisplayFormFieldValues.Contains(x.ExhibitorApplicationId)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(contactEmail))
                    {
                        var searchEmailFormFieldValues = emailFormFieldValues.Where(x => x.Value.ToLower().Contains(contactEmail.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResult = emailLogs.Where(x => searchEmailFormFieldValues.Contains(x.ExhibitorApplicationId)).ToList();
                    }
                }
                else if (searchType == "all")
                {
                    var currentShow = GetCurrentShow();
                    var emailLogs = db.EmailLogs.Include("Email").Where(x => x.LinkedShowId == currentShow.Id).OrderByDescending(x => x.DateSent).ToList();

                    if (!String.IsNullOrEmpty(date))
                    {
                        var searchDate = DateTime.Parse(date);
                        searchResult = emailLogs.Where(x => x.DateSent >= searchDate && x.DateSent < searchDate.AddDays(1)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(subject))
                    {
                        searchResult = emailLogs.Where(x => x.Email.Subject.ToLower().Contains(subject.ToLower())).ToList();
                    }
                    else if (!String.IsNullOrEmpty(tradingName))
                    {
                        var searchDisplayFormFieldValues = displayFormFieldValues.Where(x => x.Value.ToLower().Contains(tradingName.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResult = emailLogs.Where(x => searchDisplayFormFieldValues.Contains(x.ExhibitorApplicationId)).ToList();
                    }
                    else if (!String.IsNullOrEmpty(contactEmail))
                    {
                        var searchEmailFormFieldValues = emailFormFieldValues.Where(x => x.Value.ToLower().Contains(contactEmail.ToLower())).Select(x => x.ExhibitorApplicationId).ToList();
                        searchResult = emailLogs.Where(x => searchEmailFormFieldValues.Contains(x.ExhibitorApplicationId)).ToList();
                    }
                }

                var model = new LogListForSearchModel()
                {
                    SearchId = searchId,
                    SearchType = searchType,
                    EmailLogs = searchResult,
                    DisplayFormFieldValues = displayFormFieldValues,
                    EmailFormFieldValues = emailFormFieldValues
                };

                return PartialView(model);
            }
        }

        #endregion

        #region Email Settings

        public ActionResult EmailSettings()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var emailSetting = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    if (emailSetting == null)
                    {
                        emailSetting = new EmailSetting();
                        emailSetting.LinkedShowId = GetCurrentShow().Id;
                        db.EmailSettings.Add(emailSetting);
                        db.SaveChanges();
                    }

                    var model = new EmailSettingsModel()
                    {
                        EmailSetting = emailSetting,
                        EmailLogsDisplayFieldList = CreateEmailSettingsSelectList(),
                        ExhibitorEmailAndLoginFieldList = CreateEmailSettingsSelectList()
                    };

                    return View(model);
                }
            }
        }

        [HttpPost]
        public ActionResult EmailSettings(EmailSettingsModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new CaravanQLDContext())
                {
                    var currentShow = GetCurrentShow();
                    var es = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == currentShow.Id);
                    es.CompanyName = model.EmailSetting.CompanyName;
                    es.ContactEmail = model.EmailSetting.ContactEmail;
                    es.ContactNumber = model.EmailSetting.ContactNumber;
                    es.ContactFax = model.EmailSetting.ContactFax;
                    es.ContactPostalAddress = model.EmailSetting.ContactPostalAddress;
                    es.Website = model.EmailSetting.Website;
                    es.TestEmailAddress = model.EmailSetting.TestEmailAddress;
                    es.TestExhibitorShowID = model.EmailSetting.TestExhibitorShowID;
                    es.LogsPerPage = model.EmailSetting.LogsPerPage;
                    es.AdminEmailAddress = model.EmailSetting.AdminEmailAddress;
                    es.ApplicationEmailAddress = model.EmailSetting.ApplicationEmailAddress;
                    es.EmailSender = model.EmailSetting.EmailSender;
                    es.EmailLogsDisplayField = model.EmailSetting.EmailLogsDisplayField;
                    es.ExhibitorEmailAndLoginField = model.EmailSetting.ExhibitorEmailAndLoginField;

                    db.SaveChanges();

                    ModelState.Clear();
                }
            }

            model.EmailLogsDisplayFieldList = CreateEmailSettingsSelectList();
            model.ExhibitorEmailAndLoginFieldList = CreateEmailSettingsSelectList();

            return View(model);
        }

        private List<SelectListItem> CreateEmailSettingsSelectList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formSections = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFields = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection && x.DataType != FormDataType.TextField).OrderBy(x => x.Name).ToList();
                var formFieldSelectList = new List<SelectListItem>();

                formFieldSelectList.Add(new SelectListItem() { Text = "SHOW ID", Value = "" });
                formFieldSelectList.Add(new SelectListItem() { Text = "-Show ID", Value = "", Selected = true });

                foreach (var formSection in formSections)
                {
                    formFieldSelectList.Add(new SelectListItem() { Text = formSection.Name.ToUpper(), Value = "" });

                    var sectionFields = formFields.Where(x => x.FormId == formSection.Id).OrderBy(x => x.Name);
                    foreach (var field in sectionFields)
                    {
                        formFieldSelectList.Add(new SelectListItem() { Text = " -" + field.Name, Value = field.Id.ToString() });
                    }
                }
                return formFieldSelectList;
            }
        }

        #endregion

        #region Pages

        public ActionResult Pages()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult PageList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var pages = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
                var model = new PageListModel()
                {
                    ContentPages = pages
                };

                return PartialView(model);
            }
        }

        public ActionResult AddNewPage()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var lastPage = db.ContentPages.Where(x => x.PageType == PageType.Custom && x.LinkedShowId == currentShow.Id).OrderByDescending(x => x.Order).FirstOrDefault();
                
                var page = new ContentPage()
                {
                    Order = lastPage != null ? lastPage.Order + 1 : 0,
                    LinkedShowId = GetCurrentShow().Id
                };

                db.ContentPages.Add(page);
                db.SaveChanges();

                var model = new EditPageModel()
                {
                    ContentPage = page
                };

                return PartialView("EditPage", model);
            }
        }

        public ActionResult EditPage(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var page = db.ContentPages.FirstOrDefault(x => x.Id == id);

                var model = new EditPageModel()
                {
                    ContentPage = page
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditPage(EditPageModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var contentPages = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var page = db.ContentPages.FirstOrDefault(x => x.Id == model.ContentPage.Id);
                page.Name = model.ContentPage.Name;
                page.IsCurrent = model.ContentPage.IsCurrent;
                page.TextContent = model.ContentPage.TextContent;

                if (page.PageType != model.ContentPage.PageType && (page.PageType == PageType.Custom && model.ContentPage.PageType != PageType.Custom))
                {
                    var oldOrder = page.Order;

                    var pagesToAdjust = contentPages.Where(x => x.Order > oldOrder && x.PageType == PageType.Custom).ToList();
                    foreach (var p in pagesToAdjust)
                    {
                        p.Order = p.Order - 1;
                    }

                    page.Order = 999;
                }
                else if (page.Order != model.ContentPage.Order || (page.PageType != model.ContentPage.PageType && model.ContentPage.PageType == PageType.Custom))
                {
                    var oldOrder = page.Order;
                    var newOrder = model.ContentPage.Order;
                    var maxOrder = contentPages.Where(x => x.PageType == PageType.Custom).Select(x => x.Order).Max();

                    if (newOrder < 0)
                        newOrder = 0;

                    if (newOrder > maxOrder)
                        newOrder = maxOrder;

                    //it already was a custom page
                    if (page.PageType == PageType.Custom)
                    {
                        if (newOrder > oldOrder)
                        {
                            var pagesToAdjust = contentPages.Where(x => x.Order > oldOrder && x.Order <= newOrder);
                            foreach (var p in pagesToAdjust)
                            {
                                p.Order = p.Order - 1;
                            }
                        }
                        else if (newOrder < oldOrder)
                        {
                            var pagesToAdjust = contentPages.Where(x => x.Order < oldOrder && x.Order >= newOrder);
                            foreach (var p in pagesToAdjust)
                            {
                                p.Order = p.Order + 1;
                            }
                        }
                    }
                    //it is a custom page now
                    else
                    {
                        var pagesToAdjust = contentPages.Where(x => x.Order > newOrder && x.PageType == PageType.Custom).ToList();
                        foreach (var p in pagesToAdjust)
                        {
                            p.Order = p.Order + 1;
                        }
                    }

                    page.Order = newOrder;
                }

                page.PageType = model.ContentPage.PageType;
                db.SaveChanges();

                ModelState.Clear();

                model.ContentPage = page;

                return PartialView(model);
            }
        }

        public ActionResult DeletePage(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var page = db.ContentPages.FirstOrDefault(x => x.Id == id);
                var currentShow = GetCurrentShow();

                if (page.PageType == PageType.Custom)
                {
                    var pagesToAdjust = db.ContentPages.Where(x => x.PageType == PageType.Custom && x.Order > page.Order && x.LinkedShowId == currentShow.Id);
                    foreach (var p in pagesToAdjust)
                    {
                        p.Order = p.Order - 1;
                    }
                }
                db.ContentPages.Remove(page);
                db.SaveChanges();

                var pages = db.ContentPages.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
                var model = new PageListModel()
                {
                    ContentPages = pages
                };

                return PartialView("PageList", model);
            }
        }

        #endregion

        #region Portal Settings

        public ActionResult PortalSettings()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult DeleteAllData()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView(model);
            }
        }

        public ActionResult DeleteData()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var ee in exhibitorEmails)
                {
                    db.ExhibitorApplicationEmails.Remove(ee);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var email in emails)
                {
                    db.Emails.Remove(email);
                }

                var siteInfos = db.SiteInfos.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var siteInfo in siteInfos)
                {
                    db.SiteInfos.Remove(siteInfo);
                }

                var productInfos = db.ProductInfos.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var productInfo in productInfos)
                {
                    db.ProductInfos.Remove(productInfo);
                }

                var exhibitorApplications = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var exhibitorApplication in exhibitorApplications)
                {
                    db.ExhibitorApplications.Remove(exhibitorApplication);
                }

                db.SaveChanges();

                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView("DeleteAllData", model);
            }
        }

        public ActionResult DeleteExhibitors()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var ee in exhibitorEmails)
                {
                    db.ExhibitorApplicationEmails.Remove(ee);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var siteInfos = db.SiteInfos.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var siteInfo in siteInfos)
                {
                    db.SiteInfos.Remove(siteInfo);
                }

                var productInfos = db.ProductInfos.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var productInfo in productInfos)
                {
                    db.ProductInfos.Remove(productInfo);
                }

                var exhibitorApplications = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var exhibitorApplication in exhibitorApplications)
                {
                    db.ExhibitorApplications.Remove(exhibitorApplication);
                }

                db.SaveChanges();

                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView("DeleteAllData", model);
            }
        }

        public ActionResult DeleteEmails()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitorEmails = db.ExhibitorApplicationEmails.Include("ExhibitorApplication").Where(x => x.ExhibitorApplication.LinkedShowId == currentShow.Id);
                foreach (var ee in exhibitorEmails)
                {
                    db.ExhibitorApplicationEmails.Remove(ee);
                }

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }

                var emails = db.Emails.Where(x => x.LinkedShowId == currentShow.Id);
                foreach (var email in emails)
                {
                    db.Emails.Remove(email);
                }

                db.SaveChanges();

                var exhibitorCount = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailCount = db.Emails.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;
                var emailLogCount = db.EmailLogs.Where(x => x.LinkedShowId == currentShow.Id).ToList().Count;

                var model = new DeleteAllDataModel()
                {
                    ExhibitorCount = exhibitorCount,
                    EmailCount = emailCount,
                    EmailLogCount = emailLogCount
                };

                return PartialView("DeleteAllData", model);
            }
        }

        #endregion 

        #region Shows

        private Show GetCurrentShow()
        {
            var showId = (Guid)Session[AppConstants.CurrentShowAdminSession];
            if (showId != null)
            {
                using (var db = new CaravanQLDContext())
                {
                    var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                    return show;
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult CurrentShow()
        {
            return PartialView(GetCurrentShow());
        }

        public ActionResult Shows()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
            {
                return View();
            }
        }

        public ActionResult ShowList()
        {
            using (var db = new CaravanQLDContext())
            {
                var shows = db.Shows.OrderBy(x => x.Name).ToList();
                var model = new ShowListModel()
                {
                    Shows = shows
                };

                return PartialView(model);
            }
        }

        //public ActionResult AddNewShow()
        //{
        //    using (var db = new CaravanQLDContext())
        //    {
        //        var show = new Show();
        //        db.Shows.Add(show);
        //        db.EmailSettings.Add(new EmailSetting() { LinkedShowId = show.Id });
        //        db.PortalSettings.Add(new PortalSetting() { LinkedShowId = show.Id });
        //        db.SaveChanges();
        //        var model = new EditShowModel()
        //        {
        //            Show = show
        //        };

        //        return PartialView("EditShow", model);
        //    }
        //}

        public ActionResult AddNewShow()
        {
            using (var db = new CaravanQLDContext())
            {
                var shows = db.Shows.OrderBy(x => x.Name).ToList();

                var model = new AddNewShowModel()
                {
                    Shows = shows
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult AddNewShow(AddNewShowModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                try
                {
                    var showId = new Guid(model.Show);
                    var templateShow = db.Shows.FirstOrDefault(x => x.Id == showId);
                    if (templateShow == null)
                    {
                        var shows = db.Shows.OrderBy(x => x.Name).ToList();

                        model = new AddNewShowModel()
                        {
                            Shows = shows
                        };

                        return PartialView(model);
                    }
                    else
                    {
                        //Create the new show based on the template show
                        var show = new Show();
                        show.ShowId = templateShow.ShowId;
                        show.Name = "Based On " + templateShow.Name;
                        show.ShowType = templateShow.ShowType;
                        show.StartDate = templateShow.StartDate;
                        show.EndDate = templateShow.EndDate;
                        show.LinkIdentifier = "Temp" + templateShow.LinkIdentifier.Replace(" ", "");
                        show.ConfirmationEmail = String.IsNullOrEmpty(templateShow.ConfirmationEmail) ? "" : templateShow.ConfirmationEmail;
                        db.Shows.Add(show);
                        db.SaveChanges();

                        //Add Email Settings for the new show
                        var templateEmailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == templateShow.Id);
                        var emailSettings = new EmailSetting()
                        {
                            LinkedShowId = show.Id,
                            CompanyName = templateEmailSettings.CompanyName,
                            ContactEmail = templateEmailSettings.ContactEmail,
                            ContactNumber = templateEmailSettings.ContactNumber,
                            ContactFax = templateEmailSettings.ContactFax,
                            ContactPostalAddress = templateEmailSettings.ContactPostalAddress,
                            Website = templateEmailSettings.Website,
                            TestEmailAddress = templateEmailSettings.TestEmailAddress,
                            TestExhibitorShowID = templateEmailSettings.TestExhibitorShowID,
                            SortByDateCreated = templateEmailSettings.SortByDateCreated,
                            LogsPerPage = templateEmailSettings.LogsPerPage,
                            AdminEmailAddress = templateEmailSettings.AdminEmailAddress,
                            ApplicationEmailAddress = templateEmailSettings.ApplicationEmailAddress,
                            EmailSender = templateEmailSettings.EmailSender,
                            EmailLogsDisplayField = templateEmailSettings.EmailLogsDisplayField,
                            ExhibitorEmailAndLoginField = templateEmailSettings.ExhibitorEmailAndLoginField
                        };

                        db.EmailSettings.Add(emailSettings);
                        db.SaveChanges();

                        //Add Portal Settings for the new show
                        var templatePortalSettings = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == templateShow.Id);
                        var portalSettings = new PortalSetting();
                        portalSettings.LinkedShowId = show.Id;
                        db.PortalSettings.Add(portalSettings);
                        db.SaveChanges();

                        //Add form sections and form fields
                        var templateFormSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == templateShow.Id).ToList();

                        foreach (var templateFormSection in templateFormSections)
                        {
                            var formSection = new Form()
                            {
                                LinkedShowId = show.Id,
                                Name = templateFormSection.Name,
                                Order = templateFormSection.Order,
                                IsCurrent = templateFormSection.IsCurrent,
                                IncludeInExhibitorList = templateFormSection.IncludeInExhibitorList
                            };

                            db.Forms.Add(formSection);
                            db.SaveChanges();

                            //Create all the form fields
                            var templateFormFields = templateFormSection.FormFields.ToList();
                            foreach (var templateFormField in templateFormFields)
                            {
                                var formField = new FormField()
                                {
                                    FormId = formSection.Id,
                                    Name = templateFormField.Name,
                                    Order = templateFormField.Order,
                                    SpanTwoColumns = templateFormField.SpanTwoColumns,
                                    TextContent = String.IsNullOrEmpty(templateFormField.TextContent) ? "" : templateFormField.TextContent,
                                    ValidationType = templateFormField.ValidationType,
                                    WebProspectFieldName = templateFormField.WebProspectFieldName,
                                    DataType = templateFormField.DataType,
                                    IncludeInImport = templateFormField.IncludeInImport,
                                    Label = templateFormField.Label,
                                    Mandatory = templateFormField.Mandatory,
                                    TextField = templateFormField.TextField,
                                    MandatoryFormFieldValue = templateFormField.MandatoryFormFieldValue,
                                    CopiedFromFormFieldId = templateFormField.Id
                                };

                                db.FormFields.Add(formField);
                                db.SaveChanges();

                                //Create form field items
                                var templateFormFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == templateFormField.Id).ToList();

                                foreach (var templateFormFieldItem in templateFormFieldItems)
                                {
                                    var formFieldItem = new FormFieldItem()
                                    {
                                        FormFieldId = formField.Id,
                                        Order = templateFormFieldItem.Order,
                                        Text = templateFormFieldItem.Text,
                                        Value = templateFormFieldItem.Value
                                    };

                                    db.FormFieldItems.Add(formFieldItem);
                                    db.SaveChanges();
                                }
                            }
                        }

                        //Go back and assign all mandatory fields now that we have created all of the form fields
                        foreach (var templateFormSection in templateFormSections)
                        {
                            var templateFormFields = templateFormSection.FormFields.ToList();
                            foreach (var templateFormField in templateFormFields)
                            {
                                if (templateFormField.MandatoryFormFieldId != null)
                                {
                                    var newFormField = db.FormFields.FirstOrDefault(x => x.CopiedFromFormFieldId == templateFormField.Id);
                                    var mandatoryFormField = db.FormFields.FirstOrDefault(x => x.CopiedFromFormFieldId == templateFormField.MandatoryFormFieldId);
                                    newFormField.MandatoryFormFieldId = mandatoryFormField.Id;
                                }
                            }
                        }

                        //Create all pages
                        var templatePages = db.ContentPages.Where(x => x.LinkedShowId == templateShow.Id).ToList();

                        foreach (var templatePage in templatePages)
                        {
                            var page = new ContentPage()
                            {
                                IsCurrent = templatePage.IsCurrent,
                                LinkedShowId = show.Id,
                                Name = templatePage.Name,
                                Order = templatePage.Order,
                                PageType = templatePage.PageType,
                                TextContent = templatePage.TextContent
                            };

                            db.ContentPages.Add(page);
                            db.SaveChanges();
                        }

                        //Create all emails 
                        var templateEmails = db.Emails.Where(x => x.LinkedShowId == templateShow.Id).ToList();

                        foreach (var templateEmail in templateEmails)
                        {
                            var email = new Email()
                            {
                                LinkedShowId = show.Id,
                                Body = templateEmail.Body,
                                IsInitialEmail = templateEmail.IsInitialEmail,
                                Subject = templateEmail.Subject
                            };
                            db.Emails.Add(email);
                            db.SaveChanges();
                        }

                        //Get the 3 form fields from email settings and portal settings that I need to take care of
                        Guid templateEmailLogsDisplayFieldId = Guid.Empty;

                        if (!String.IsNullOrEmpty(templateEmailSettings.EmailLogsDisplayField))
                        {
                            Guid.TryParse(templateEmailSettings.EmailLogsDisplayField, out templateEmailLogsDisplayFieldId);
                            emailSettings.EmailLogsDisplayField = db.FormFields.FirstOrDefault(x => x.CopiedFromFormFieldId == templateEmailLogsDisplayFieldId).Id.ToString();
                        }

                        Guid templateExhibitorEmailAndLoginFieldId = Guid.Empty;

                        if (!String.IsNullOrEmpty(templateEmailSettings.ExhibitorEmailAndLoginField))
                        {
                            Guid.TryParse(templateEmailSettings.ExhibitorEmailAndLoginField, out templateExhibitorEmailAndLoginFieldId);
                            emailSettings.ExhibitorEmailAndLoginField = db.FormFields.FirstOrDefault(x => x.CopiedFromFormFieldId == templateExhibitorEmailAndLoginFieldId).Id.ToString();
                        }

                        Guid templateSortByFormFieldId = Guid.Empty;

                        if (templatePortalSettings.FormFieldId != null)
                        {
                            templateSortByFormFieldId = Guid.Parse(templatePortalSettings.FormFieldId.ToString());
                            portalSettings.FormFieldId = db.FormFields.FirstOrDefault(x => x.CopiedFromFormFieldId == templateSortByFormFieldId).Id;
                        }

                        db.SaveChanges();
                    }
                }
                catch (Exception exc)
                {
                    var doSomething = exc;
                }

                var model2 = new ShowListModel()
                {
                    Shows = db.Shows.OrderBy(x => x.Name).ToList()
                };

                return PartialView("ShowList", model2);
            }
        }

        public ActionResult EditShow(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == id);

                var model = new EditShowModel()
                {
                    Show = show
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditShow(EditShowModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == model.Show.Id);

                show.Name = model.Show.Name;
                show.ShowType = model.Show.ShowType;
                show.StartDate = model.Show.StartDate;
                show.EndDate = model.Show.EndDate;
                show.LinkIdentifier = model.Show.LinkIdentifier.Replace(" ", "");
                show.Contributor = String.IsNullOrEmpty(model.Show.Contributor) ? "" : model.Show.Contributor;
                show.ShellScheme = String.IsNullOrEmpty(model.Show.ShellScheme) ? "" : model.Show.ShellScheme;
                show.AdditionalNotesAndRequests = String.IsNullOrEmpty(model.Show.AdditionalNotesAndRequests) ? "" : model.Show.AdditionalNotesAndRequests;
                show.ConfirmationEmail = String.IsNullOrEmpty(model.Show.ConfirmationEmail) ? "" : model.Show.ConfirmationEmail;
                show.OnlyOptionB = model.Show.OnlyOptionB;
                show.OptionBTitle = String.IsNullOrEmpty(model.Show.OptionBTitle) ? "" : model.Show.OptionBTitle;
                show.OptionBText = String.IsNullOrEmpty(model.Show.OptionBText) ? "" : model.Show.OptionBText;

                db.SaveChanges();

                ModelState.Clear();
                model.Show = show;

                return PartialView(model);
            }
        }

        public ActionResult DeleteShow(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == id);
                var contentPages = db.ContentPages.Where(x => x.LinkedShowId == id).ToList();
                foreach (var contentPage in contentPages)
                {
                    db.ContentPages.Remove(contentPage);
                }
                db.SaveChanges();

                var emailLogs = db.EmailLogs.Where(x => x.LinkedShowId == id).ToList();
                foreach (var emailLog in emailLogs)
                {
                    db.EmailLogs.Remove(emailLog);
                }
                db.SaveChanges();

                var emails = db.Emails.Where(x => x.LinkedShowId == id).ToList();
                foreach (var email in emails)
                {
                    db.Emails.Remove(email);
                }
                db.SaveChanges();

                var emailSettings = db.EmailSettings.Where(x => x.LinkedShowId == id).ToList();
                foreach (var emailSetting in emailSettings)
                {
                    db.EmailSettings.Remove(emailSetting);
                }
                db.SaveChanges();

                var formExhibitors = db.FormExhibitors.Include("Form").Where(x => x.Form.LinkedShowId == id).ToList();
                foreach (var fe in formExhibitors)
                {
                    db.FormExhibitors.Remove(fe);
                }
                db.SaveChanges();

                var exhibitorApplications = db.ExhibitorApplications.Include("SiteInfo").Include("ProductInfo").Where(x => x.LinkedShowId == id).ToList();
                foreach (var ea in exhibitorApplications)
                {
                    if (ea.SiteInfo != null)
                        db.SiteInfos.Remove(ea.SiteInfo);

                    if (ea.ProductInfo != null)
                        db.ProductInfos.Remove(ea.ProductInfo);

                    db.ExhibitorApplications.Remove(ea);
                }
                db.SaveChanges();

                var portalSettings = db.PortalSettings.Where(x => x.LinkedShowId == id).ToList();
                foreach (var portalSetting in portalSettings)
                {
                    db.PortalSettings.Remove(portalSetting);
                }
                db.SaveChanges();

                var forms = db.Forms.Where(x => x.LinkedShowId == id).ToList();
                foreach (var form in forms)
                {
                    db.Forms.Remove(form);
                }
                db.SaveChanges();

                db.Shows.Remove(show);
                db.SaveChanges();

                var model = new ShowListModel()
                {
                    Shows = db.Shows.OrderBy(x => x.Name).ToList()
                };

                return PartialView("ShowList", model);
            }
        }

        public ActionResult ChooseShow()
        {
            using (var db = new CaravanQLDContext())
            {
                var shows = db.Shows.OrderBy(x => x.Name).ToList();

                var model = new ChooseShowModel()
                {
                    Shows = shows
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult ChooseShow(ChooseShowModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var showId = new Guid(model.Show);
                var show = db.Shows.FirstOrDefault(x => x.Id == showId);
                if (show != null)
                {
                    Session[AppConstants.CurrentShowAdminSession] = showId;
                    return RedirectToAction("Exhibitors");
                }
                else
                {
                    var shows = db.Shows.OrderBy(x => x.Name).ToList();
                    model = new ChooseShowModel()
                    {
                        Shows = shows
                    };

                    return View(model);
                }
            }
        }

        public ActionResult EditWebProspectFields(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var show = db.Shows.FirstOrDefault(x => x.Id == id);
                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == show.Id).OrderBy(x => x.Order).ToList();

                var model = new EditWebProspectFieldsModel()
                {
                    Show = show,
                    FormSections = formSections
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditWebProspectFields(EditWebProspectFieldsModel model, string fakeOptionalParameter = "")
        {
            using (var db = new CaravanQLDContext())
            {
                var request = Request;
                var show = db.Shows.FirstOrDefault(x => x.Id == model.Show.Id);
                var formSections = db.Forms.Include("FormFields").Where(x => x.LinkedShowId == show.Id).OrderBy(x => x.Order).ToList();


                //Set Show ID
                var webProspectShowId = request.Form.Get("Show.ShowId");

                if (!String.IsNullOrEmpty(webProspectShowId))
                {
                    show.ShowId = webProspectShowId;
                }
                else
                {
                    show.ShowId = "";
                }

                //Set Date Submitted ID
                var webProspectDateSubmitted = request.Form.Get("Show.DateSubmitted");

                if (!String.IsNullOrEmpty(webProspectDateSubmitted))
                {
                    show.DateSubmitted = webProspectDateSubmitted;
                }
                else
                {
                    show.DateSubmitted = "";
                }

                foreach (var formSection in formSections)
                {
                    foreach (var formField in formSection.FormFields)
                    {
                        var newWebProspectField = request.Form.Get(formField.Id.ToString());
                        if (!String.IsNullOrEmpty(newWebProspectField))
                        {
                            formField.WebProspectFieldName = newWebProspectField;
                        }
                        else
                        {
                            formField.WebProspectFieldName = "";
                        }
                    }
                }

                db.SaveChanges();

                ModelState.Clear();

                model = new EditWebProspectFieldsModel()
                {
                    FormSections = formSections,
                    Show = db.Shows.FirstOrDefault(x => x.Id == model.Show.Id)
                };

                return PartialView(model);
            }
        }

        #endregion

        #region Forms

        public ActionResult Forms()
        {
            if (!IsUserPortalAdmin())
                return RedirectToAction("Logout", "Account");
            else if (!HasSelectedShow())
                return RedirectToAction("ChooseShow");
            else
                return View();
        }

        public ActionResult FormList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();

                var model = new FormListModel()
                {
                    Forms = forms
                };

                return PartialView(model);
            }
        }

        public ActionResult CreateForm()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();

                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formOrder = 0;

                if (forms.Count >= 1)
                    formOrder = forms.Select(x => x.Order).Max() + 1;

                var form = new Form()
                {
                    Order = formOrder,
                    LinkedShowId = currentShow.Id
                };

                db.Forms.Add(form);
                db.SaveChanges();

                var model = new EditFormModel()
                {
                    Form = form
                };

                return PartialView("EditForm", model);
            }
        }

        public ActionResult EditForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var form = db.Forms.FirstOrDefault(x => x.Id == id);

                var model = new EditFormModel()
                {
                    Form = form
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditForm(EditFormModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var form = forms.FirstOrDefault(x => x.Id == model.Form.Id);
                form.Name = model.Form.Name;
                form.IsCurrent = model.Form.IsCurrent;
                form.IncludeInExhibitorList = model.Form.IncludeInExhibitorList;

                if (form.Order != model.Form.Order)
                {
                    var oldOrder = form.Order;
                    var newOrder = model.Form.Order;
                    if (oldOrder > newOrder)
                    {
                        if (newOrder < 0)
                            newOrder = 0;

                        var formsToAdjust = forms.Where(x => x.Order < oldOrder && x.Order >= newOrder);
                        foreach (var f in formsToAdjust)
                        {
                            f.Order = f.Order + 1;
                        }
                    }
                    else if (oldOrder < newOrder)
                    {
                        var maxOrder = forms.Select(x => x.Order).Max();
                        if (newOrder > maxOrder)
                            newOrder = maxOrder;

                        var formsToAdjust = forms.Where(x => x.Order > oldOrder && x.Order <= newOrder);
                        foreach (var f in formsToAdjust)
                        {
                            f.Order = f.Order - 1;
                        }
                    }

                    form.Order = newOrder;
                }

                db.SaveChanges();

                ModelState.Clear();

                model = new EditFormModel()
                {
                    Form = form
                };

                return PartialView(model);
            }
        }

        public ActionResult DeleteForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                //what other things do I need to delete?
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var deletedFormOrder = form.Order;

                var portalSettings = db.PortalSettings.FirstOrDefault(x => x.LinkedShowId == form.LinkedShowId);
                var emailSettings = db.EmailSettings.FirstOrDefault(x => x.LinkedShowId == form.LinkedShowId);

                var formFields = db.FormFields.Where(x => x.FormId == form.Id).ToList();
                foreach (var formField in formFields)
                {
                    if (portalSettings.FormFieldId == formField.Id)
                    {
                        portalSettings.FormFieldId = null;
                        db.SaveChanges();
                    }
                }

                db.Forms.Remove(form);

                var currentShow = GetCurrentShow();
                var formsToAdjust = db.Forms.Where(x => x.Order > deletedFormOrder);
                foreach (var f in formsToAdjust)
                {
                    f.Order = f.Order - 1;
                }

                db.SaveChanges();

                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();

                var model = new FormListModel()
                {
                    Forms = forms
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult MoveFormUp(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var form = forms.FirstOrDefault(x => x.Id == id);

                var currentOrder = form.Order;
                if (currentOrder != 0)
                {
                    var newOrder = currentOrder - 1;
                    var formAbove = forms.FirstOrDefault(x => x.Order == newOrder);
                    form.Order = newOrder;
                    formAbove.Order = currentOrder;
                }

                db.SaveChanges();

                var model = new FormListModel()
                {
                    Forms = forms.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult MoveFormDown(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var forms = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).ToList();
                var form = forms.FirstOrDefault(x => x.Id == id);

                var maxOrder = forms.Select(x => x.Order).Max();
                if (form.Order != maxOrder)
                {
                    var currentOrder = form.Order;
                    var newOrder = currentOrder + 1;
                    var formBelow = forms.FirstOrDefault(x => x.Order == newOrder);
                    form.Order = newOrder;
                    formBelow.Order = currentOrder;
                }

                db.SaveChanges();

                var model = new FormListModel()
                {
                    Forms = forms.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormList", model);
            }
        }

        public ActionResult EditStandardForm(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.Include("FormFields").FirstOrDefault(x => x.Id == id);
                var model = new EditStandardFormModel()
                {
                    Form = form
                };

                return PartialView(model);
            }
        }

        public ActionResult CreateFormField(Guid formSectionId, Guid formFieldId)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formSection = db.Forms.Include("FormFields").FirstOrDefault(x => x.Id == formSectionId);
                var formField = new FormField()
                {
                    Id = formFieldId,
                    FormId = formSectionId,
                    Order = formSection.FormFields.ToList().Count
                };

                db.FormFields.Add(formField);
                db.SaveChanges();

                //Create Confirmation Field List
                var formFields = db.FormFields.Where(x => x.FormId == formField.FormId).OrderBy(x => x.Name).ToList();
                var confirmFieldList = new List<SelectListItem>();
                confirmFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var ff in formFields)
                {
                    confirmFieldList.Add(new SelectListItem() { Text = ff.Name, Value = ff.Id.ToString() });
                }

                //Create Text Field List
                var textFieldList = new List<SelectListItem>();
                textFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var textField in CreateSmartFieldList())
                {
                    textFieldList.Add(new SelectListItem() { Text = textField[1], Value = textField[0] });
                }

                //Create Mandatory Field List
                var mandatoryFieldList = new List<SelectListItem>();
                mandatoryFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var ff in formFields.Where(x => x.DataType == FormDataType.Checkbox || x.DataType == FormDataType.DropDown || x.DataType == FormDataType.RadioButton))
                {
                    mandatoryFieldList.Add(new SelectListItem() { Text = ff.Name, Value = ff.Id.ToString() });
                }

                var model = new EditFormFieldModel()
                {
                    FormField = formField,
                    ConfirmFieldList = confirmFieldList,
                    TextFieldList = textFieldList,
                    MandatoryFormFieldList = mandatoryFieldList
                };

                return PartialView("EditFormField", model);
            }
        }

        public JsonResult CreateFormFieldValues(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var exhibitors = db.ExhibitorApplications.Where(x => x.LinkedShowId == currentShow.Id).ToList();

                foreach (var exhibitor in exhibitors)
                {
                    var formFieldValue = new FormFieldValue()
                    {
                        ExhibitorApplicationId = exhibitor.Id,
                        FormFieldId = id
                    };

                    db.FormFieldValues.Add(formFieldValue);
                }

                db.SaveChanges();
            }

            return Json(id, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditFormField(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Where(x => x.FormId == formField.FormId).OrderBy(x => x.Name).ToList();

                //Create Confirmation Field List
                var confirmFieldList = new List<SelectListItem>();
                confirmFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var ff in formFields)
                {
                    confirmFieldList.Add(new SelectListItem() { Text = ff.Name, Value = ff.Id.ToString() });
                }

                //Create Text Field List
                var textFieldList = new List<SelectListItem>();
                textFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var textField in CreateSmartFieldListStaticVersion())
                {
                    textFieldList.Add(new SelectListItem() { Text = textField[1], Value = textField[0] });
                }

                //Create Mandatory Field List
                var mandatoryFieldList = new List<SelectListItem>();
                mandatoryFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var ff in formFields.Where(x => x.DataType == FormDataType.Checkbox || x.DataType == FormDataType.DropDown || x.DataType == FormDataType.RadioButton))
                {
                    mandatoryFieldList.Add(new SelectListItem() { Text = ff.Name, Value = ff.Id.ToString() });
                }

                var model = new EditFormFieldModel()
                {
                    FormField = formField,
                    ConfirmFieldList = confirmFieldList,
                    TextFieldList = textFieldList,
                    MandatoryFormFieldList = mandatoryFieldList
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditFormField(EditFormFieldModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == model.FormField.Id);
                var formFields = db.FormFields.Where(x => x.FormId == formField.FormId).ToList();
                formField.Name = model.FormField.Name;
                formField.Label = model.FormField.Label;
                formField.Mandatory = model.FormField.Mandatory;
                formField.DataType = model.FormField.DataType;
                formField.ValidationType = model.FormField.ValidationType;
                formField.ConfirmationField = CheckStringForNull(model.FormField.ConfirmationField);
                formField.TextField = CheckStringForNull(model.FormField.TextField);
                formField.MandatoryFormFieldId = model.FormField.MandatoryFormFieldId;
                formField.MandatoryFormFieldValue = CheckStringForNull(model.FormField.MandatoryFormFieldValue);

                if (formField.Order != model.FormField.Order)
                {
                    var currentOrder = formField.Order;
                    var newOrder = model.FormField.Order;

                    if (currentOrder > newOrder)
                    {
                        if (newOrder < 0)
                            newOrder = 0;

                        var formFieldsToAdjust = formFields.Where(x => x.Order >= newOrder && x.Order < currentOrder);
                        foreach (var f in formFieldsToAdjust)
                        {
                            f.Order = f.Order + 1;
                        }
                    }
                    else if (currentOrder < newOrder)
                    {
                        if (formFields.Count > 0)
                        {
                            var maxOrder = formFields.Select(x => x.Order).Max();
                            if (newOrder > maxOrder)
                                newOrder = maxOrder;
                        }

                        var formFieldsToAdjust = formFields.Where(x => x.Order <= newOrder && x.Order > currentOrder);
                        foreach (var f in formFieldsToAdjust)
                        {
                            f.Order = f.Order - 1;
                        }
                    }

                    formField.Order = newOrder;
                }

                formField.SpanTwoColumns = model.FormField.SpanTwoColumns;
                if (model.FormField.DataType == FormDataType.ContentSection)
                {
                    formField.TextContent = model.FormField.TextContent;
                }

                db.SaveChanges();
                ModelState.Clear();
                model.FormField = formField;

                //Create Confirmation Field List
                var confirmFieldList = new List<SelectListItem>();
                confirmFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var ff in formFields)
                {
                    confirmFieldList.Add(new SelectListItem() { Text = ff.Name, Value = ff.Id.ToString() });
                }

                model.ConfirmFieldList = confirmFieldList;

                //Create Text Field List
                var textFieldList = new List<SelectListItem>();
                textFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var textField in CreateSmartFieldListStaticVersion())
                {
                    textFieldList.Add(new SelectListItem() { Text = textField[1], Value = textField[0] });
                }

                model.TextFieldList = textFieldList;

                //Create Mandatory Field List
                var mandatoryFieldList = new List<SelectListItem>();
                mandatoryFieldList.Add(new SelectListItem() { Text = "", Value = "", Selected = true });

                foreach (var ff in formFields.Where(x => x.DataType == FormDataType.Checkbox || x.DataType == FormDataType.DropDown || x.DataType == FormDataType.RadioButton))
                {
                    mandatoryFieldList.Add(new SelectListItem() { Text = ff.Name, Value = ff.Id.ToString() });
                }

                model.MandatoryFormFieldList = mandatoryFieldList;

                return PartialView(model);
            }
        }

        public ActionResult FormFieldList(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var form = db.Forms.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == id).OrderBy(x => x.Order).ToList();
                var model = new FormFieldListModel()
                {
                    Form = form,
                    FormFields = formFields
                };

                return PartialView(model);
            }
        }

        public ActionResult MoveFormFieldUp(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == formField.FormId).ToList();

                if (formField.Order != 0)
                {
                    var currentOrder = formField.Order;
                    var newOrder = formField.Order - 1;
                    var formFieldAbove = formFields.FirstOrDefault(x => x.Order == newOrder);

                    formField.Order = newOrder;
                    formFieldAbove.Order = currentOrder;
                    db.SaveChanges();
                }

                var model = new FormFieldListModel()
                {
                    Form = formField.Form,
                    FormFields = formFields.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormFieldList", model);
            }
        }

        public ActionResult MoveFormFieldDown(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == formField.FormId).ToList();

                var maxOrder = 0;
                if (formFields.Count > 0)
                    maxOrder = formFields.Select(x => x.Order).Max();

                if (formField.Order < maxOrder)
                {
                    var currentOrder = formField.Order;
                    var newOrder = formField.Order + 1;
                    var formFieldBelow = formFields.FirstOrDefault(x => x.Order == newOrder);

                    formField.Order = newOrder;
                    formFieldBelow.Order = currentOrder;
                    db.SaveChanges();
                }

                var model = new FormFieldListModel()
                {
                    Form = formField.Form,
                    FormFields = formFields.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormFieldList", model);
            }
        }

        public ActionResult GetNewFormFieldOrder(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                return Json(formField.Order, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteFormField(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.FirstOrDefault(x => x.Id == id);
                var form = db.Forms.FirstOrDefault(x => x.Id == formField.FormId);
                var formFields = db.FormFields.Include("FormFieldItems").Where(x => x.FormId == formField.FormId);
                var formFieldsToAdjust = formFields.Where(x => x.Order > formField.Order).ToList();

                db.FormFields.Remove(formField);

                foreach (var f in formFieldsToAdjust)
                {
                    f.Order = f.Order - 1;
                }

                db.SaveChanges();

                var model = new FormFieldListModel()
                {
                    Form = form,
                    FormFields = formFields.OrderBy(x => x.Order).ToList()
                };

                return PartialView("FormFieldList", model);
            }
        }

        public ActionResult CreateFormFieldItem(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.Include("FormFieldItems").FirstOrDefault(x => x.Id == id);
                var maxOrder = 0;
                if (formField.FormFieldItems.Count > 0)
                    maxOrder = formField.FormFieldItems.Select(x => x.Order).Max() + 1;

                var formFieldItem = new FormFieldItem()
                {
                    FormFieldId = id,
                    Order = maxOrder
                };

                db.FormFieldItems.Add(formFieldItem);
                db.SaveChanges();

                var model = new EditFormFieldItemModel()
                {
                    FormFieldItem = formFieldItem
                };

                return PartialView("EditFormFieldItem", model);
            }
        }

        public ActionResult EditFormFieldItem(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formFieldItem = db.FormFieldItems.FirstOrDefault(x => x.Id == id);
                var model = new EditFormFieldItemModel()
                {
                    FormFieldItem = formFieldItem
                };

                return PartialView(model);
            }
        }

        [HttpPost]
        public ActionResult EditFormFieldItem(EditFormFieldItemModel model)
        {
            using (var db = new CaravanQLDContext())
            {
                var formFieldItem = db.FormFieldItems.FirstOrDefault(x => x.Id == model.FormFieldItem.Id);
                var formFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == formFieldItem.FormFieldId).ToList();
                formFieldItem.Text = model.FormFieldItem.Text;
                formFieldItem.Value = model.FormFieldItem.Value;

                if (formFieldItem.Order != model.FormFieldItem.Order)
                {
                    var currentOrder = formFieldItem.Order;
                    var newOrder = model.FormFieldItem.Order;

                    if (currentOrder > newOrder)
                    {
                        if (newOrder < 0)
                            newOrder = 0;

                        var formFieldItemsToAdjust = formFieldItems.Where(x => x.Order >= newOrder && x.Order < currentOrder);
                        foreach (var f in formFieldItemsToAdjust)
                        {
                            f.Order = f.Order + 1;
                        }
                    }
                    else if (currentOrder < newOrder)
                    {
                        if (formFieldItems.Count > 0)
                        {
                            var maxOrder = formFieldItems.Select(x => x.Order).Max();
                            if (newOrder > maxOrder)
                                newOrder = maxOrder;
                        }

                        var formFieldItemsToAdjust = formFieldItems.Where(x => x.Order <= newOrder && x.Order > currentOrder);
                        foreach (var f in formFieldItemsToAdjust)
                        {
                            f.Order = f.Order - 1;
                        }
                    }

                    formFieldItem.Order = newOrder;
                }

                db.SaveChanges();
                model.FormFieldItem = formFieldItem;
                ModelState.Clear();

                return PartialView(model);
            }
        }

        public ActionResult FormFieldItemList(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formField = db.FormFields.Include("FormFieldItems").FirstOrDefault(x => x.Id == id);
                var model = new FormFieldItemListModel()
                {
                    FormFieldItems = formField.FormFieldItems.OrderBy(x => x.Order).ToList(),
                    FormField = formField
                };
                return PartialView(model);
            }
        }

        public ActionResult DeleteFormFieldItem(Guid id)
        {
            using (var db = new CaravanQLDContext())
            {
                var formFieldItem = db.FormFieldItems.Include("FormField").FirstOrDefault(x => x.Id == id);
                var formField = formFieldItem.FormField;
                var formFieldItemsToAdjust = db.FormFieldItems.Where(x => x.FormFieldId == formFieldItem.FormFieldId && x.Order > formFieldItem.Order);

                foreach (var f in formFieldItemsToAdjust)
                {
                    f.Order = f.Order - 1;
                }

                db.FormFieldItems.Remove(formFieldItem);
                db.SaveChanges();

                var model = new FormFieldItemListModel()
                {
                    FormFieldItems = db.FormFieldItems.Where(x => x.FormFieldId == formField.Id).OrderBy(x => x.Order).ToList(),
                    FormField = formField
                };

                return PartialView("FormFieldItemList", model);
            }
        }

        public JsonResult GetSmartFieldArray()
        {
            return Json(CreateSmartFieldList().ToArray(), JsonRequestBehavior.AllowGet);
        }

        private List<string[]> CreateSmartFieldList()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formSections = db.Forms.Where(x => x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFields = db.FormFields.Include("Form").Where(x => x.Form.LinkedShowId == currentShow.Id && x.DataType != FormDataType.ContentSection && x.DataType != FormDataType.TextField).OrderBy(x => x.Name).ToList();

                var smartFieldList = new List<string[]>();

                smartFieldList.Add(new string[3] { "", "CURRENT SHOW TITLE", "CURRENT SHOW TITLE" });
                smartFieldList.Add(new string[3] { "%Current Show Title%", "-Current Show Title", "Current Show Title" });
                smartFieldList.Add(new string[3] { "", "SHOW ID", "SHOW ID" });
                smartFieldList.Add(new string[3] { "%Show ID%", "-Show ID", "Show ID" });

                foreach (var formSection in formSections)
                {
                    smartFieldList.Add(new string[3] { "", formSection.Name.ToUpper(), formSection.Name.ToUpper() });

                    var sectionFields = formFields.Where(x => x.FormId == formSection.Id).OrderBy(x => x.Name);
                    foreach (var field in sectionFields)
                    {
                        smartFieldList.Add(new string[3] { "%" + field.Name + "%", " -" + field.Name, field.Name });
                    }
                }
                return smartFieldList;
            }
        }

        private List<string[]> CreateSmartFieldListStaticVersion()
        {
            var smartFieldList = new List<string[]>();

            //smartFieldList.Add(new string[3] { "", "CURRENT SHOW TITLE", "CURRENT SHOW TITLE" });
            //smartFieldList.Add(new string[3] { "%Current Show Title%", "-Current Show Title", "Current Show Title" });
            smartFieldList.Add(new string[3] { "", "SHOW ID", "SHOW ID" });
            smartFieldList.Add(new string[3] { "%Show ID%", "-Show ID", "Show ID" });

            return smartFieldList;
        }

        public ActionResult ViewApplicationForm()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                var pageContent = db.ContentPages.FirstOrDefault(x => x.LinkedShowId == currentShow.Id && x.PageType == PageType.Application);
                var pageText = pageContent == null ? "" : pageContent.TextContent;

                var model = new ViewApplicationFormModel()
                {
                    FormSections = formSections,
                    FormFieldItems = formFieldItems,
                    PageContent = pageText
                };

                return PartialView(model);
            }
        }

        public ActionResult ViewConfirmationForm()
        {
            using (var db = new CaravanQLDContext())
            {
                var currentShow = GetCurrentShow();
                var formSections = db.Forms.Include("FormFields").Where(x => x.IsCurrent && x.LinkedShowId == currentShow.Id).OrderBy(x => x.Order).ToList();
                var formFieldItems = db.FormFieldItems.Include("FormField.Form").Where(x => x.FormField.Form.LinkedShowId == currentShow.Id).ToList();
                var pageContent = db.ContentPages.FirstOrDefault(x => x.LinkedShowId == currentShow.Id && x.PageType == PageType.Confirmation);
                var pageText = pageContent == null ? "" : pageContent.TextContent;

                var model = new ViewConfirmationFormModel()
                {
                    FormSections = formSections,
                    FormFieldItems = formFieldItems,
                    PageContent = pageText
                };

                return PartialView(model);
            }
        }

        #endregion
    }
}
