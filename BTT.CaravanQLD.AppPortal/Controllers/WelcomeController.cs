﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BTT.CaravanQLD.AppPortal.Controllers
{
    [Authorize]
    public class WelcomeController : Controller
    {
        //
        // GET: /Welcome/

        public ActionResult Index()
        {
            if (User != null && User.Identity.Name == AppConstants.AdminUserName)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "Welcome");
            }
            else
            {
                return View();
            }
        }

    }
}
