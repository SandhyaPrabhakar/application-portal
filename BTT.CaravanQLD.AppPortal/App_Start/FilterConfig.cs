﻿using System.Web;
using System.Web.Mvc;

namespace BTT.CaravanQLD.AppPortal
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}