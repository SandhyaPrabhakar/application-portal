﻿using System.Web;
using System.Web.Optimization;

namespace BTT.CaravanQLD.AppPortal
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/dataportalscripts").Include(
                "~/Scripts/btt-notifications.js",
                "~/Scripts/ckeditor/ckeditor.js",
                "~/Scripts/ckfinder/ckfinder.js",
                "~/Scripts/DataPortalScript.js"));

            bundles.Add(new ScriptBundle("~/bundles/applicationscripts").Include(
                "~/Scripts/ApplicationFormScript.js",
                "~/Scripts/ExhibitorPortalScript.js",
                "~/Scripts/ckfinder_ex/ckfinder.js"));

            bundles.Add(new ScriptBundle("~/bundles/noty").Include(
                "~/Scripts/noty/packaged/jquery.noty.packaged.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqGrid").Include(
                "~/Scripts/jqGrid/grid.locale-en.js",
                "~/Scripts/jqGrid/jquery.jqGrid.src.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/themes/yaml/core/base.css",
                "~/Content/themes/yaml/forms/gray-theme.css",
                "~/Content/typography.css",
                "~/Content/ui.jqgrid.css",
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css/iehacks").Include("~/Content/themes/yaml/core/iehacks.min.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            
        }
    }
}