﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BTT.CaravanQLD.AppPortal.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static string SplitPascalCase(this string PascalCaseString, bool ReplaceUnderscoreWithSpace)
        {
            var result = ReplaceUnderscoreWithSpace ? Regex.Replace(PascalCaseString, "([A-Z])(_)([A-Z])", "$1 $3").Trim() : PascalCaseString;
            result = Regex.Replace(result, "([^A-Z])([A-Z])", "$1 $2").Trim();
            result = Regex.Replace(result, "([A-Z])([A-Z])([^A-Z\\s])", "$1 $2$3").Trim();
            result = Regex.Replace(result, "([A-Za-z])([0-9])", "$1 $2").Trim();
            result = Regex.Replace(result, "([0-9])([A-Za-z])", "$1 $2").Trim();
            return result;
        }

        public static MvcHtmlString LabelTitleizeFor<T, TResult>(this HtmlHelper<T> helper, Expression<Func<T, TResult>> expression)
        {
            string propertyName = ExpressionHelper.GetExpressionText(expression);

            if (propertyName.IndexOf(".") > 0)
            {
                propertyName = propertyName.Substring(propertyName.LastIndexOf(".") + 1);
            }

            string labelValue = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).DisplayName;

            if (string.IsNullOrEmpty(labelValue))
            {
                //labelValue = Inflector.Net.Inflector.Titleize(propertyName);
                labelValue = propertyName.SplitPascalCase(true);
            }

            string label = String.Format("<label for=\"{0}\">{1}</label>", propertyName, labelValue);
            return MvcHtmlString.Create(label);
        }

        public static MvcHtmlString LabelTitleizeFor<T, TResult>(this HtmlHelper<T> helper, Expression<Func<T, TResult>> expression, string className)
        {
            string propertyName = ExpressionHelper.GetExpressionText(expression);

            if (propertyName.IndexOf(".") > 0)
            {
                propertyName = propertyName.Substring(propertyName.LastIndexOf(".") + 1);
            }

            string labelValue = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).DisplayName;

            if (string.IsNullOrEmpty(labelValue))
            {
                //labelValue = Inflector.Net.Inflector.Titleize(propertyName);
                labelValue = propertyName.SplitPascalCase(true);
            }

            string label = String.Format("<label class=\"{2}\" for=\"{0}\">{1}</label>", propertyName, labelValue, className);
            return MvcHtmlString.Create(label);
        }

        /// <summary>
        /// Creates a link that will open a jQuery UI dialog form.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="dialogContentUrl">The url that will return the content to be loaded into the dialog window</param>
        /// <param name="dialogTitle">The title to be displayed in the dialog window</param>
        /// <param name="updateTargetId">The id of the div that should be updated after the form submission</param>
        /// <param name="updateUrl">The url that will return the content to be loaded into the target div</param>
        /// <returns></returns>
        public static MvcHtmlString DialogFormLink(this HtmlHelper htmlHelper, string linkText, string dialogContentUrl,
            string dialogTitle, string updateTargetId, string updateUrl, bool displayOnly = false, string idName = "", string className = "", bool showEmailSaveButton = false)
        {
            TagBuilder builder = new TagBuilder("a");
            builder.SetInnerText(linkText);
            builder.Attributes.Add("href", dialogContentUrl);
            if (!String.IsNullOrEmpty(idName))
                builder.Attributes.Add("id", idName);
            if (!String.IsNullOrEmpty(className))
                builder.AddCssClass(className);
            builder.Attributes.Add("title", dialogTitle);
            builder.Attributes.Add("data-dialog-title", dialogTitle);
            builder.Attributes.Add("data-update-target-id", updateTargetId);
            builder.Attributes.Add("data-update-url", updateUrl);
            builder.Attributes.Add("data-display-only", displayOnly ? "true" : "false");
            builder.Attributes.Add("data-show-email-save-button", showEmailSaveButton ? "true" : "false");

            // Add a css class named dialogLink that will be
            // used to identify the anchor tag and to wire up
            // the jQuery functions
            builder.AddCssClass("dialogLink");

            return new MvcHtmlString(builder.ToString());
        }

        public static MvcHtmlString ConvertBoolToYesOrNo (bool value)
        {
            return new MvcHtmlString(value ? "Yes" : "No");
        }

        public static SelectList ToSelectList<T>(this T enumeration)
        {
            var source = Enum.GetValues(typeof(T));
            var items = new Dictionary<object, string>();
            var displayAttributeType = typeof(DisplayAttribute);

            foreach (var value in source)
            {
                FieldInfo field = value.GetType().GetField(value.ToString());
                DisplayAttribute attrs = (DisplayAttribute)field.GetCustomAttributes(displayAttributeType, false).First();
                items.Add(value, attrs.GetName());
            }

            return new SelectList(items, "Key", "Value", enumeration);
        }
    }
}